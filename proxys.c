/*---------------------------------------------------------------------------
 *  Proyecto Fin de Carrera: Enjambre de microbots para despliegue autónomo de 
 *                           una red inalámbrica mallada.
 * 
 *  Francisco José Aguilera Leal
 *  Universidad de Málaga
 * --------------------------------------------------------------------------- 
 * Funciones para el manejo de proxys.
 * ---------------------------------------------------------------------------
*/

#include "proxys.h"

/* ---------------------------------------------------------------------------
 * configuraProxys - Configura los proxies de los robots
 * --------------------------------------------------------------------------- */
int configuraProxys(playerc_client_t** sim_robots, playerc_position2d_t** sim_position2d, playerc_sonar_t** sim_sonar, playerc_simulation_t** simulation, playerc_graphics2d_t** sim_graphics2d, int number_robots,int puerto)
{
	int i;

	//Create the client and connect it to the server
	*sim_robots = playerc_client_create(NULL, "localhost", puerto); //6665
	if (0 != playerc_client_connect(*sim_robots))
		return -1;	

	//Create and subscribe to the position2d devices
	for (i=0;i<number_robots;i++)
	{	
		sim_position2d[i] = playerc_position2d_create(*sim_robots, i);
		if (playerc_position2d_subscribe(sim_position2d[i], PLAYER_OPEN_MODE))
			return -1;
		
		if(i%5==0)
			playerc_client_read(*sim_robots);
	}

	//Create and subscribe to the ranger devices
	for (i=0;i<number_robots;i++)
	{
		sim_sonar[i] = playerc_sonar_create(*sim_robots, i);
		if (playerc_sonar_subscribe(sim_sonar[i], PLAYER_OPEN_MODE))
			return -1;

		if(i%5==0)
			playerc_client_read(*sim_robots);
	}

	playerc_client_read(*sim_robots);

	//Create and subscribe to the simulation device
	
	*simulation = playerc_simulation_create(*sim_robots, 0);
	if (playerc_simulation_subscribe(*simulation, PLAYER_OPEN_MODE))
		return -1;


	//Create and sub scribe to the graphics2d device
	for (i=0;i<number_robots;i++)
	{
		sim_graphics2d[i] = playerc_graphics2d_create(*sim_robots, i);
		if (playerc_graphics2d_subscribe(sim_graphics2d[i], PLAYER_OPEN_MODE))
			return -1;

		if(i%5==0)
			playerc_client_read(*sim_robots);
	}

	return 1;
}

/* ---------------------------------------------------------------------------
 * cierraProxys - Cierra los proxies de los robots
 * --------------------------------------------------------------------------- */
void cierraProxys(playerc_client_t** sim_robots, playerc_position2d_t** sim_position2d, playerc_sonar_t** sim_sonar, playerc_simulation_t** simulation, playerc_graphics2d_t** sim_graphics2d, int number_robots)
{
	int i;

	for(i=0;i<number_robots;i++)
	{
		//playerc_client_read(*sim_robots);
		playerc_position2d_unsubscribe(sim_position2d[i]);
		playerc_position2d_destroy(sim_position2d[i]);
		playerc_sonar_unsubscribe(sim_sonar[i]);
		//playerc_client_read(*sim_robots);
		playerc_sonar_destroy(sim_sonar[i]);
		playerc_graphics2d_unsubscribe(sim_graphics2d[i]);
		playerc_graphics2d_destroy(sim_graphics2d[i]);
	}
	playerc_simulation_unsubscribe(*simulation);
	playerc_simulation_destroy(*simulation);
	playerc_client_disconnect(*sim_robots);
	playerc_client_destroy(*sim_robots);
}
