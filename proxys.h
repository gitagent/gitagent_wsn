/*---------------------------------------------------------------------------
 *  Proyecto Fin de Carrera: Enjambre de microbots para despliegue autónomo de 
 *                           una red inalámbrica mallada.
 * 
 *  Francisco José Aguilera Leal
 *  Universidad de Málaga
 * --------------------------------------------------------------------------- 
 * Archivo de cabecera de proxys.c
 * ---------------------------------------------------------------------------
*/

#ifndef PROXYS_H
#define PROXYS_H

#include <libplayerc/playerc.h>

int configuraProxys(playerc_client_t** sim_robots, playerc_position2d_t** sim_position2d, playerc_sonar_t** sim_sonar, playerc_simulation_t** simulation, playerc_graphics2d_t** sim_graphics2d, int number_robots,int puerto);
void cierraProxys(playerc_client_t** sim_robots, playerc_position2d_t** sim_position2d, playerc_sonar_t** sim_sonar, playerc_simulation_t** simulation, playerc_graphics2d_t** sim_graphics2d, int number_robots);

#endif /*PROXYS_H*/
