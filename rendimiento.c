/*---------------------------------------------------------------------------
 *  Proyecto Fin de Carrera: Enjambre de microbots para despliegue autónomo de 
 *                           una red inalámbrica mallada.
 * 
 *  Francisco José Aguilera Leal
 *  Universidad de Málaga
 * --------------------------------------------------------------------------- 
 * Menu con opciones para mostrar informacion sobre el despliegue.
 * ---------------------------------------------------------------------------
*/

#include "rendimiento.h"

/* ---------------------------------------------------------------------------
 * hayDatosDespliegue - Comprueba si se tienen datos del despliegue
 * --------------------------------------------------------------------------- */
int hayDatosDespliegue(deployment_data_t* d_data)
{
	return(d_data->p_data->own_packets_sent!=NULL);
}

/* ---------------------------------------------------------------------------
 * borraDatosDespliegue - Borra todos los datos del despliegue
 * --------------------------------------------------------------------------- */
void borraDatosDespliegue(deployment_data_t* d_data,int* rotos)
{
	if(rotos!=NULL)
	{
		free(rotos);
		rotos=NULL;
	}

	free(d_data->robot_data);
	d_data->robot_data=NULL;

	free(d_data->p_data->packets_sent);
	d_data->p_data->packets_sent=NULL;
	free(d_data->p_data->own_packets_sent);
	d_data->p_data->own_packets_sent=NULL;
	free(d_data->p_data->packets_rx);
	d_data->p_data->packets_rx=NULL;
	free(d_data->p_data->own_packets_rx);
	d_data->p_data->own_packets_rx=NULL;
	free(d_data->p_data->distance_walked);
	d_data->p_data->distance_walked=NULL;
	free(d_data->p_data->positions);
	d_data->p_data->positions=NULL;

	d_data->number_robots=0;
	d_data->map_size=0;
}

/* ---------------------------------------------------------------------------
 * inicializaDatosDespliegue - Inicializa struct de datos del despliegue
 * --------------------------------------------------------------------------- */
void inicializaDatosDespliegue(deployment_data_t* d_data,int number_robots,int map_size)
{
	d_data->map_size=map_size;
	d_data->number_robots=number_robots;

	d_data->p_data->packets_sent=(double*) malloc(sizeof(double) * number_robots);
	d_data->p_data->own_packets_sent=(double*) malloc(sizeof(double) * number_robots);
	d_data->p_data->packets_rx=(double*) malloc(sizeof(double) * number_robots);
	d_data->p_data->own_packets_rx=(double*) malloc(sizeof(double) * number_robots);
	d_data->p_data->distance_walked=(double*) malloc(sizeof(double) * number_robots);
	d_data->p_data->wasted_energy=(double*) malloc(sizeof(double) * number_robots);
	d_data->p_data->net_mean_power=(double*) malloc(sizeof(double) * 1000);
	d_data->p_data->positions=(player_pose2d_t*) malloc(sizeof(player_pose2d_t) * number_robots);
	d_data->robot_data=(robot_data_t*) malloc(sizeof(robot_data_t) * number_robots);
}

///* ---------------------------------------------------------------------------
// * inicializaDatosDespliegueTest - Inicializa struct de datos del test (ojo, distinta de la anterior!!!
// * --------------------------------------------------------------------------- */
static void inicializaDatosDespliegueTest(deployment_data_t* d_data_test, player_pose2d_t* positions,int number_robots,int map_size)
{
	d_data_test->map_size=map_size;
	d_data_test->number_robots=number_robots;

	d_data_test->p_data->packets_sent=(double*) malloc(sizeof(double) * number_robots);
	d_data_test->p_data->own_packets_sent=(double*) malloc(sizeof(double) * number_robots);
	d_data_test->p_data->packets_rx=(double*) malloc(sizeof(double) * number_robots);
	d_data_test->p_data->own_packets_rx=(double*) malloc(sizeof(double) * number_robots);
	d_data_test->p_data->distance_walked=(double*) malloc(sizeof(double) * number_robots);
	d_data_test->p_data->net_mean_power=(double*) malloc(sizeof(double) * 1000);
	d_data_test->p_data->wasted_energy=(double*) malloc(sizeof(double) * number_robots); //Aunque no se use, crear..
	d_data_test->p_data->positions=positions;
	//d_data_test->robot_data=(robot_data_t*) malloc(sizeof(robot_data_t) * number_robots);
}

/* ---------------------------------------------------------------------------
 * networkTest - Test de la red una vez desplegada
 * --------------------------------------------------------------------------- */
static void networkTest(deployment_data_t* d_data_test,player_pose2d_t* positions, int estrategia)
{
	int i,j;

	inicializaDatosDespliegueTest(d_data_test,positions,d_data_test->number_robots,d_data_test->map_size);

	for(i=0;i<d_data_test->number_robots;i++)
	{
		//XXXX. What the fuck???
	}

	double* msj_tx=(double*) malloc(sizeof(double) * d_data_test->number_robots);
	
	clearArrays(d_data_test->p_data->packets_sent,d_data_test->number_robots);
	clearArrays(d_data_test->p_data->own_packets_sent,d_data_test->number_robots);
	clearArrays(d_data_test->p_data->packets_rx,d_data_test->number_robots);
	clearArrays(d_data_test->p_data->own_packets_rx,d_data_test->number_robots);

	printf("Testing the network.\n");

	if(estrategia==NO_STRATEGY)
	{
		printf("Sending a message to each other robot... (strategy = every robot rtx once)\n");
	}
	else if(estrategia==ONLY_IF_CLOSER)
	{
		printf("Sending a message to each other robot... (strategy = every robot rtx once only if closer)\n");

	}

	for(i=0;i<d_data_test->number_robots;i++)
	{
		for(j=0;j<d_data_test->number_robots;j++)
		{
			if(i!=j)
			{
				clearArrays(msj_tx,d_data_test->number_robots);
				if(d_data_test->tipo_red==ADHOC)
				{
					sendAdhoc(d_data_test,i,i,j,msj_tx,estrategia);
					//sendAdhoc(d_data_test,i,i,j,msj_tx,estrategia); //XXX
				}
				else
				{
					sendAPJerq(d_data_test,i,i,j,msj_tx,estrategia);
				}
			}
		}
	}

	for(i=0;i<d_data_test->number_robots;i++)	
	{
		printf("Robot %d: msjs sent %f, own msjs sent %f%%, msjs received %f, own msjs received %f%%\n",i+1,d_data_test->p_data->packets_sent[i],d_data_test->p_data->own_packets_sent[i]*100/(double)(d_data_test->p_data->packets_sent[i]),d_data_test->p_data->packets_rx[i],d_data_test->p_data->own_packets_rx[i]*100/(double)(d_data_test->p_data->packets_rx[i]));
	}


	//XXXX TODO!!!! Coger de aqui
	double mean_packets_sent=0;
	double mean_packets_rx=0;
	double st_dev_packets_sent=0;
	double st_dev_packets_rx=0;

	/* Calcula media y desviacion tipica */
	for(i=0;i<d_data_test->number_robots;i++)	
	{
		mean_packets_sent+=d_data_test->p_data->packets_sent[i];
		mean_packets_rx+=d_data_test->p_data->packets_rx[i];
	}
	mean_packets_sent=mean_packets_sent/d_data_test->number_robots;
	mean_packets_rx=mean_packets_rx/d_data_test->number_robots;

	for(i=0;i<d_data_test->number_robots;i++)	
	{
		st_dev_packets_sent+=pow(d_data_test->p_data->packets_sent[i]-mean_packets_sent,2);
		st_dev_packets_rx+=pow(d_data_test->p_data->packets_rx[i]-mean_packets_rx,2);
	}
	st_dev_packets_sent=sqrt(st_dev_packets_sent/d_data_test->number_robots);
	st_dev_packets_rx=sqrt(st_dev_packets_rx/d_data_test->number_robots);	

	printf("\nmean packets sent %f, standard deviation packets sent %f, mean packets rx %f, standard deviation packets rx %f\n\n",mean_packets_sent,st_dev_packets_sent,mean_packets_rx,st_dev_packets_rx);

	free(msj_tx);
	msj_tx=NULL;
}


//XXXX. Igual que la anterior, pero en lugar de en pantalla, imprime en el fichero
static void networkTestFile(FILE * fichero, deployment_data_t* d_data_test,player_pose2d_t* positions, int estrategia)
{
	int i,j;

	inicializaDatosDespliegueTest(d_data_test,positions,d_data_test->number_robots,d_data_test->map_size);


	double* msj_tx=(double*) malloc(sizeof(double) * d_data_test->number_robots);

	clearArrays(d_data_test->p_data->packets_sent,d_data_test->number_robots);
	clearArrays(d_data_test->p_data->own_packets_sent,d_data_test->number_robots);
	clearArrays(d_data_test->p_data->packets_rx,d_data_test->number_robots);
	clearArrays(d_data_test->p_data->own_packets_rx,d_data_test->number_robots);

	printf("Testing the network.\n");

	if(estrategia==NO_STRATEGY)
	{
		printf("Sending a message to each other robot... (strategy = every robot rtx once)\n");
	}
	else if(estrategia==ONLY_IF_CLOSER)
	{
		printf("Sending a message to each other robot... (strategy = every robot rtx once only if closer)\n");
	}

#ifdef MODELO_TODOS_A_TODOS
	for(i=0;i<d_data_test->number_robots;i++)
	{
		if (!d_data_test->robot_data[i].broken)
		{
			for(j=0;j<d_data_test->number_robots;j++)
			{
				if((i!=j)&&(!d_data_test->robot_data[j].broken))
				{
					clearArrays(msj_tx,d_data_test->number_robots);
					if(d_data_test->tipo_red==ADHOC)
					{
						sendAdhoc(d_data_test,i,i,j,msj_tx,estrategia);
					//sendAdhoc(d_data_test,i,i,j,msj_tx,estrategia); //XXX
					}
					else
					{
						sendAPJerq(d_data_test,i,i,j,msj_tx,estrategia);
					}
				}
			}
		}
	}
#else
	//XXXX. En este caso es redundante, pues esto ya se ha calculado, pero bueno....
	for(i=0;i<d_data_test->number_robots;i++)
	{
		if (!d_data_test->robot_data[i].broken)
		{
			clearArrays(msj_tx,d_data_test->number_robots);
			if(d_data_test->tipo_red==ADHOC)
			{
				if (d_data_test->robot_data[i].level!=1)
				{	//AP no manda
					sendAdhoc(d_data_test,i,i,d_data_test->FinalAP,msj_tx,estrategia);
				}
				//sendAdhoc(d_data_test,i,i,j,msj_tx,estrategia); //XXX
			}
			else
			{
				if (d_data_test->robot_data[i].level==1 || ((d_data_test->robot_data[i].level==0) && (d_data_test->robot_data[i].registered_to!=-1)))
				{  //AP no envia
					sendAPJerq(d_data_test,i,i,d_data_test->FinalAP,msj_tx,estrategia);
				}
				else if (d_data_test->robot_data[i].level!=2)
				{
					sendAdhoc(d_data_test, i, i, -1, msj_tx, 0);
				}
			}
		}
	}
#endif


//	for(i=0;i<d_data_test->number_robots;i++)
//	{
//		printf("Robot %d: msjs sent %f, own msjs sent %f%%, msjs received %f, own msjs received %f%%\n",i+1,d_data_test->p_data->packets_sent[i],d_data_test->p_data->own_packets_sent[i]*100/(double)(d_data_test->p_data->packets_sent[i]),d_data_test->p_data->packets_rx[i],d_data_test->p_data->own_packets_rx[i]*100/(double)(d_data_test->p_data->packets_rx[i]));
//	}


	//XXXX TODO!!!! Coger de aqui
	double mean_packets_sent=0;
	double mean_packets_rx=0;
	double st_dev_packets_sent=0;
	double st_dev_packets_rx=0;


	double mean_packets_sent_noAP=0;
	double st_dev_packets_sent_noAP=0;

	double no_rotos=0.0;

	double min_packets_sent=1e200;
	double max_packets_sent=0;
	double min_packets_sent_noAP=1e200;
	double max_packets_sent_noAP=0;

	/* Calcula media y desviacion tipica, maximo y minimo */
	for(i=0;i<d_data_test->number_robots;i++)
	{
		if (!d_data_test->robot_data[i].broken)
		{
			mean_packets_sent+=d_data_test->p_data->packets_sent[i];
			if (min_packets_sent>d_data_test->p_data->packets_sent[i])
				min_packets_sent=d_data_test->p_data->packets_sent[i];
			if (max_packets_sent<d_data_test->p_data->packets_sent[i])
					max_packets_sent=d_data_test->p_data->packets_sent[i];
			mean_packets_rx+=d_data_test->p_data->packets_rx[i];
			no_rotos+=1.0;
			if (i!=d_data_test->FinalAP)
			{
				if (min_packets_sent_noAP>d_data_test->p_data->packets_sent[i])
					min_packets_sent_noAP=d_data_test->p_data->packets_sent[i];
				if (max_packets_sent_noAP<d_data_test->p_data->packets_sent[i])
						max_packets_sent_noAP=d_data_test->p_data->packets_sent[i];
				mean_packets_sent_noAP+=d_data_test->p_data->packets_rx[i];
			}


		}
	}
	mean_packets_sent=mean_packets_sent/no_rotos; //d_data_test->number_robots;
	mean_packets_sent_noAP=mean_packets_sent_noAP/(no_rotos-1.0); //d_data_test->number_robots;
	mean_packets_rx=mean_packets_rx/no_rotos; //d_data_test->number_robots;

	for(i=0;i<d_data_test->number_robots;i++)
	{
		if (!d_data_test->robot_data[i].broken){
			st_dev_packets_sent+=pow(d_data_test->p_data->packets_sent[i]-mean_packets_sent,2);
			st_dev_packets_rx+=pow(d_data_test->p_data->packets_rx[i]-mean_packets_rx,2);

			if (i!=d_data_test->FinalAP)
			{
				st_dev_packets_sent_noAP+=pow(d_data_test->p_data->packets_sent[i]-mean_packets_sent,2);
			}
		}
	}
	st_dev_packets_sent=sqrt(st_dev_packets_sent)/no_rotos; //d_data_test->number_robots);
	st_dev_packets_sent_noAP=sqrt(st_dev_packets_sent_noAP)/(no_rotos-1.0); //d_data_test->number_robots);
	st_dev_packets_rx=sqrt(st_dev_packets_rx)/no_rotos; //d_data_test->number_robots);

	fprintf(fichero," %lf %lf %lf %lf %lf %lf",mean_packets_sent,mean_packets_rx,st_dev_packets_sent,st_dev_packets_rx,min_packets_sent,max_packets_sent);
	fprintf(fichero," %lf %lf %lf %lf",mean_packets_sent_noAP,st_dev_packets_sent_noAP,min_packets_sent_noAP,max_packets_sent_noAP);

	//printf("\nmean packets sent %f, standard deviation packets sent %f, mean packets rx %f, standard deviation packets rx %f\n\n",mean_packets_sent,st_dev_packets_sent,mean_packets_rx,st_dev_packets_rx);

	free(msj_tx);
	msj_tx=NULL;
}


//TODO XXXX WHat Is Thisssss???
/* ---------------------------------------------------------------------------
 * meanPower - Calcula la potencia media para que un robot transmita un 
 * 	       mensaje al resto
 * --------------------------------------------------------------------------- */
/* Funcion que calcula la potencia media para que se tx con el resto de robots.*/
double meanPower(deployment_data_t* d_data,int my_robot)
{
	int j,received=0,num_received=0;
	double tx_power,mean_power=0;

	double* msj_tx=(double*) malloc(sizeof(double) * d_data->number_robots);

	for(j=0;j<d_data->number_robots;j++)
	{
		tx_power=0;
		if((my_robot!=j)&&(!d_data->robot_data[j].broken))
		{
			clearArrays(msj_tx,d_data->number_robots);
			received=0;
			if(d_data->tipo_red==ADHOC)
			{
				potAdhoc(d_data,my_robot,my_robot,j,msj_tx,&received,&tx_power);
				//potAdhoc(d_data,my_robot,my_robot,j,msj_tx,&received,&tx_power); //XXX
			}
			else
			{
				potAPJerq(d_data,my_robot,my_robot,j,msj_tx,&received,&tx_power);
			} 
			if(received)
			{
				mean_power+=tx_power;
				num_received++;	
			}
		}
	}

	if(num_received!=0)
	{
		mean_power=mean_power/num_received;
	}

	free(msj_tx);
	msj_tx=NULL;	

	return mean_power;
}

/* ---------------------------------------------------------------------------
 * netMeanPower - Calcula la potencia media en la red
 * --------------------------------------------------------------------------- */
double netMeanPower(deployment_data_t* d_data,int verbose)
{
	int i,num_sum=0;
	double mean_power;
	double network_mean_power=0;

	if(verbose)
	{
		printf("Calculating mean power to transmit from each robot and total mean power\n");
	}	
	
	for(i=0;i<d_data->number_robots;i++)
	{
		mean_power=meanPower(d_data,i);
		if(verbose)
		{
			printf("Robot %d: mean power to transmit to the rest of robots = %f\n",i,mean_power);
		}	
		if(mean_power!=0)
		{	
			network_mean_power+=mean_power;
			num_sum++;
		}
	}

	if(num_sum!=0)
	{
		network_mean_power=network_mean_power/num_sum;
	}

	if(verbose)	
	{
		printf("Mean power of the network = %f\n",network_mean_power);
	}
	
	return network_mean_power;
}

/* ---------------------------------------------------------------------------
 * showGraph - Dibuja un grafico
 * --------------------------------------------------------------------------- */
static void showGraph(FILE* gnuplotPipe,double* y_data,int x_range,char* title,char* xlabel,char* ylabel,char* temp_name,char* with)
{
	char cadena_title[100] = "set title ";
	strcat(cadena_title,title);

	char cadena_xlabel[100] = "set xlabel ";
	strcat(cadena_xlabel,xlabel);

	char cadena_ylabel[100] = "set ylabel ";
	strcat(cadena_ylabel,ylabel);

	char cadena_xrange[100] ="set xrange [-1:";
	char str[5];
	sprintf(str, "%d", x_range+1);
	strcat(cadena_xrange,str);
	strcat(cadena_xrange,"]");

	char cadena_yrange[100] ="set yrange [0:";
	sprintf(str, "%ld", (long int)(maximo(y_data)*1.3));
	strcat(cadena_yrange,str);
	strcat(cadena_yrange,"]");

	char cadena_temp_name[100];
	strcpy(cadena_temp_name,temp_name); 
	strcat(cadena_temp_name,".temp");

	char cadena_with[100] = "plot '";
	strcat(cadena_with,cadena_temp_name);	
	strcat(cadena_with,"' with ");
	strcat(cadena_with,with);

	char * commandsForGnuplot[] = {cadena_title, cadena_xlabel, cadena_ylabel,cadena_xrange,cadena_with}; //"set yrange [0:25]"
	FILE * temp = fopen(cadena_temp_name, "w");
	
	int i;
	for (i=0; i < x_range; i++)
	{
		fprintf(temp, "%lf %lf \n", (double)i+1, (double)y_data[i]); //Write the data to a temporary file
	}

	for (i=0; i < sizeof(commandsForGnuplot)/sizeof(char*); i++)
	{
		fprintf(gnuplotPipe, "%s \n", commandsForGnuplot[i]); //Send commands to gnuplot one by one.
		fflush(gnuplotPipe);
	}

	fclose(temp);

}

/* ---------------------------------------------------------------------------
 * replotGraph - Dibuja encima de un grafico
 * --------------------------------------------------------------------------- */
static void replotGraph(FILE* gnuplotPipe,double* y_data,int x_range,int y_range,char* title,char* temp_name,char* with)
{
	char cadena_title[50] = "set title ";
	strcat(cadena_title,title);

	char cadena_temp_name[50];
	strcpy(cadena_temp_name,temp_name); 
	strcat(cadena_temp_name,".temp");

	char cadena_with[50] = "replot '";
	strcat(cadena_with,cadena_temp_name);	
	strcat(cadena_with,"' with ");
	strcat(cadena_with,with);

	char cadena_xrange[100] ="set xrange [-1:";
	char str[5];
	sprintf(str, "%d", x_range+1);
	strcat(cadena_xrange,str);
	strcat(cadena_xrange,"]");

	char cadena_yrange[100] ="set yrange [-1:";
	sprintf(str, "%d", y_range);
	strcat(cadena_yrange,str);
	strcat(cadena_yrange,"]");

	char * commandsForGnuplot[] = {cadena_title,cadena_with};
	FILE * temp = fopen(cadena_temp_name, "w");
	FILE * temp2;
	int i;
	for (i=0; i < x_range; i++)
	{
		fprintf(temp, "%lf %lf \n", (double)i+1, (double)y_data[i]); //Write the data to a temporary file
	}

	for (i=0; i < sizeof(commandsForGnuplot)/sizeof(char*); i++)
	{
		fprintf(gnuplotPipe, "%s \n", commandsForGnuplot[i]); //Send commands to gnuplot one by one.
		fflush(gnuplotPipe);
	}

	fclose(temp);
}

/* ---------------------------------------------------------------------------
 * signalIntensity - Intensidad de la señal recibida
 * --------------------------------------------------------------------------- */
static double uniformity(deployment_data_t* d_data)
{
	double uniformity=0,net_uniformity=0,d_media,d;
	double unbroken_robots=0.0;
	int i,j,k;

	for(i=0;i<d_data->number_robots;i++)
	{
		k=0;
		d_media=0;

		if (!d_data->robot_data[i].broken) /* XXXX */
		{
			// Calculo total nodos cercanos y distancia media a ellos
			for(j=0;j<d_data->number_robots;j++)
			{
				d=calculateDistance2(d_data->p_data,i,j);
				if(d<=MIN_WIFI_DISTANCE)
				{
					k++;
					d_media+=d;
				}
			}
			d_media=d_media/k;

			// Calculo uniformidad de i
			for(j=0;j<d_data->number_robots;j++)
			{
				if (!d_data->robot_data[j].broken) /* XXXX */
				{
					d=calculateDistance2(d_data->p_data,i,j);
					if(d<=MIN_WIFI_DISTANCE)
					{
						uniformity+=pow(d-d_media,2);
					}
				}
			}
			uniformity=sqrt(uniformity/k);

			net_uniformity+=uniformity;

			unbroken_robots+=1.0; //XXXX
		}
	}
	net_uniformity=net_uniformity/unbroken_robots; //XXXX.../d_data->number_robots;

	return net_uniformity;
}

/* ---------------------------------------------------------------------------
 * ---------------------------------------------------------------------------
 *
 * simAdhocBA - Función principal del menu de resultados del despliegue.
 *
 * Parametros:
 *		d_data: Datos sobre el despliegue de la red
 *		sim_robots: Proxys de los robots
 *		sim_position2d: Proxys a sus interfaces de posicion
 *		sim_graphics2d: Proxys a sus interfaces de gráficos
 * 
 * --------------------------------------------------------------------------- 
 * --------------------------------------------------------------------------- */
void rendimiento(deployment_data_t* d_data, playerc_client_t **sim_robots, playerc_position2d_t** sim_position2d, playerc_graphics2d_t** sim_graphics2d)
{
	int i,j,estrategia,salir=0;
	int i_reader=-1;
	int number_robots=d_data->number_robots;

	while(salir==0)
	{
		system("clear");
		playerc_client_read(*sim_robots);
		for(i=0;i<number_robots;i++)
		{
			dibuja(d_data->robot_data, sim_position2d, sim_graphics2d, i, number_robots, d_data->tipo_red,d_data->algoritmo);
		}
		printf ("------------------  MENU ESTADISTICAS  ------------------\n");
		printf("Seleccione que desea mostrar:\n");
		printf("0) Duracción del despliegue y coeficiente de uniformidad.\n");
		printf("1) Calcular y dibujar área de cobertura.\n");
		printf("2) Distancia recorrida por cada robot.\n");
		printf("3) %% Paquetes enviados y recibidos durante el despliegue.\n");
		printf("4) %% Paquetes enviados y recibidos con la red desplegada\n");
		printf("5) Potencia media en la red y de cada robot.\n");
		printf("6) Volver al menu principal.\n");

		fflush(stdin);
		scanf("%d",&i_reader);
			
		switch(i_reader)
		{
			case 0:
				printf("Time of deployment: %f\n\n", d_data->p_data->depl_time);
				printf("Uniformity: %lf\n\n", uniformity(d_data));
				makePause();
				break;
		
			case 1:
			{
				printf("Calculating coverage... \n");
				double relative_coverage=calculateCoverage(d_data); 
				double total_coverage=relative_coverage*d_data->map_size;
				printf("Total coverage: %f m\n", total_coverage);
				printf("Coverage relative to the size of the map: %f %%\n", relative_coverage*100);

				printf("\nPloting covered area of each robot...\n");
				dibujaCoverage(d_data,sim_robots,sim_graphics2d);
				makePause();
				break;
			}
			case 2:
			{
				printf("Plotting distance travelled by each robot during deployment\n");
				double mean_dist=0;
				for(i=0;i<number_robots;i++)	
				{
					printf("Robot %d: distance walked %f\n",i+1,d_data->p_data->distance_walked[i]);
					mean_dist+=d_data->p_data->distance_walked[i];
				}
				mean_dist=mean_dist/20;	//XXXX Dividido por 20????? ¿¿¿No seria por number_robots????
				FILE * gnuplotPipe = popen ("gnuplot", "w");
				showGraph(gnuplotPipe,d_data->p_data->distance_walked,number_robots,"\"Distancia recorrida\"","\"Robot\"","\"Distancia (m)\"","distance","boxes");
				printf("Distancia media recorrida por la red: %lf \n",mean_dist);
				makePause();
				fclose(gnuplotPipe);
				break;
			}
			case 3:
			{
				for(i=0;i<number_robots;i++)	
				{
					printf("Robot %d: msjs sent %f, own msjs sent %f%%, msjs received %f, own msjs received %f%%\n",i+1,d_data->p_data->packets_sent[i],d_data->p_data->own_packets_sent[i]*100/(double)(d_data->p_data->packets_sent[i]),d_data->p_data->packets_rx[i],d_data->p_data->own_packets_rx[i]*100/(double)(d_data->p_data->packets_rx[i]));
				}
				printf("Plotting packets sent by each robot during deployment\n");
				FILE * gnuplotPipe = popen ("gnuplot -", "w");
				showGraph(gnuplotPipe,d_data->p_data->packets_sent,number_robots,"\"Paquetes enviados\"","\"Robot\"","\"Paquetes\"","enviados","boxes");
				replotGraph(gnuplotPipe,d_data->p_data->own_packets_sent,number_robots,(int)(maximo(d_data->p_data->packets_sent)),"\"Paquetes enviados propios\"","enviados propios","impulses");
				FILE * gnuplotPipe2 = popen ("gnuplot -", "w");
				printf("Plotting packets received by each robot during deployment\n");
				showGraph(gnuplotPipe2,d_data->p_data->packets_rx,number_robots,"\"Paquetes recibidos\"","\"Robot\"","\"Paquetes\"","recibidos","boxes");
				replotGraph(gnuplotPipe2,d_data->p_data->own_packets_rx,number_robots,(int)(maximo(d_data->p_data->packets_rx)),"\"Paquetes recibidos propios\"","recibidos propios","impulses");
				makePause();
				fclose(gnuplotPipe);
				fclose(gnuplotPipe2);
				break;
			}
			case 4:
				printf("Seleccione una estrategia de enrutamiento:\n");
				printf("0) Sin estrategia. Todos los robots reenvían los mensajes recibidos una vez.\n");
				printf("1) Reenviar mensaje una vez solo si estoy mas cerca del objetivo.\n");	
				fflush(stdin);
				scanf("%d",&i_reader);
				while(i_reader<0||i_reader>1)
				{
					printf("Introduzca una de esas opciones, por favor...\n");
					fflush(stdin);
					scanf("%d",&i_reader);
				}		
				
				deployment_data_t dummy;	
				deployment_data_t* d_data_test=&dummy;

				performance_data_t dummy2;
				d_data_test->p_data=&dummy2;
				d_data_test->number_robots=d_data->number_robots;
				d_data_test->map_size=d_data->map_size;
				d_data_test->robot_data=d_data->robot_data;
				d_data_test->tipo_red=d_data->tipo_red;
				d_data_test->algoritmo=d_data->algoritmo;

				estrategia=i_reader;	
				networkTest(d_data_test,d_data->p_data->positions,estrategia);
				printf("Plotting packets sent by each robot during test\n");
				FILE * gnuplotPipe = popen ("gnuplot -", "w");
				showGraph(gnuplotPipe,d_data_test->p_data->packets_sent,number_robots,"\"Paquetes enviados\"","\"Robot\"","\"Paquetes\"","enviados","boxes");
				replotGraph(gnuplotPipe,d_data_test->p_data->own_packets_sent,number_robots,(int)(maximo(d_data_test->p_data->packets_sent)),"\"Paquetes enviados propios\"","enviados propios","impulses");
				FILE * gnuplotPipe2 = popen ("gnuplot -", "w");
				printf("Plotting packets received by each robot during test\n");
				showGraph(gnuplotPipe2,d_data_test->p_data->packets_rx,number_robots,"\"Paquetes recibidos\"","\"Robot\"","\"Paquetes\"","recibidos","boxes");
				replotGraph(gnuplotPipe2,d_data_test->p_data->own_packets_rx,number_robots,(int)(maximo(d_data_test->p_data->packets_rx)),"\"Paquetes recibidos propios\"","recibidos propios","impulses");
				makePause();
				fclose(gnuplotPipe);
				fclose(gnuplotPipe2);
				break;
			case 5:
			{
				printf("Seleccione que desea mostrar:\n");
				printf("0) Potencia media en la red durante el despliegue.\n");
				printf("1) Potencia media de cada robot.\n");	
				fflush(stdin);
				scanf("%d",&i_reader);
				while(i_reader<0||i_reader>1)
				{
					printf("Introduzca una de esas opciones, por favor...\n");
					fflush(stdin);
					scanf("%d",&i_reader);
				}
				FILE * gnuplotPipe = popen ("gnuplot -", "w");
				if(i_reader==0)
				{
					printf("Plotting mean net power during deployment\n");
					showGraph(gnuplotPipe,d_data->p_data->net_mean_power,d_data->p_data->num_iterations/10,"\"Potencia media en la red durante el despliegue\"","\"Iteracion/10 \"","\"Potencia(mW)\"","potencia red","lines");
					printf("Potencia media en la red final: %lf \n",d_data->p_data->net_mean_power[d_data->p_data->num_iterations/10-1]);
				}
				else
				{
					printf("Plotting mean power of each robot\n");
					double* mean_power=(double*)malloc(sizeof(double)*number_robots);
					for(i=0;i<number_robots;i++)
					{
						mean_power[i]=meanPower(d_data,i);
					}
					showGraph(gnuplotPipe,mean_power,number_robots,"\"Potencia media de cada robot\"","\"Robot\"","\"Potencia(mW)\"","potencia","boxes");
					free(mean_power);
				}	
				makePause();
				fclose(gnuplotPipe);	
				break;
			}
			case 6:
				//4) Store data for general statistics.
				//printf("Storing data for further statistics.\n");//TODO
				salir=1;
				break;
			default:
				break;
		}
	}
}


void save_performance(FILE *  fichero,deployment_data_t *d_data)
{

//XXXX TODO ver los parametros que hay que pasar aqui....
	//TODO. quitar los nodos rotos de las estadisticas....!!!!

	int i=0;
	double relative_coverage=calculateCoverage(d_data);
	double total_coverage=relative_coverage*d_data->map_size;
	double uniformidad=uniformity(d_data);
	double mean_dist=0.0;
	double robots_alive=0.0;
	double mean_power=0.0;

	for(i=0;i<d_data->number_robots;i++)
	{

		if (!d_data->robot_data[i].broken)
		{
			mean_dist+=d_data->p_data->distance_walked[i];
			mean_power+=meanPower(d_data,i); //XXXX
			robots_alive+=1.0;
		}
	}
	mean_dist=mean_dist/robots_alive; ///¿¿¿XXXX por que se dividia por 20 en el original??
	mean_power=mean_power/robots_alive;
	//Falta por calcular la potencia y las varianzas...

	clearArrays(d_data->p_data->packets_sent,d_data->number_robots);
	clearArrays(d_data->p_data->own_packets_sent,d_data->number_robots);
	clearArrays(d_data->p_data->packets_rx,d_data->number_robots);
	clearArrays(d_data->p_data->own_packets_rx,d_data->number_robots);
	clearArrays(d_data->p_data->wasted_energy,d_data->number_robots);

	//Imprime algunas estadisticas
	fprintf(fichero," %lf \t %lf \t %lf \t %lf \t %lf \t %lf ",relative_coverage,total_coverage,uniformidad,mean_dist,robots_alive,mean_power);


	//XXXX. WHy copy?????
	deployment_data_t dummy;
	deployment_data_t* d_data_test=&dummy;

	performance_data_t dummy2;
	d_data_test->p_data=&dummy2;
	d_data_test->number_robots=d_data->number_robots;
	d_data_test->map_size=d_data->map_size;
	d_data_test->robot_data=d_data->robot_data;
	d_data_test->tipo_red=d_data->tipo_red;
	d_data_test->algoritmo=d_data->algoritmo;
	d_data_test->FinalAP=d_data->FinalAP;

	//Calcula e imprime las estadisticas de media y varianza de paquetes
	networkTestFile(fichero,d_data_test,d_data->p_data->positions,ONLY_IF_CLOSER); //Estrategia "Only if closer"

	fprintf(fichero,"\n");
	fflush(fichero);

}

