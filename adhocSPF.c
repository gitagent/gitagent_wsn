/*---------------------------------------------------------------------------
 *  Proyecto Fin de Carrera: Enjambre de microbots para despliegue autónomo de 
 *                           una red inalámbrica mallada.
 * 
 *  Francisco José Aguilera Leal
 *  Universidad de Málaga
 * --------------------------------------------------------------------------- 
 * Despliegue de red jerarquizada con Access Point.
 * Uso de los campos de potencial sociales.
 * ---------------------------------------------------------------------------
*/  

#include "adhocSPF.h"
#include "APjer.h" //Para aprovechar la funcion ChooseAP


/* ---------------------------------------------------------------------------
 * changeBroken - Modifica el estado de un robot (roto o funcionando).
 * --------------------------------------------------------------------------- */
static void changeBroken(playerc_position2d_t** sim_position2d, robot_data_t* robot_data, int my_robot, int number_robots)
{
	int i;
	if(my_robot>=0 && my_robot < number_robots)
	{
		if(robot_data[my_robot].broken==0)
		{
			robot_data[my_robot].broken=1;
			robot_data[my_robot].stopped=1;
		}
		else
		{
			robot_data[my_robot].broken=0;
			robot_data[my_robot].stopped=0;
		}
	}
}

static void BreakSPF(playerc_position2d_t** sim_position2d, robot_data_t* robot_data, int my_robot, int number_robots,int *AP)
{
	int i;
	if(my_robot>=0 && my_robot < number_robots)
	{
		if(robot_data[my_robot].broken==0)
		{
			robot_data[my_robot].broken=1;
			robot_data[my_robot].stopped=1;
		}
	}

	if (*AP==my_robot)
	{
		*AP=chooseAP(sim_position2d, robot_data,number_robots,0,-1);
	}
//		else
//		{
//			robot_data[my_robot].broken=0;
//			robot_data[my_robot].stopped=0;
//		}
//	}
}

/* ---------------------------------------------------------------------------
 * potentialLaw - Calcula la fuerza
 * --------------------------------------------------------------------------- */
static double potentialLaw(double distance, double c1, double c2, double sigma1, double sigma2, double xoff, double yoff)
{
	double f = -c1/pow((distance+xoff),sigma1) + c2/pow((distance+xoff),sigma2) + yoff;
	return f;
}

/* ---------------------------------------------------------------------------
 * vectorTotal - Calcula el vector de fuerza ejercido por la red sobre
 *		 el robot
   --------------------------------------------------------------------------- */
static void vectorTotal(playerc_position2d_t** sim_position2d, robot_data_t* robot_data, int number_robots, int num_rotos, int map_size, int my_robot, double* x, double* y,int obstaculos)
{
	double f,d,c1,c2,sigma1,sigma2,xoff,yoff;
	int j,no_moverse=0;
	int i=0;
	int cercanos=0;
	
	*x=0;
	*y=0;

//	if (i==0)
//	{
//		printf("Help me");
//	}


	// Dibuja lineas hasta los robots en rango
	for(i=0;i<number_robots;i++)
	{
		double distance=sqrt(pow(sim_position2d[my_robot]->px-sim_position2d[i]->px,2)+pow(sim_position2d[my_robot]->py-sim_position2d[i]->py,2));
		if(i!=my_robot && distance<=RADIO_COVERAGE_SPF && !robot_data[i].broken)
		{
			cercanos++;
		}
	}


	//1) Esta roto?
	if((robot_data[my_robot].broken==1)||(robot_data[my_robot].level!=0))
	{
		// De momento estoy quieto. (Luego lo movere hacia abajo)
		no_moverse=1;

		//XXXX. Añadida condicion de forma que si es el AP, tampoco se mueve

	}

	else
	{
		for(j=0;j<number_robots;j++) 
		{	
			// Se aleja 2 m de los que no estan rotos
			if (my_robot!=j)
			{
				d=calculateDistance(sim_position2d, j, my_robot);

				if( robot_data[j].broken==0)
				{
					f=0;


					//Fuerza de repulsion hasta 1.5 m
					c1=20;
					c2=0;
					sigma1=7;
					sigma2=1; //Anulado
					xoff=0;
					yoff=0;
					f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
				}
				// Intenta no chocarse con los rotos
				else
				{
					// Repulsion hasta 0.4 m
					c1=0.001;
					c2=0;
					sigma1=8;
					sigma2=1;// Da igual este termino se anula
					xoff=0;
					yoff=0;
					f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);

				}

				*x=*x+f*(sim_position2d[j]->px - sim_position2d[my_robot]->px)/d;
				*y=*y+f*(sim_position2d[j]->py - sim_position2d[my_robot]->py)/d; //XXXX. Se divide por d???

				//Fuerza adicional de cohesion de la red
				if (robot_data[j].level>0)
				{
					f=20*((double)num_rotos/(double)number_robots)/((double)(cercanos+1));
					*x=*x+f*(sim_position2d[j]->px - sim_position2d[my_robot]->px);///d;
					*y=*y+f*(sim_position2d[j]->py - sim_position2d[my_robot]->py);///d; //XXXX. Se divide por d???
				}



			}


			//XXXX, PEDAZO DE ERROR QUE HABÏA AQUI. AL ESTAR ESTO FUERA del for
			//El nodo 0 daba NAN (divide por 0 la primera vez)
			//Para el resto, se alteraba el vector de fuerza
//			*x=*x+f*(sim_position2d[j]->px - sim_position2d[i]->px)/d;
//			*y=*y+f*(sim_position2d[j]->py - sim_position2d[i]->py)/d;
		}

	}

	// Repulsion de los muros laterales hasta 0.4 m.
	c1=0.001;
	c2=0;
	sigma1=8;
	sigma2=1; // Da igual este termino se anula
	xoff=0;
	yoff=0;

	// Muro norte:
	d=fabs(map_size/2-sim_position2d[my_robot]->py);
	f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
	*y=*y+f*(map_size/2 - sim_position2d[my_robot]->py)/d;

	// Muro sur:	
	d=fabs(-map_size/2-sim_position2d[my_robot]->py);
	f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
	*y=*y+f*(-map_size/2-sim_position2d[my_robot]->py)/d;

	// Muro oeste:
	d=fabs(-map_size/2-sim_position2d[my_robot]->px);
	f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
	*x=*x+f*(-map_size/2-sim_position2d[my_robot]->px)/d;

	// Muro este:
	d=fabs(map_size/2-sim_position2d[my_robot]->px);
	f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
	*x=*x+f*(map_size/2 - sim_position2d[my_robot]->px)/d;

	// Escenario de obstaculos 1
	if(obstaculos==1)
	{
		// Repulsion muros interiores hasta 0.4 m
		c1=0.001; //XXXX. Aumentado (era 0.001) a ver si los obstaculos repelen un poco mas
		c2=0;
		sigma1=12;	//XXXX ¿12?¿Diferente a la otra (muertos y muros fronterizos)?
		sigma2=1;// Da igual este termino se anula
		xoff=0;
		yoff=0;

		// Muro superior
		if(sim_position2d[my_robot]->px>-0.5*map_size/5 && sim_position2d[my_robot]->px<0.5*map_size/5)
		{
			d=fabs(map_size/5-sim_position2d[my_robot]->py);
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*y=*y+f*(map_size/5-sim_position2d[my_robot]->py)/d;
		}
		else if(sim_position2d[my_robot]->px<-0.5*map_size/5)
		{
			d=sqrt(pow(-0.5*map_size/5-sim_position2d[my_robot]->px,2)+pow(map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(-0.5*map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(map_size/5-sim_position2d[my_robot]->py)/d;
		}
		else if(sim_position2d[my_robot]->px>0.5*map_size/5)
		{
			d=sqrt(pow(0.5*map_size/5-sim_position2d[my_robot]->px,2)+pow(map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(0.5*map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(map_size/5-sim_position2d[my_robot]->py)/d;
		}

		// Muro central
		if(sim_position2d[my_robot]->py<0 && sim_position2d[my_robot]->py>-0.5*map_size/5)
		{
			d=fabs(0-sim_position2d[my_robot]->px);
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(0-sim_position2d[my_robot]->px)/d;
		}
		else if(sim_position2d[my_robot]->py<-0.5*map_size/5)
		{
			d=sqrt(pow(0-sim_position2d[my_robot]->px,2)+pow(-0.5*map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(0-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(-map_size/5-sim_position2d[my_robot]->py)/d;
		}
		else if(sim_position2d[my_robot]->py>0)
		{
			d=sqrt(pow(0-sim_position2d[my_robot]->px,2)+pow(0-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(0-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(0-sim_position2d[my_robot]->py)/d;
		}

		// Muro derecho
		if(sim_position2d[my_robot]->py<-0.5*map_size/5 && sim_position2d[my_robot]->py>-1.5*map_size/5)
		{
			d=fabs(1.5*map_size/5-sim_position2d[my_robot]->px);
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(1.5*map_size/5-sim_position2d[my_robot]->px)/d; //XXXX. Aqui ponia 1-5 en lugar de 1.5
		}
		else if(sim_position2d[my_robot]->py>-0.5*map_size/5)
		{
			d=sqrt(pow(1.5*map_size/5-sim_position2d[my_robot]->px,2)+pow(-0.5*map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(1.5*map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(-0.5*map_size/5-sim_position2d[my_robot]->py)/d;
		}
		else if(sim_position2d[my_robot]->py<-1.5*map_size/5)
		{
			d=sqrt(pow(1.5*map_size/5-sim_position2d[my_robot]->px,2)+pow(-1.5*map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(1.5*map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(-1.5*map_size/5-sim_position2d[my_robot]->py)/d;
		}

		// Muro izquierdo
		if(sim_position2d[my_robot]->px>-1.5*map_size/5 && sim_position2d[my_robot]->px<-0.5*map_size/5)
		{
			d=fabs(-1.5*map_size/5-sim_position2d[my_robot]->py);
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*y=*y+f*(-1.5*map_size/5-sim_position2d[my_robot]->py)/d; //XXXX ¿modificar de 1.5 a 1.57?
		}
		else if(sim_position2d[my_robot]->px<-1.5*map_size/5)
		{
			d=sqrt(pow(-1.5*map_size/5-sim_position2d[my_robot]->px,2)+pow(-1.5*map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(-1.5*map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(-1.5*map_size/5-sim_position2d[my_robot]->py)/d;
		}
		else if(sim_position2d[my_robot]->px>-0.5*map_size/5)
		{
			d=sqrt(pow(-0.5*map_size/5-sim_position2d[my_robot]->px,2)+pow(-1.5*map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(-0.5*map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(-1.5*map_size/5-sim_position2d[my_robot]->py)/d;
		}
	}	

	// Escenario de obstaculos 2
	if(obstaculos==2)
	{
		// Repulsion muros interiores hasta 0.4 m
		c1=0.001;
		c2=0;
		sigma1=12;		//XXXX. ¿¿¿Por qué diferente al de los otros???
		sigma2=1;// Da igual este termino se anula
		xoff=0;
		yoff=0;

		// Muro superior izquierdo
		if(sim_position2d[my_robot]->py<1.5*map_size/5 && sim_position2d[my_robot]->py>map_size/5)
		{
			d=fabs(-map_size/5-sim_position2d[my_robot]->px);
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(-map_size/5-sim_position2d[my_robot]->px)/d;
		}
		else if(sim_position2d[my_robot]->py>1.5*map_size/5)
		{
			d=sqrt(pow(-map_size/5-sim_position2d[my_robot]->px,2)+pow(1.5*map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(-map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(1.5*map_size/5-sim_position2d[my_robot]->py)/d;
		}
		else if(sim_position2d[my_robot]->py<map_size/5)
		{
			d=sqrt(pow(-map_size/5-sim_position2d[my_robot]->px,2)+pow(map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(-map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(map_size/5-sim_position2d[my_robot]->py)/d;
		}

		// Muro derecho
		if(sim_position2d[my_robot]->px>0 && sim_position2d[my_robot]->px<map_size/5)
		{
			d=fabs(0-sim_position2d[my_robot]->py);
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*y=*y+f*(0-sim_position2d[my_robot]->py)/d;
		}
		else if(sim_position2d[my_robot]->px<0)
		{
			d=sqrt(pow(0-sim_position2d[my_robot]->px,2)+pow(0-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(0-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(0-sim_position2d[my_robot]->py)/d;
		}
		else if(sim_position2d[my_robot]->px>map_size/5)
		{
			d=sqrt(pow(map_size/5-sim_position2d[my_robot]->px,2)+pow(0-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(0-sim_position2d[my_robot]->py)/d;
		}

		// Muro inferior izquierdo
		if(sim_position2d[my_robot]->py>-1.5*map_size/5 && sim_position2d[my_robot]->py<-map_size/5)
		{
			d=fabs(-map_size/5-sim_position2d[my_robot]->px);
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(-map_size/5-sim_position2d[my_robot]->px)/d;
		}
		else if(sim_position2d[my_robot]->py<-1.5*map_size/5)
		{
			d=sqrt(pow(-map_size/5-sim_position2d[my_robot]->px,2)+pow(-1.5*map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(-map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(-1.5*map_size/5-sim_position2d[my_robot]->py)/d;
		}
		else if(sim_position2d[my_robot]->py>-map_size/5)
		{
			d=sqrt(pow(-map_size/5-sim_position2d[my_robot]->px,2)+pow(-map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(-map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(-map_size/5-sim_position2d[my_robot]->py)/d;
		}
	}

	if(no_moverse)
	{
		*x=0;
		*y=0;
	}
}

/* ---------------------------------------------------------------------------
 * moverse - Controla el movimiento de los robots
 * --------------------------------------------------------------------------- */
static void moverse(playerc_position2d_t** sim_position2d, robot_data_t* robot_data, int my_robot, int number_robots, int num_rotos, int map_size,int obstaculos)
{	
	double x;
	double y;
	double modulo;
	double angulo;
	double my_angle=sim_position2d[my_robot]->pa;


	//Calcula el vector resultante de todas las fuerzas
	vectorTotal(sim_position2d,robot_data,number_robots,num_rotos,map_size,my_robot,&x,&y,obstaculos);
		
	//Solo si el modulo de ese vector es mayor que un cierto valor, muevo al robot
	modulo=sqrt(pow(x,2)+pow(y,2));

	//if (my_robot==0) printf("vect %f %f ",x,y); XXXX DEPURACION

	if (modulo<UMBRAL_MOVIMIENTO)
	{
		playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,0,0);
		playerc_position2d_set_cmd_car(sim_position2d[my_robot],0,0);

		//XXXX: Faltaba aqui????...
		robot_data[my_robot].stopped=1;

	}
	else
	{

		robot_data[my_robot].stopped=0; //XXXX. Faltaba aqui!!!

		//Calcula el ángulo
		angulo = atan2(y/modulo,x/modulo); //Devuelve un valor entre -pi/2 y pi/2

		if(fabs(my_angle-angulo) > 0.1)
		{			
			// Modo giro
			playerc_position2d_set_cmd_car(sim_position2d[my_robot],0,0);

			if(angulo>0)
			{
				if(my_angle<0)
				{
					if (my_angle+3.1415<angulo)
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,-0.5,0); //giro agujas relog
					}
					else
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,0.5,0); //giro contrario 
					}
				}
				else
				{
					if (my_angle>angulo)
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,-0.5,0); //giro agujas relog
					}
					else
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,0.5,0); //giro contrario 
					}
				}
			}
			else
			{
				if(my_angle>0)
				{
					if (my_angle-3.1415<angulo)
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,-0.5,0); //giro agujas relog
					}
					else
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,0.5,0);//giro contrario 
					}
				}
				else
				{
					if (my_angle>angulo)
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,-0.5,0); //giro agujas relog
					}
					else
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,0.5,0);//giro contrario 
					}
				}
			}
		}				
			// Modo coche
/*
			double movement=0.05*sin(my_angle-angulo);

			if(angulo>0)
			{
				if(my_angle<0)
				{
					if (my_angle+3.1415<angulo)
					{
						playerc_position2d_set_cmd_car(sim_position2d[my_robot],movement*(movement>0),-0.5);			
					}
					else
					{
						playerc_position2d_set_cmd_car(sim_position2d[my_robot],movement*(movement>0),0.5);			
					}
				}
				else
				{
					if (my_angle>angulo)
					{
						playerc_position2d_set_cmd_car(sim_position2d[my_robot],movement*(movement>0),-0.5);			
					}
					else
					{
						playerc_position2d_set_cmd_car(sim_position2d[my_robot],movement*(movement>0),0.5);			
					}
				}
			}
			else
			{
				if(my_angle>0)
				{
					if (my_angle-3.1415<angulo)
					{
						playerc_position2d_set_cmd_car(sim_position2d[my_robot],movement*(movement>0),-0.5);			
					}
					else
					{
						playerc_position2d_set_cmd_car(sim_position2d[my_robot],movement*(movement>0),0.5);			
					}
				}
				else
				{
					if (my_angle>angulo)
					{
						playerc_position2d_set_cmd_car(sim_position2d[my_robot],movement*(movement>0),-0.5);			
					}
					else
					{ 
						playerc_position2d_set_cmd_car(sim_position2d[my_robot],movement*(movement>0),0.5);			
					}
				}
			}
		}
*/
		else
		{
			playerc_position2d_set_cmd_car(sim_position2d[my_robot],0.05,0);
		}
	}
}		

/* ---------------------------------------------------------------------------
 * ---------------------------------------------------------------------------
 *
 * simAdhocSPF - Función principal que controla el despliegue.
 *
 * Parametros:
 *		d_data: Datos sobre el despliegue de la red
 *		rotos: Arrays de robots rotos al comienzo
 *		obstaculos: Referencia para los obstaculos del mundo
 *		sim_robots: Proxys de los robots
 *		sim_position2d: Proxys a sus interfaces de posicion
 *		simulation: Proxy a la interfaz simulacion
 *		sim_graphics2d: Proxys a sus interfaces de gráficos
 * 
 * --------------------------------------------------------------------------- 
 * --------------------------------------------------------------------------- */
deployment_data_t* simAdhocSPF(deployment_data_t* d_data,int* rotos,int obstaculos,playerc_client_t **sim_robots, playerc_position2d_t** sim_position2d,playerc_simulation_t** simulation, playerc_graphics2d_t** sim_graphics2d,playerc_sonar_t** sim_sonar)
{
	//Declaro variables 
	int i,contador=0,stop=0,n=0,stopped=0,LastStopped=0;
	srand(time(NULL));

	double mean_power;

	int reader,salir,level;	

	int number_robots=d_data->number_robots;
	int map_size=d_data->map_size;

	double* msj_tx=(double*) malloc(sizeof(double) * number_robots);

	//Inicializo variables
	for(i=0;i<number_robots;i++)
	{
		if(d_data->nueva_simulacion)
		{
			//Variables algoritmo
			d_data->robot_data[i].turn=0;
			d_data->robot_data[i].turning_count=0;
			d_data->robot_data[i].walking_count=0;
			d_data->robot_data[i].backbone=0;
			d_data->robot_data[i].stopped=0;
			d_data->robot_data[i].level=0;
			d_data->robot_data[i].registered_to=-1;
			d_data->robot_data[i].num_registered=0;
			d_data->p_data->distance_walked[i]=0; //XXXX. Movido aqui
			d_data->robot_data[i].remaining_battery=ROBOT_INITIAL_BATTERY;
			d_data->FinalAP=-1;
		}
		else
		{
			if(rotos!=NULL)
			{
				if (rotos[i])
				{
					//XXXX. Acumulativo
					BreakSPF(sim_position2d,d_data->robot_data,i,number_robots,&(d_data->FinalAP));


				}
			}
		}
//		else
//		{
//			d_data->robot_data[i].broken|=0;
//		}

		//Variables para medir rendimiento
		//d_data->p_data->distance_walked[i]=0;//XXXX. Solo si nueva
		d_data->p_data->packets_sent[i]=0;
		d_data->p_data->own_packets_sent[i]=0;
		d_data->p_data->packets_rx[i]=0;
		d_data->p_data->own_packets_rx[i]=0;

	}

	//Robots tell where they are, pose and odometrics and move a bit 
	for(i=0;i<number_robots;i++) 
	{	

		if (d_data->nueva_simulacion)
		{
			//XXXX ¿necesario si no nueva simulacion???
			//En caso de serlo, hacerlo solo para los no rotos que no sean AP
//			if (!d_data->robot_data[i].broken)
//			{
				playerc_position2d_set_cmd_car(sim_position2d[i],0.005,0);
//			}
		}
		if(i%10==0) playerc_client_read(*sim_robots);

		char spider[10];
		sprintf(spider,"spider%d",i+1);

		double px,py,pa;	
		playerc_simulation_get_pose2d(*simulation, spider, &px,&py,&pa);
		//printf("Robot  %d empieza en: %f %f %f\n", i,px,py,pa);

		if(i%10==0) playerc_client_read(*sim_robots);

		d_data->p_data->positions[i].px=px;
		d_data->p_data->positions[i].py=py;
		d_data->p_data->positions[i].pa=pa;
	}

	for(i=0;i<number_robots;i++)
	{
		playerc_position2d_set_cmd_car(sim_position2d[i],0,0);
		if(i%10==0)
			playerc_client_read(*sim_robots);
	}


//Added by XXXX. Escogemos un nodo como AP....
	if (d_data->nueva_simulacion)
	{
		d_data->FinalAP=chooseAP(sim_position2d, d_data->robot_data,number_robots,0,-1);
	}
	d_data->robot_data[d_data->FinalAP].stopped=1;
	d_data->robot_data[d_data->FinalAP].level=1; //No hay nivel 2...

	printf("AP escogido %i",d_data->FinalAP);


// --------------------------------------------		DEPLOYMENT		----------------------------------------------------

	time_t comienzo=time(NULL);
	time_t LastTime=time(NULL);
	time_t final,comienzo_pausa,final_pausa;
	double pause_time=0;

	//sleep(2); //XXXX. Pausa Para poder ver los mensajes

	//system("reset");
	printf("Para pausar la simulación y mostrar el menu de simulación pulse enter\n");

	//Start Algorithm
	while(stop!=1)
	{

		//Wait for new data from server
		playerc_client_read(*sim_robots);
		
		for(i=0;i<number_robots;i++)
		{
			//0)Si esta roto no hace nada. (Lo mismo me lo llevo hacia abajo...)
			if(d_data->robot_data[i].broken==1)
			{
				//printf("Robot %d está roto.\n",i);
				moverse(sim_position2d,d_data->robot_data,i,number_robots,d_data->num_rotos,map_size,obstaculos);
			}
			//XXXX ¿Hace lo mismo en ambos casos?
			// Si no esta roto se mueve
			else 
			{
				//printf("Robot %d no está roto.\n",i);
				moverse(sim_position2d,d_data->robot_data,i,number_robots,d_data->num_rotos,map_size,obstaculos);
			}
	
			//Update positions and calculate distance walked
			player_pose2d_t new_pose;
			new_pose.px= sim_position2d[i]->px;
			new_pose.py= sim_position2d[i]->py;
			new_pose.pa= sim_position2d[i]->pa;

			d_data->p_data->distance_walked[i]+=sqrt(pow(d_data->p_data->positions[i].px-new_pose.px,2)+pow(d_data->p_data->positions[i].py-new_pose.py,2));
			d_data->p_data->positions[i]=new_pose;

			//Send broadcast with position (receiver=-1, estrategia=0)
			clearArrays(msj_tx,number_robots);
			sendAdhoc(d_data, i, i, -1, msj_tx, 0);
			//sendAdhoc(d_data, i, i, -1, msj_tx, 0); //XXX

			//Print graphics over robots			
			dibuja(d_data->robot_data, sim_position2d, sim_graphics2d, i, number_robots, d_data->tipo_red,d_data->algoritmo);
			
			//sleep(0.4);
		}

		//Calculo potencia media:
		if(!contador)
		{
			if(n<1000)
			{

				system("clear");
				d_data->p_data->net_mean_power[n]=netMeanPower(d_data,0);
				printf("Potencia media en la red = %lf\n",d_data->p_data->net_mean_power[n]);
			}
			n++;
		}
		contador=(contador+1)%NET_MEAN_POWER_ITERATIONS;

		//Miro si ha terminado el algoritmo
		stop=1;
		stopped=number_robots;
		for(i=0;i<number_robots;i++)
		{
			//If there is a robot moving and it's not broken, go on
			if(d_data->robot_data[i].stopped==0&&d_data->robot_data[i].broken!=1)
			{
				stop=0;
				stopped--;
			}
		}

		//Parada forzada cuando la red se estanca pero los nodos se siguen moviendo un poco....
		if (stopped>=0.92*number_robots)
		{

//			if (LastStopped!=stopped)
//			{	//Si ha cambiado el numero de robots parados, retomamos el tiempo
//				LastTime=time(NULL);
//				LastStopped=stopped;
//			}
			if (difftime(time(NULL),LastTime)>(120.0*(number_robots-stopped)))
			{	//Esperamos 2 minutos por robot no parado.
				stop=1;
			}
		}
		else
		{
			LastTime=time(NULL);
			//LastStopped=stopped;
		}

	
		if(kbhit())
		{
			comienzo_pausa=time(NULL);

			reader=-1;
			salir=0;

			for(i=0;i<number_robots;i++)
			{
				playerc_position2d_set_cmd_vel(sim_position2d[i],0,0,0,0);
			}

			while(salir==0)
			{
				system("clear");
				printf ("------------------  MENU DESPLIEGUE  ------------------\n");
				printf("Acciones posibles durante el despliegue:\n");
				printf("0) Reanudar simulación.\n");
				printf("1) Romper/Arreglar un spider.\n");
				printf("2) Terminar despliegue.\n");

				fflush(stdin);
				scanf("%d",&reader);
			
				while(reader<0||reader>3)
				{
					printf("Introduzca una de esas opciones, por favor...\n");
					fflush(stdin);
					scanf("%d",&reader);
				}				
				switch(reader)
				{
					case 0:
						salir=1;
						break;
					
					case 1:
						printf("Introduce el número del spider a modificar\n");
						fflush(stdin); 
						scanf("%d",&reader);
						changeBroken(sim_position2d,d_data->robot_data,reader,number_robots);
						salir=1;
						break;
					
					case 2:
						final_pausa=time(NULL);
						pause_time+=difftime(final_pausa,comienzo_pausa);	
						final=time(NULL);
						d_data->p_data->depl_time=difftime(final,comienzo)-pause_time;	
						free(msj_tx);
						d_data->p_data->num_iterations=n*NET_MEAN_POWER_ITERATIONS;
						return d_data;
				}
			}

			printf("Para pausar la simulación y mostrar el menu de simulación pulse enter\n");			
			
			final_pausa=time(NULL);
			pause_time+=difftime(final_pausa,comienzo_pausa);
		}
	}

	final=time(NULL);
	d_data->p_data->depl_time=difftime(final,comienzo)-pause_time;


	//Aqui acaba el despliegue por lo que parece ---> determinar que nodos mueren y lanzar otro...


	printf("El algoritmo de despliegue ha terminado.\n",i);

	sleep(2);
	
	d_data->p_data->num_iterations=n*NET_MEAN_POWER_ITERATIONS;
	free(msj_tx);
	
	return d_data;
}

