/* 
/*---------------------------------------------------------------------------
 *  Proyecto Fin de Carrera: Enjambre de microbots para despliegue autónomo de 
 *                           una red inalámbrica mallada.
 * 
 *  Francisco José Aguilera Leal
 *  Universidad de Málaga
 * --------------------------------------------------------------------------- 
 * Archivo de cabecera de adhocSPF.c
 * ---------------------------------------------------------------------------
*/

#ifndef ADHOCSPF_H
#define ADHOCSPF_H

#include <stdio.h>
#include <unistd.h> //For the sleep command
#include <time.h> //For the seed to srand for time
#include <libplayerc/playerc.h>
#include <math.h>

#include "config.h"
#include "auxFunctions.h"
#include "proxys.h"
#include "rendimiento.h"
#include "datatypes.h"
#include "coverage.h"
#include "routing.h"
#include "dibuja.h"

deployment_data_t* simAdhocSPF(deployment_data_t* d_data,int* rotos,int obstaculos,playerc_client_t **sim_robots, playerc_position2d_t** sim_position2d, playerc_simulation_t** simulation, playerc_graphics2d_t** sim_graphics2d,playerc_sonar_t** sim_sonar); //JMCG. added sonar

#endif /*ADHOCSPF_H*/
