/*---------------------------------------------------------------------------
 *  Proyecto Fin de Carrera: Enjambre de microbots para despliegue autónomo de 
 *                           una red inalámbrica mallada.
 * 
 *  Francisco José Aguilera Leal
 *  Universidad de Málaga
 * --------------------------------------------------------------------------- 
 * Archivo de configuración
 * ---------------------------------------------------------------------------
*/


#ifndef CONFIG_H
#define CONFIG_H


/* ------------------------------- COMUNES -------------------------------- */

/* Cada cuantas iteraciones calculo la potencia media en la red */
#define NET_MEAN_POWER_ITERATIONS	10	

/* Potencia niveles 0 y nodos adhoc */
#define P_TX_ADHOC		0.1 //mW

/* Potencia niveles 2 y quizas 1 */
#define P_TX_LEVEL_2		1 //mW	

/* Umbral de sensibilidad (minima Pr). Ajustado a -93 dBm*/
//#define P_RX_MIN		5*pow((double)10.0, -10) //mW

/* Constante para el cálculo de la potencia adaptado a valores del robot real */
//#define K_ROBOT_REAL		5*pow((double)10.0, -8)	//mW

/* Calculo directamente el cociente para no manejar tantos decimales */

#define COCIENTE_PRXMIN_K	0.01 // Si pongo k=10^-8 seria 0.05

/* ------------------------- ALGORITMO DE BACKBONE -------------------------*/

/* Distancia medida por sensor para esquivar obstaculo */
#define AVOID_DISTANCE 		0.15			

/* Distancia hasta la cual puedo recibir de otro robot */
#define RADIO_COVERAGE_BA	2 			

/* Distancia para separarme de los otros robots	*/
#define MIN_WIFI_DISTANCE	1.5	

/* Intensidad maxima para separarme de los otros robots	*/
#define MAX_WIFI_INTENSITY	(1.0/MIN_WIFI_DISTANCE*MIN_WIFI_DISTANCE)				

/* --------------------------- POTENTIAL FIELDS ----------------------------*/


/* Umbral de fuerza a partir de la cual muevo los robots */
#define UMBRAL_MOVIMIENTO	1					



			/* Red adhoc */

/* Distancia hasta la cual puedo recibir de otro robot */				
#define RADIO_COVERAGE_SPF	2.5 				


			/* Red jerarquizada */

/* Distancia para elegir Access Point. Robots a mayor distancia de la estacion
 base en (0,0) no pueden ser AP */
#define CHOOSE_AP_DIST 		3.16			

/* Distancia limite hasta donde llega el AP */
#define MAX_AP_DIST 		10	

/* Maxima distancia para recibir comunicacion de nodos level 1 por parte de
 los level 0	*/	
#define MAX_L1L0_DIST		2	

/* Distancia limite para que los nodos nivel 1 se alejen de los otros de
 nivel 1 */
#define MIN_L1L1_DIST 		1.6					

/* Diferencia de distancia entre un l0 y dos l1 para que se cambie de l1 */
#define MIN_DIFF_L0L1_DIST	0.5 

/* Maximo numero de nodos que un nodo de nivel 1 puede tener registrados */
#define MAX_REGISTERED 		4


//XXX JOSE --> cutrisimulacion de perdidas y  retransmisiones
#define NUM_MAX_RETX 1
#define PROB_LOSS 0.0

#define ROBOT_INITIAL_BATTERY 3000
//JMCG: Ajustar!!!!
#define ROBOT_WASTE_PER_PACKET 0.000138

#define ROBOT_DUTYCYCLE_WASTE 0.15
#define ROBOT_PACKET_RATE_HOUR 360.0

//This is for Tx power
#define ROBOT_WASTE_INCREASE_DISTANCE 0.1

//This one is for walked distance (mA*escala/velocidad (m/s)/3600.0)
#define BATTERY_WASTE_PER_DISTANCE ((200.0*10.0/(1.0/20.0))/3600.0)

#endif /*CONFIG_H*/
