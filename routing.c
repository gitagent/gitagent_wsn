/*---------------------------------------------------------------------------
 *  Proyecto Fin de Carrera: Enjambre de microbots para despliegue autónomo de 
 *                           una red inalámbrica mallada.
 * 
 *  Francisco José Aguilera Leal
 *  Universidad de Málaga
 * --------------------------------------------------------------------------- 
 * Funciones de enrutamiento de mensajes para los distintos tipos de redes.
 * ---------------------------------------------------------------------------
*/

#include "routing.h"
#include <stdlib.h>
/* ---------------------------------------------------------------------------
 * sendAdhoc - Envia mensaje entre dos robots en una red ad hoc
 * --------------------------------------------------------------------------- */

//XXX MSG_TX ¿DOUBLE????


/* ---------------------------------------------------------------------------
 * sendAdhoc - Envia mensaje entre dos robots en una red ad hoc
 * --------------------------------------------------------------------------- */
//FUNCION ORIGINAL!!!!

void sendAdhoc(deployment_data_t* d_data,int my_robot, int sender, int receiver, double* msj_tx, int estrategia)
{
	int i;
	int number_robots=d_data->number_robots;
	float radio_coverage;

	if(d_data->algoritmo==BA)
	{
		radio_coverage=RADIO_COVERAGE_BA;
	}
	else
	{
		radio_coverage=RADIO_COVERAGE_SPF;
	}


	d_data->p_data->packets_sent[my_robot]++;
	//XXXX En modo ADHOC, la potencia con la que se transmite sera la correspondiente a "radio coverage". Le aplico el cargo por distancia
	d_data->p_data->wasted_energy[my_robot]+=ROBOT_WASTE_PER_PACKET*(1.0+ROBOT_WASTE_INCREASE_DISTANCE*radio_coverage);
	if(sender==my_robot)
	{
		//printf("Robot %d envía a %d:\n",my_robot+1,receiver+1);
		d_data->p_data->own_packets_sent[my_robot]++;
	}
	//printf("Robot %d en sendAdhoc: posx %f posy %f \n",my_robot+1,d_data->p_data->positions[my_robot].px,d_data->p_data->positions[my_robot].py);
	msj_tx[my_robot]=1;

	for(i=0;i<number_robots;i++)
	{
		if((i!=my_robot)&&(!d_data->robot_data[i].broken))
		{
			if(calculateDistance2(d_data->p_data,my_robot,i) <= radio_coverage)
			{
				//printf("Robot %d reenvía a %d el mensaje de %d para %d\n",my_robot+1,i+1,sender+1,receiver+1);
				d_data->p_data->packets_rx[i]++;
				if(receiver==i)
				{
					d_data->p_data->own_packets_rx[i]++;
				}
				else if(estrategia==0 && msj_tx[i]==0) //Estrategia rtx una vez todos.
				{
					sendAdhoc(d_data, i, sender, receiver, msj_tx, estrategia);
				}
				else if((calculateDistance2(d_data->p_data,i,receiver) < calculateDistance2(d_data->p_data,my_robot,receiver)) && msj_tx[i]==0) //Estrategia rtx si mas cerca
				{
					sendAdhoc(d_data, i, sender, receiver, msj_tx, estrategia);
				}
			}
		}
	}
}


// RETOCADA  REPASAR....
//void sendAdhoc(deployment_data_t* d_data,int my_robot, int sender, int receiver, double* msj_tx, int estrategia)
//{
//	int i,ret;
//	int number_robots=d_data->number_robots;
//	float radio_coverage;
//
//	if(d_data->algoritmo==BA)
//	{
//		radio_coverage=RADIO_COVERAGE_BA;
//	}
//	else
//	{
//		radio_coverage=RADIO_COVERAGE_SPF;
//	}
//
//
//	d_data->p_data->packets_sent[my_robot]++; //TODO XXXX Aqui????
//	if(sender==my_robot)
//	{
//		//printf("Robot %d envía a %d:\n",my_robot+1,receiver+1);
//		d_data->p_data->own_packets_sent[my_robot]++;
//	}
//	//printf("Robot %d en sendAdhoc: posx %f posy %f \n",my_robot+1,d_data->p_data->positions[my_robot].px,d_data->p_data->positions[my_robot].py);
//	//XXX msj_tx[my_robot]+=1;
//
//	for(i=0;i<number_robots;i++)
//	{
//		if(i!=my_robot)
//		{
//			if(calculateDistance2(d_data->p_data,my_robot,i) <= radio_coverage)
//			{
//				//printf("Robot %d reenvía a %d el mensaje de %d para %d\n",my_robot+1,i+1,sender+1,receiver+1);
//				d_data->p_data->packets_rx[i]++;
//				if(receiver==i)
//				{
//					d_data->p_data->own_packets_rx[i]++;
//				}
//				else if(estrategia==0 ) //Estrategia rtx una vez todos.
//				{
//					while (msj_tx[i]<(NUM_MAX_RETX))
//					{
//						msj_tx[my_robot]+=1;
//						sendAdhoc(d_data, i, sender, receiver, msj_tx, estrategia);
//						if ((((double)rand()/(double)RAND_MAX))>PROB_LOSS) break;
//					}
//					//sendAdhoc(d_data, i, sender, receiver, msj_tx, estrategia); //XXX
//				}
//				else if((calculateDistance2(d_data->p_data,i,receiver) < calculateDistance2(d_data->p_data,my_robot,receiver))) //Estrategia rtx si mas cerca
//				{
//			        while (msj_tx[i]<(NUM_MAX_RETX))
//					{
//						msj_tx[my_robot]+=1;
//						sendAdhoc(d_data, i, sender, receiver, msj_tx, estrategia);
//						if ((((double)rand()/(double)RAND_MAX))>PROB_LOSS) break;
//					}
//					//XXXsendAdhoc(d_data, i, sender, receiver, msj_tx, estrategia);
//					//sendAdhoc(d_data, i, sender, receiver, msj_tx, estrategia); //XXX
//
//				}
//			}
//		}
//	}
//}

/* ---------------------------------------------------------------------------
 * sendAPJerq - Envia mensaje entre dos robots en una red jerarquizada con AP
 * --------------------------------------------------------------------------- */
void sendAPJerq(deployment_data_t* d_data, int my_robot, int sender, int receiver, double* msj_tx,int estrategia)
{
	int i,j,to_robot=-1;
	int number_robots=d_data->number_robots;	

	msj_tx[my_robot]=1;


	//to_robot=d_data->robot_data[my_robot].registered_to;

	//XXXX. Cambios a hacer.... El coste es el de mandar al mio padre
	//Tengo que obtener la distancia enre mi robot y su padre.
	//Y aplicar:::
	//d_data->p_data->wasted_energy[my_robot]+=ROBOT_WASTE_PER_PACKET*(1.0+ROBOT_WASTE_INCREASE_DISTANCE*radio_coverage);
	//CUIDADO QUE ESTA FUNCION VALE TANTO EN SENTIDO ASCENDENTE COMO DESCENTENTE!!!!!
	//No te la cargues por si en un futuro se sigue usando...

	if(receiver!=my_robot)
	{
		d_data->p_data->packets_sent[my_robot]++;
		if(sender==my_robot)
		{	
			//printf("Robot %d envía a %d \n",my_robot+1,receiver+1);
			d_data->p_data->own_packets_sent[my_robot]++;
		}

		if(d_data->robot_data[my_robot].level==0 && d_data->robot_data[my_robot].registered_to!=-1)
		{
			// Si soy nivel 0 registrado solo puedo mandarlo a mi nivel 1
			to_robot=d_data->robot_data[my_robot].registered_to;
			d_data->p_data->packets_rx[to_robot]++;

			double distancia=calculateDistance2(d_data->p_data,my_robot,d_data->robot_data[my_robot].registered_to); //Calcula distancia
			d_data->p_data->wasted_energy[my_robot]+=ROBOT_WASTE_PER_PACKET*(1.0+ROBOT_WASTE_INCREASE_DISTANCE*distancia);

			if(receiver==to_robot)
			{
				//printf("Robot %d (n0) envía a %d (n1) porque es el receptor\n",my_robot+1,to_robot+1);
				d_data->p_data->own_packets_rx[to_robot]++;
			}
			else
			{
				//printf("Robot %d (n0) envía a %d (n1) porque es su nivel 1\n",my_robot+1,to_robot+1);
				sendAPJerq(d_data,to_robot,sender,receiver,msj_tx,estrategia);
			}
		}
		
		else if(d_data->robot_data[my_robot].level==1)
		{
			for(i=0;i<number_robots;i++)
			{
				// Miro si esta entre mis registrados
				if(d_data->robot_data[i].registered_to == my_robot && i == receiver)
				{
					double distancia=calculateDistance2(d_data->p_data,my_robot,receiver); //Calcula distancia
					d_data->p_data->wasted_energy[my_robot]+=ROBOT_WASTE_PER_PACKET*(1.0+ROBOT_WASTE_INCREASE_DISTANCE*distancia);

					d_data->p_data->packets_rx[i]++;
					if(receiver==i)
					{
						to_robot=i;
						//printf("Robot %d (n1) envía a %d (n0) porque es el receptor\n",my_robot+1,receiver+1);
						d_data->p_data->own_packets_rx[i]++;
					}
				}
			}
			// Si no esta entre mis registrados y el AP no me ha mandado el msj, lo mando al AP
			if(to_robot==-1 && !msj_tx[d_data->robot_data[my_robot].registered_to])
			{
				to_robot=d_data->robot_data[my_robot].registered_to;
				d_data->p_data->packets_rx[to_robot]++;

				double distancia=calculateDistance2(d_data->p_data,my_robot,d_data->robot_data[my_robot].registered_to); //Calcula distancia
				d_data->p_data->wasted_energy[my_robot]+=ROBOT_WASTE_PER_PACKET*(1.0+ROBOT_WASTE_INCREASE_DISTANCE*distancia);

				if(receiver==to_robot)
				{
					//printf("Robot %d (n1) envía a %d (n2) porque es el receptor\n",my_robot+1,to_robot+1);
					d_data->p_data->own_packets_rx[to_robot]++;
				}
				else
				{
					//printf("Robot %d (n1) envía a %d (n2) porque no esta entre sus registrados\n",my_robot+1,to_robot+1);
					sendAPJerq(d_data,to_robot,sender,receiver,msj_tx,estrategia);
				}
			}
		}
		else if(d_data->robot_data[my_robot].level==2)
		{
			// Si ese nivel 1 es el receptor se lo envio
			if(d_data->robot_data[receiver].registered_to==my_robot)
			{
				to_robot=receiver;
				double distancia=calculateDistance2(d_data->p_data,my_robot,to_robot); //Calcula distancia
				d_data->p_data->wasted_energy[my_robot]+=ROBOT_WASTE_PER_PACKET*(1.0+ROBOT_WASTE_INCREASE_DISTANCE*distancia);
				//printf("Robot %d (n2) envía a %d (n1) porque es el receptor\n",my_robot+1,receiver+1);
				d_data->p_data->own_packets_rx[receiver]++;
				d_data->p_data->packets_rx[receiver]++;
			}

			// En caso contrario, compruebo en que nivel 1 esta el robot y envio a ese level 1.
			else if(d_data->robot_data[receiver].registered_to!=-1)
			{
				to_robot=d_data->robot_data[receiver].registered_to;
				double distancia=calculateDistance2(d_data->p_data,my_robot,to_robot); //Calcula distancia
				d_data->p_data->wasted_energy[my_robot]+=ROBOT_WASTE_PER_PACKET*(1.0+ROBOT_WASTE_INCREASE_DISTANCE*distancia);
				d_data->p_data->packets_rx[to_robot]++;
				//printf("Robot %d envía a %d porque %d esta entre sus registrados\n",my_robot+1,to_robot+1,receiver+1);
				sendAPJerq(d_data,to_robot,sender,receiver,msj_tx,estrategia);
			}
				
			// Si no está en ninguno es que aun es un robot libre --> Lo mando como adhoc
			else
			{
				//printf("ES LIBRE!!!\n");
				double distancia=RADIO_COVERAGE_SPF; //Calcula distancia
				d_data->p_data->wasted_energy[my_robot]+=ROBOT_WASTE_PER_PACKET*(1.0+ROBOT_WASTE_INCREASE_DISTANCE*distancia);

				clearArrays(msj_tx,d_data->number_robots);
				sendAdhoc(d_data,my_robot,sender,receiver,msj_tx,estrategia);
			}
		}
	}
}

/* ---------------------------------------------------------------------------
 * potAdhoc - Potencia necesaria (mW) para transmitir de un robot a otro 
 * 	      en red adhoc.
 * --------------------------------------------------------------------------- */
double potAdhoc(deployment_data_t* d_data,int my_robot, int sender, int receiver, double* msj_tx, int* fin, double* tx_power) 
{
	int i,ret;
	double k=5*pow((double)10.0,-8);
	int number_robots=d_data->number_robots;	
	float radio_coverage;

	if(d_data->algoritmo==BA)
	{
		radio_coverage=RADIO_COVERAGE_BA;
	}
	else
	{
		radio_coverage=RADIO_COVERAGE_SPF;
	}

	if(receiver!=my_robot)
	{
		//XXX msj_tx[my_robot]+=1;
	
		for(i=0;i<number_robots && !(*fin);i++)
		{
			if((i!=my_robot)&&(!d_data->robot_data[i].broken))
			{
				if(calculateDistance2(d_data->p_data,my_robot,i) <= radio_coverage)
				{
					//printf("Robot %d reenvía a %d el mensaje de %d para %d\n",my_robot+1,i+1,sender+1,receiver+1);

					if((calculateDistance2(d_data->p_data,i,receiver) < calculateDistance2(d_data->p_data,my_robot,receiver)))
					{
						//(*tx_power)+=COCIENTE_PRXMIN_K*pow(calculateDistance2(d_data->p_data,my_robot,i),2);
						//potAdhoc(d_data, i, sender, receiver, msj_tx, fin,tx_power); //XXX tx_power acumula...
						//for (ret=0;ret<NUM_MAX_RETX;ret++)
						while (msj_tx[i]<(NUM_MAX_RETX))
						{
							(*tx_power)+=COCIENTE_PRXMIN_K*pow(calculateDistance2(d_data->p_data,my_robot,i),2);
							msj_tx[my_robot]+=1; //XXX
							potAdhoc(d_data, i, sender, receiver, msj_tx, fin,tx_power);
							if ((((double)rand()/(double)RAND_MAX))>PROB_LOSS) break;
						}

					}		
				}
			}
		}
	}
	else
	{
		*fin=1;
		//printf("El mensaje ha llegado a receiver = %d. Potencia tx necesaria: %lf\n",receiver+1,*tx_power);
		//printf("Constante K= %lf, Prmin= %lf\n", k, (double)P_RX_MIN);
	}
}

/* ---------------------------------------------------------------------------
 * potAPJerq - Potencia necesaria (mW) para transmitir de un robot a otro 
 * 	      en red jerarquizada con AP.
 * --------------------------------------------------------------------------- */
double potAPJerq(deployment_data_t* d_data,int my_robot, int sender, int receiver, double* msj_tx, int* fin, double* tx_power) 
{
	int i,j;
	int number_robots=d_data->number_robots;
	double distancia;	
	
	if(receiver!=my_robot)
	{
		msj_tx[my_robot]=1;
	
		// Los niveles 0 registrados mandan directamente a su nivel 1
		if(d_data->robot_data[my_robot].level==0 && d_data->robot_data[my_robot].registered_to!=-1)
		{
			distancia=calculateDistance2(d_data->p_data,my_robot,d_data->robot_data[my_robot].registered_to);
			(*tx_power)+=COCIENTE_PRXMIN_K*pow(distancia,2);
			potAPJerq(d_data, d_data->robot_data[my_robot].registered_to, sender, receiver, msj_tx, fin,tx_power);
		}
		// Los niveles 1
		else if(d_data->robot_data[my_robot].level==1)
		{
			for(i=0;i<number_robots;i++)
			{
				// Miro si esta entre mis registrados
				if(d_data->robot_data[i].registered_to == my_robot && i == receiver)
				{
					distancia=calculateDistance2(d_data->p_data,my_robot,i);
					(*tx_power)+=COCIENTE_PRXMIN_K*pow(distancia,2);
					potAPJerq(d_data, i, sender, receiver, msj_tx, fin,tx_power);
				}
			}
			// Si no esta entre mis registrados y el AP no me ha mandado el msj, lo mando al AP
			if(!(*fin) && !msj_tx[d_data->robot_data[my_robot].registered_to])
			{
				distancia=calculateDistance2(d_data->p_data,my_robot,d_data->robot_data[my_robot].registered_to);
				(*tx_power)+=COCIENTE_PRXMIN_K*pow(distancia,2);
				potAPJerq(d_data, d_data->robot_data[my_robot].registered_to, sender, receiver, msj_tx, fin,tx_power);
			}
		}
		// El nivel 2
		else if(d_data->robot_data[my_robot].level==2)
		{
			// Compruebo en que nivel 1 esta el robot y envio a ese level 1.
			for(i=0;i<number_robots;i++)
			{
				if(i!=my_robot && d_data->robot_data[i].registered_to == my_robot && !msj_tx[i])
				{
					// Si el nivel 1 es el receptor
					if(receiver==i)
					{
						(*tx_power)+=COCIENTE_PRXMIN_K*pow(calculateDistance2(d_data->p_data,my_robot,i),2);
						potAPJerq(d_data, i, sender, receiver, msj_tx, fin,tx_power);
					}
					// Si ese nivel 1 tiene registrado al receptor
					else
					{
						for(j=0;j<number_robots;j++)
						{
							if(j==receiver && d_data->robot_data[j].registered_to==i)
							{
								(*tx_power)+=COCIENTE_PRXMIN_K*pow(calculateDistance2(d_data->p_data,my_robot,i),2);
								potAPJerq(d_data, i, sender, receiver, msj_tx, fin,tx_power);
							}
						}
					}
				}
			}
				
			// Si no está en ninguno es que aun es un robot libre --> Lo mando como adhoc
			if(!(*fin))
			{
				clearArrays(msj_tx,d_data->number_robots);
				potAdhoc(d_data, my_robot, sender, receiver, msj_tx, fin,tx_power);
			}
		}
		// Si es nivel 0 sin registrar lo mando como adhoc
		else if(!(*fin))
		{
			clearArrays(msj_tx,d_data->number_robots);
			potAdhoc(d_data, my_robot, sender, receiver, msj_tx, fin,tx_power);
		}
	}
	else
	{
		*fin=1;
	}
}

