/*---------------------------------------------------------------------------
 *  Proyecto Fin de Carrera: Enjambre de microbots para despliegue autónomo de 
 *                           una red inalámbrica mallada.
 * 
 *  Francisco José Aguilera Leal
 *  Universidad de Málaga
 * --------------------------------------------------------------------------- 
 * Estructuras de datos que se utilizan.
 * ---------------------------------------------------------------------------
*/

#ifndef DATATYPES_H 
#define DATATYPES_H

#include <libplayerc/playerc.h>

typedef enum algorithm_t { BA,SPF,AAA } algorithm_t;

typedef enum network_type_t { JERARQUIZADA,ADHOC } network_type_t;

typedef struct performance_data_t
{
	float depl_time;
	player_pose2d_t* positions;
   	double* packets_sent;
	double* own_packets_sent;
   	double* packets_rx;
	double* own_packets_rx;
	double* wasted_energy; //ADDED to the energy model.
	double* distance_walked;
	double* net_mean_power;
	int num_iterations;
} performance_data_t;

typedef struct robot_data_t
{
	double remaining_battery; //JMCG
	int turn;
   	int turning;
   	int turning_count;
	int walking_count;
	int backbone;
	int stopped;
	int broken;
	int level; // Nivel 2: AP. Nivel 1: Se comunican con el AP. Nivel 0: Se registran en los de nivel 1.
	int low_battery;
	int registered_to; // A que nodo de nivel 1 esta registrado
	int num_registered; // Numero de nodos de nivel 0 registrados a el
} robot_data_t;

typedef struct deployment_data_t
{
	robot_data_t* robot_data;
	performance_data_t* p_data;
	int number_robots;
	int num_rotos; //JMCG
	int map_size;
	int nueva_simulacion;	
	network_type_t tipo_red;
	algorithm_t algoritmo;
	int FinalAP;
} deployment_data_t;

/*************************************** ADDED by Gita ************************************************/
typedef struct tasks_data
{
	int num_tasks; //used to determine task ids
	int* task_ids; //contains task ids
	float* xpos; // array of the x positions of the tasks on the grid - indexing based on task id
	float* ypos; // array of the y positions of the tasks on the grid - indexing based on task id

} tasks_data;

typedef struct assigned_robots
{
	// the length of these arrays is determined by the nr of tasks that are assigned
	int* robots; //contains robot ids
	int* tasks_assigned;
	float* xpos; // array of the x positions of the tasks on the grid - indexing based on tasks_assigned
	float* ypos; // array of the y positions of the tasks on the grid - indexing based on tasks_assigned

} assigned_robots;

typedef struct help_protocol_data
{
	// the length of these arrays is determined by the nr of tasks that are assigned
	double* previous_step_battery;
	int* requested_help; //0s for no, 1s for yes. The array has the same length as the number of robots
	int* started_negotiation;
	int robot2help; // used by the AP
	float* giving_help; //this is a matrix with size n_robots*nr_robots.
	int* msgs_exchanged;//The array has the same length as the number of robots
	int round_of_SPF; // 1 if we want to run spf on the current iteration, otherwise 0.
	int total_negotiations;
	int total_negotiations_AP;
	int unfinished_negotiations;
	int unfinished_negotiations_AP;


} help_protocol_data;
/******************************************************************************************************/

#endif /*DATATYPES_H*/
