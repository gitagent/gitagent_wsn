/*---------------------------------------------------------------------------
 *  Proyecto Fin de Carrera: Enjambre de microbots para despliegue autónomo de 
 *                           una red inalámbrica mallada.
 * 
 *  Francisco José Aguilera Leal
 *  Universidad de Málaga
 * --------------------------------------------------------------------------- 
 * Archivo de cabecera de routing.c
 * ---------------------------------------------------------------------------
*/

#ifndef ROUTING_H
#define ROUTING_H

#include <math.h>
#include "datatypes.h"
#include "auxFunctions.h"
#include "config.h"

void sendAdhoc(deployment_data_t* d_data, int my_robot, int sender, int receiver, double* msj_tx, int estrategia);
void sendAPJerq(deployment_data_t* d_data, int my_robot, int sender, int receiver, double* msj_tx,int estrategia);
double potAdhoc(deployment_data_t* d_data,int my_robot, int sender, int receiver, double* msj_tx, int* fin, double* tx_power);
double potAPJerq(deployment_data_t* d_data,int my_robot, int sender, int receiver, double* msj_tx, int* fin, double* tx_power);

#endif /*ROUTING_H*/
