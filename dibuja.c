/*---------------------------------------------------------------------------
 *  Proyecto Fin de Carrera: Enjambre de microbots para despliegue autónomo de 
 *                           una red inalámbrica mallada.
 * 
 *  Francisco José Aguilera Leal
 *  Universidad de Málaga
 * --------------------------------------------------------------------------- 
 * Representacion en Stage.
 * ---------------------------------------------------------------------------
*/  

#include "dibuja.h"

/* ---------------------------------------------------------------------------
 * dibuja - Representa en Stage los robots y su estado
 * --------------------------------------------------------------------------- */
void dibuja(robot_data_t* robot_data, playerc_position2d_t** sim_position2d, playerc_graphics2d_t** sim_graphics2d, int my_robot, int number_robots, network_type_t tipo_red, algorithm_t algoritmo)
{
	int i;

	player_color_t color;
	player_point_2d_t point[3];
	player_point_2d_t point2[3];
	player_point_2d_t point3[4];


	// Borro si es necesario	
	if(my_robot==0)
	{
		int i;
		for(i=0;i<number_robots;i++)
		{
			if(robot_data[i].level==2)
			{
				playerc_graphics2d_clear(sim_graphics2d[i]);
			}
		}
	}
	if(robot_data[my_robot].level!=2)
	{
		playerc_graphics2d_clear(sim_graphics2d[my_robot]);
	}
  
	// Primero dibujo los rotos
	if(robot_data[my_robot].broken==1)
	{
		//printf("dibuja roto de spider %d\n",my_robot+1);
		color.red=255;
		color.blue=20;
		color.green=0;
		color.alpha=0;
		playerc_graphics2d_setcolor(sim_graphics2d[my_robot], color);
	
		point[0].px=0.07;
		point[0].py=0.07;
		point[1].px=0;
		point[1].py=0;
		point[2].px=-0.07;
		point[2].py=-0.07;
		point2[0].px=0.07;
		point2[0].py=-0.07;
		point2[1].px=0;
		point2[1].py=0;
		point2[2].px=-0.07;
		point2[2].py=0.07;
		playerc_graphics2d_draw_polyline (sim_graphics2d[my_robot], point, 3);
		playerc_graphics2d_draw_polyline (sim_graphics2d[my_robot], point2, 3);		
	}
	
	else if(tipo_red==ADHOC)
	{
		float radio_coverage;
		if(algoritmo==BA)
		{
			radio_coverage=RADIO_COVERAGE_BA;
		}
		else
		{
			radio_coverage=RADIO_COVERAGE_SPF;
		}

		// Ahora los que pertenecen a la backbone
		if(robot_data[my_robot].backbone)
		{
			//printf("dibuja bk de spider %d\n",my_robot+1);
			color.red=0;
			color.blue=255;
			color.green=0;
			color.alpha=0;
			playerc_graphics2d_setcolor(sim_graphics2d[my_robot], color);

			point[0].px=-0.06;
			point[0].py=-0.03;
			point[1].px=-0.06;
			point[1].py=0;
			point[2].px=-0.06;
			point[2].py=0.03;		
			playerc_graphics2d_draw_polyline (sim_graphics2d[my_robot], point, 3);
		}	

		// Dibuja lineas hasta los robots en rango		
		for(i=my_robot;i<number_robots;i++)
		{
			double distance=sqrt(pow(sim_position2d[my_robot]->px-sim_position2d[i]->px,2)+pow(sim_position2d[my_robot]->py-sim_position2d[i]->py,2));
			if(i!=my_robot && distance<=radio_coverage && !robot_data[i].broken)
			{
		
				color.red=0;
				color.blue=0;
				color.green=255;
				color.alpha=0;
				playerc_graphics2d_setcolor(sim_graphics2d[my_robot], color);

				double angle=atan2(sim_position2d[i]->py-sim_position2d[my_robot]->py,sim_position2d[i]->px-sim_position2d[my_robot]->px);
				player_point_2d_t point[2];
				point[0].px=0;
				point[0].py=0;
				point[1].px=distance*cos(angle-sim_position2d[my_robot]->pa);
				point[1].py=distance*sin(angle-sim_position2d[my_robot]->pa);
				playerc_graphics2d_draw_polyline(sim_graphics2d[my_robot], point, 2);
			}
		}	
	}
	else if(tipo_red==JERARQUIZADA)
	{	
		// Dibuja level 2 y level 1
		if(robot_data[my_robot].level>0)
		{
			color.red=0;
			color.blue=255;
			color.green=0;
			color.alpha=0;
			playerc_graphics2d_setcolor(sim_graphics2d[my_robot], color);

			player_point_2d_t point[3];
			player_point_2d_t point2[3];

			if(robot_data[my_robot].level==1)
			{
				point[0].px=0.07;
				point[0].py=0;
				point[1].px=0.06;
				point[1].py=0;
				point[2].px=0.05;
				point[2].py=0;		
				playerc_graphics2d_draw_polyline(sim_graphics2d[my_robot], point, 3);

				int AP_robot=robot_data[my_robot].registered_to;
				double distance = calculateDistance(sim_position2d, my_robot, AP_robot);
				double angle=atan2(-sim_position2d[AP_robot]->py+sim_position2d[my_robot]->py,-sim_position2d[AP_robot]->px+sim_position2d[my_robot]->px);
				point2[0].px=0;
				point2[0].py=0;
				point2[1].px=distance*cos(angle-sim_position2d[AP_robot]->pa);
				point2[1].py=distance*sin(angle-sim_position2d[AP_robot]->pa);
				playerc_graphics2d_draw_polyline(sim_graphics2d[AP_robot], point2, 2);
			}
			else if(robot_data[my_robot].level==2)
			{
				point[0].px=0.07;
				point[0].py=0.01;
				point[1].px=0.06;
				point[1].py=0.01;
				point[2].px=0.05;
				point[2].py=0.01;		
				playerc_graphics2d_draw_polyline (sim_graphics2d[my_robot], point, 3);
				point2[0].px=0.07;
				point2[0].py=-0.01;
				point2[1].px=0.06;
				point2[1].py=-0.01;
				point2[2].px=0.05;
				point2[2].py=-0.01;
				playerc_graphics2d_draw_polyline (sim_graphics2d[my_robot], point2, 3);	
			}
		}	

		// Dibuja registrados		
		else if(robot_data[my_robot].level==0 && robot_data[my_robot].registered_to!=-1)
		{
			color.red=0;
			color.blue=0;
			color.green=255;
			color.alpha=0;
			playerc_graphics2d_setcolor(sim_graphics2d[my_robot], color);

			int l1_robot=robot_data[my_robot].registered_to;
			double distance = calculateDistance(sim_position2d, my_robot, l1_robot);
			double angle=atan2(sim_position2d[l1_robot]->py-sim_position2d[my_robot]->py,sim_position2d[l1_robot]->px-sim_position2d[my_robot]->px);
			player_point_2d_t point[2];
			point[0].px=0;
			point[0].py=0;
			point[1].px=distance*cos(angle-sim_position2d[my_robot]->pa);
			point[1].py=distance*sin(angle-sim_position2d[my_robot]->pa);
			playerc_graphics2d_draw_polyline(sim_graphics2d[my_robot], point, 2);
		}


		//Luego los en bajo consumo
		//Deberia salir un triangulito azul...
		if(robot_data[my_robot].low_battery==1)
			{
				//printf("dibuja roto de spider %d\n",my_robot+1);
				color.red=0;
				color.blue=255;
				color.green=0;
				color.alpha=0;
				playerc_graphics2d_setcolor(sim_graphics2d[my_robot], color);

				point3[0].px=0.00;
				point3[0].py=0.12;
				point3[1].px=0.12;
				point3[1].py=-0.12;
				point3[2].px=-0.12;
				point3[2].py=-0.12;
				point3[3].px=0.00;
				point3[3].py=0.12;
//				point2[0].px=0.0;
//				point2[0].py=0.12;
//				point2[1].px=0;
//				point2[1].py=0;
//				point2[2].px=0.0;
//				point2[2].py=-0.12;
				playerc_graphics2d_draw_polyline (sim_graphics2d[my_robot], point3, 4);
				//playerc_graphics2d_draw_polyline (sim_graphics2d[my_robot], point2, 3);
			}

	}
}
