/*---------------------------------------------------------------------------
 *  Proyecto Fin de Carrera: Enjambre de microbots para despliegue autónomo de 
 *                           una red inalámbrica mallada.
 * 
 *  Francisco José Aguilera Leal
 *  Universidad de Málaga
 * --------------------------------------------------------------------------- 
 * Archivo con funciones para calcular y dibujar la zona de cobertura 
 * de cada robot.
 * ---------------------------------------------------------------------------
*/

#include "coverage.h"

/* ---------------------------------------------------------------------------
 * createGrid - Crea la rejilla de ocupacion
 * --------------------------------------------------------------------------- */
static int** createGrid(deployment_data_t* d_data)
{
	int i,gx,gy,x,y;

	int grid_size=round(d_data->map_size/GRID_CELD);

	// Creo rejilla e inicializo rejilla
	int **coverage_grid = (int**)malloc(sizeof(int*)*grid_size);
	for(x=0;x<grid_size;x++)
	{
		coverage_grid[x] = (int*)malloc(sizeof(int)*grid_size);
	}

	for(x=0;x<grid_size;x++)
	{
		for(y=0;y<grid_size;y++)
		{
			coverage_grid[x][y]=0;
		}
	}

	// Para cada robot:

	fesetround(3); // Para que redondee hacia -infinito
	for(i=0;i<d_data->number_robots;i++)
	{
		if (!d_data->robot_data[i].broken) /* XXXX */
		{
			// Extraigo la rejilla en que esta a partir de su posicion
			gx=(int)rint(fabs(-d_data->map_size/2-d_data->p_data->positions[i].px)/GRID_CELD);
			gy=(int)rint(fabs(-d_data->map_size/2-d_data->p_data->positions[i].py)/GRID_CELD);

			// Recorro el grid para ver en que cuadros influye
			for(y=0;y<grid_size;y++)
			{
				for(x=0;x<grid_size;x++)
				{
					// En los cuadros cercanos hasta 0.6 m (3 cuadros) la probabilidad es del 100%
					if(sqrt(pow(gx-x,2)+pow(gy-y,2))<=(int)rint(ROBOT_COVERAGE/2/GRID_CELD))
					{
						coverage_grid[x][y]+=100;
					}
					else
					{
						// En los cuadros de 0.6 a 1.2 la probabilidad es del 60%
						if(sqrt(pow(gx-x,2)+pow(gy-y,2))<=(int)rint(ROBOT_COVERAGE*3/4/GRID_CELD))
						{
							coverage_grid[x][y]+=60;
						}
						// En los cuadros de 1.2 a 1.6 m la probabilidad es del 20%
						else if(sqrt(pow(gx-x,2)+pow(gy-y,2))<=(int)rint(ROBOT_COVERAGE/GRID_CELD))
						{
							coverage_grid[x][y]+=20;
						}

					}
				}
			}
		}
	}
	return coverage_grid;
}

/* ---------------------------------------------------------------------------
 * calculateCoverage - Calcula la cobertura de la red
 * --------------------------------------------------------------------------- */
double calculateCoverage(deployment_data_t* d_data)
{
	srand(time(NULL));

	int grid_size=round(d_data->map_size/GRID_CELD);

	int** coverage_grid=createGrid(d_data);

	// Recorro la rejilla para calcular la cobertura
	int x,y,sum=0;
	for(y=0;y<grid_size;y++)
	{
		for(x=0;x<grid_size;x++)
		{
			if((rand() % 100) < coverage_grid[x][y])
			{

				sum++;
			}
		}
	}

	free(coverage_grid);

	return (sum/pow(grid_size,2));
}

/* ---------------------------------------------------------------------------
 * dibujaCoverage - Representa la cobertura en Stage
 * --------------------------------------------------------------------------- */
void dibujaCoverage(deployment_data_t* d_data,playerc_client_t** sim_robots, playerc_graphics2d_t** sim_graphics2d)
{
	int i,i2;

	player_color_t color;

	for (i=0;i<d_data->number_robots;i++)
	{
		if (!d_data->robot_data[i].broken) /* XXXX */
		{
			color.red=255;
			color.blue=0;
			color.green=0;
			color.alpha=0;
			playerc_graphics2d_setcolor(sim_graphics2d[i], color);

			player_point_2d_t point[9];
			for (i2=0;i2<9;i2++)
			{
				point[i2].px=ROBOT_COVERAGE*cos(i2*M_PI/4);
				point[i2].py=ROBOT_COVERAGE*sin(i2*M_PI/4);
			}
			playerc_graphics2d_draw_polyline (sim_graphics2d[i], point, 9);

			color.red=0;
			color.blue=255;
			color.green=0;
			playerc_graphics2d_setcolor(sim_graphics2d[i], color);

			player_point_2d_t point2[9];
			for (i2=0;i2<9;i2++)
			{
				point2[i2].px=ROBOT_COVERAGE/2*cos(i2*M_PI/4);
				point2[i2].py=ROBOT_COVERAGE/2*sin(i2*M_PI/4);
			}
			playerc_graphics2d_draw_polyline (sim_graphics2d[i], point2, 9);

			color.red=0;
			color.blue=0;
			color.green=255;
			playerc_graphics2d_setcolor(sim_graphics2d[i], color);

			player_point_2d_t point3[9];
			for (i2=0;i2<9;i2++)
			{
				point3[i2].px=ROBOT_COVERAGE/4*3*cos(i2*M_PI/4);
				point3[i2].py=ROBOT_COVERAGE/4*3*sin(i2*M_PI/4);
			}
			playerc_graphics2d_draw_polyline (sim_graphics2d[i], point3, 9);
		}
	}
}

