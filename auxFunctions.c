/*---------------------------------------------------------------------------
 *  Proyecto Fin de Carrera: Enjambre de microbots para despliegue autónomo de 
 *                           una red inalámbrica mallada.
 * 
 *  Francisco José Aguilera Leal
 *  Universidad de Málaga
 * --------------------------------------------------------------------------- 
 * Funciones auxiliares.
 * ---------------------------------------------------------------------------
*/

#include "auxFunctions.h"

/* ---------------------------------------------------------------------------
 * cmd_system - Manda un comando al sistema operativo
 * --------------------------------------------------------------------------- */
char* cmd_system(const char* command)
{
    char* result = "";
    FILE *fpRead;
    fpRead = popen(command, "r");
    char buf[1024];
    memset(buf,'\0',sizeof(buf));
    while(fgets(buf,1024-1,fpRead)!=NULL)
    {
        result = buf;
    }
    if(fpRead!=NULL)
       pclose(fpRead);
    return result;
}

/* ---------------------------------------------------------------------------
 * kbhit - Detecta si se ha pulsado una tecla
 * --------------------------------------------------------------------------- */
int kbhit(void)
{
	struct timeval tv;
	fd_set read_fd;
	tv.tv_sec=0;
	tv.tv_usec=0;
	FD_ZERO(&read_fd);
	FD_SET(0,&read_fd);

	if(select(1,&read_fd,NULL,NULL,&tv)==-1)
		return 0;

	if(FD_ISSET(0,&read_fd))
		return 1;

	return 0;
}

/* ---------------------------------------------------------------------------
 * clearArrays - Rellena de ceros un arrays
 * --------------------------------------------------------------------------- */
void clearArrays(double* arrays,int size)
{
	int i;
	for(i=0;i<size;i++)
	{
		arrays[i]=0;
	}
}

/* ---------------------------------------------------------------------------
 * calculateDistance - Calcula la distancia entre dos robots
 * --------------------------------------------------------------------------- */
double calculateDistance(playerc_position2d_t** sim_position2d, int my_robot, int other_robot)
{
	double mpx,opx,mpy,opy;
	double distance;		

	mpx=sim_position2d[my_robot]->px;
	mpy=sim_position2d[my_robot]->py;

	opx=sim_position2d[other_robot]->px;
	opy=sim_position2d[other_robot]->py;

	distance=sqrt(pow(mpx-opx,2)+pow(mpy-opy,2));

	return distance;
}

/* ---------------------------------------------------------------------------
 * calculateDistance2 - Calcula la distancia entre dos robots
 * --------------------------------------------------------------------------- */
double calculateDistance2(performance_data_t* p_data, int my_robot, int other_robot)
{
	double mpx,opx,mpy,opy;
	double distance;		

	mpx=p_data->positions[my_robot].px;
	mpy=p_data->positions[my_robot].py;

	opx=p_data->positions[other_robot].px;
	opy=p_data->positions[other_robot].py;

	distance=sqrt(pow(mpx-opx,2)+pow(mpy-opy,2));

	if(distance<=0.000000000000000001)
	{
		return 0;
	}

	//printf("Distancia entre %d y %d = %f\n",my_robot+1,other_robot+1,distance);

	return distance;
}

/* ---------------------------------------------------------------------------
 * makePause - Genera una pausa para el usuario
 * --------------------------------------------------------------------------- */
void makePause()
{
	printf("(Enter c to continue)\n");
	char c_reader='0';
	while(c_reader!='c')
	{
		scanf("%c",&c_reader);
		
	}
}

/* ---------------------------------------------------------------------------
 * maximo - Calcula el maximo valor de un arrays
 * --------------------------------------------------------------------------- */
double maximo(double* arrays)
{
	int size=sizeof(arrays)/sizeof(double);

	int i;
	double max=0;
	for(i=0;i<size;i++)
	{
		if(arrays[i]>max)
		{
			max=arrays[i];
		}
	}	
	return max;
}
