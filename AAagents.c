/*---------------------------------------------------------------------------
 Author: Gita
 * --------------------------------------------------------------------------- 
 Adaptive Autononous Agents for WSN
 * ---------------------------------------------------------------------------
*/  

#include "AAagents.h"
#include "APjer.h" //Para aprovechar la funcion ChooseAP
#include <math.h>
#include <stdlib.h>

float calculate_willingness()//gets array of factors.
{
	return (float)(rand()) / (float) (RAND_MAX);
}

float calculate_utility()//gets distance from current location to destination.
{
	return (float)(rand()) / (float) (RAND_MAX);
}

tasks_data* create_tasks(tasks_data* t_data, int num_robots, int mapsize, int* broken) //should return the data structure array or array of data structures?
{
	int br = 0;
	//Count how many broken robots at startup
	if(!broken==NULL)
		for(int i=0; i< num_robots; i++)
		{
			printf("\nbroken %d", broken[i]);
			if(broken[i] == 1) br++;
		}

    // create as many tasks as active robots
	t_data->num_tasks = num_robots-br;
	t_data->xpos = (float*) malloc(sizeof(float) * t_data->num_tasks);
	t_data->ypos = (float*) malloc(sizeof(float) * t_data->num_tasks);

    // Calculate size of the grid
	int gridx, gridy, factors[t_data->num_tasks], no_factors, idx;
	no_factors = 0;
	idx = 0;
	for(int i=1;i<=t_data->num_tasks;i++)
		{
			if(t_data->num_tasks%i == 0)
			{
				factors[idx] = i;
				no_factors++;
				printf("\n factor: %d, i %d", factors[idx], idx);
				idx++;
			}
		}

	//printf("\n factor needed: %d", factors[(int)ceil(no_factors/2) - 1]);
	gridx = factors[(int)ceil(no_factors/2)];
	gridy = t_data->num_tasks/gridx;

	// Calculate locations of tasks.
	// TODO take into consideration that tasks shouldn't be so far tha connection is lost.
	printf("\ngridx %d, gridy: %d", gridx, gridy);

	for(int i=0; i<gridx; i++) //population is done column by column
		for(int j=0; j<gridy; j++)
		{
			t_data->xpos[i+j*gridx] = -(float)mapsize/2 + (float)mapsize/(2*gridx) + i*(float)mapsize/gridx;
			t_data->ypos[i+j*gridx] = -(float)mapsize/2 + (float)mapsize/(2*gridy) + j*(float)mapsize/gridy;
			printf("\nidx: %d, xpos: %f, ypos: %f", i+j*gridx, t_data->xpos[i+j*gridx], t_data->ypos[i+j*gridx]);
		}

	return t_data;
}

assigned_robots* initial_negotiation(tasks_data* t_data, int num_robots, int mapsize, int* broken)
{
	assigned_robots tutu;
	assigned_robots* ar_data=&tutu;
	ar_data->robots = (int*) malloc(sizeof(int) * t_data->num_tasks);
	ar_data->tasks_assigned = (int*) malloc(sizeof(int) * t_data->num_tasks);
	ar_data->xpos = (float*) malloc(sizeof(float) * t_data->num_tasks);
	ar_data->ypos = (float*) malloc(sizeof(float) * t_data->num_tasks);
	for(int x = 0; x < t_data->num_tasks; x++)
	{
		ar_data->robots[x] = -1;
		ar_data->tasks_assigned[x] = -1;
		ar_data->xpos[x] = -1.0;
		ar_data->ypos[x] = -1.0;
	}

	// for loop for each target
	float willingness[num_robots];
	float utility[num_robots];
	for(int i=0; i<t_data->num_tasks; i++)
	{
		// mark target as unassigned. --> all targets in the initial round are considered as unassigned, thus no need for an explicit declaration here.
		// all agents consider the target
		// each agent appends its willingness+utility for this target --> greedy
		for (int j=0; j<num_robots; j++)
		{
				willingness[j] = calculate_willingness(); //TODO if robot is broken it should reflect here.
				utility[j] = calculate_utility();
				printf("\n %f %f, sum %f", willingness[j], utility[j], willingness[j] + utility[j]);
		}

		// Select the highest w+u. If many the same, choose randomly. In principle, mapping to my ROS simulation, one agent would collect this data and choose highest
		float max = 0.0;
		int selected = -1;
		for(int k=0; k<num_robots; k++)
		{
			//At this negotiation assign each robot to one task
			// Find if robot has been assigned already
			int already_assigned = 0;
			for(int r=0; r<t_data->num_tasks; r++)
			{
				if(k+1 == ar_data->robots[r])
					{
					already_assigned = 1;
					break;
					}
			}
			//If it has not been assigned consider its willingness.
			if(already_assigned == 0)
			{
				if (willingness[k] > 0)
				{
					willingness[k] += utility[k];
					if (willingness[k] > max)
					{
						max = willingness[k];
						selected = k;
					}
				}
			}
		}
		if(!(selected == -1))
		{
			printf("\nAssign to %d", selected+1);
			ar_data->tasks_assigned[i] = i + 1;
			ar_data->robots[i] = selected+1;
			ar_data->xpos[i] = t_data->xpos[i];
			ar_data->ypos[i] = t_data->ypos[i];

			printf("\n robot %d, task %d, xpos %f, ypos %f", ar_data->robots[i], ar_data->tasks_assigned[i], ar_data->xpos[i], ar_data->ypos[i]);
		}
		else
		{
			printf("\n PROBLEM: Task %d not assigned", i+1);

		}

	}

	return ar_data;
}

void call_for_help() //modify the message_init_allocation strucutre
{

}

void read_requests() // will return array with indexes for where help is needed
{

}

void should_I_help()
{

}

void unicycle_dynamics(assigned_robots* ar_data, playerc_position2d_t** sim_position2d, robot_data_t* robot_data, int number_robots, int num_rotos, int map_size, int my_robot, double* x, double* y,int obstaculos)
{
	//TODO consideration of obstacles
	int dont_move = 0;
	*x=0;
	*y=0;

	if((robot_data[my_robot].broken==1)||(robot_data[my_robot].level!=0))
		{
			// If robot is broken or AP don't move it
			dont_move=1;
		}
		else
		{
			*x = 0 - sim_position2d[my_robot]->px;
			*y = 0 - sim_position2d[my_robot]->py;
		}
}

void move(assigned_robots* ar_data, playerc_position2d_t** sim_position2d, robot_data_t* robot_data, int my_robot, int number_robots, int num_rotos, int map_size,int obstaculos)
{
	double x;
		double y;
		double modulo;
		double angulo;
		double my_angle=sim_position2d[my_robot]->pa;


		//Calcula el vector resultante de todas las fuerzas
		//vectorTotal(sim_position2d,robot_data,number_robots,num_rotos,map_size,my_robot,&x,&y,obstaculos);
		unicycle_dynamics(ar_data, sim_position2d,robot_data,number_robots,num_rotos,map_size,my_robot,&x,&y,obstaculos);

		//Solo si el modulo de ese vector es mayor que un cierto valor, muevo al robot
		modulo=sqrt(pow(x,2)+pow(y,2));

		//if (my_robot==0) printf("vect %f %f ",x,y); XXXX DEPURACION

		if (modulo<UMBRAL_MOVIMIENTO)
		{
			playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,0,0);
			playerc_position2d_set_cmd_car(sim_position2d[my_robot],0,0);

			//XXXX: Faltaba aqui????...
			robot_data[my_robot].stopped=1;

		}
		else
		{

			robot_data[my_robot].stopped=0; //XXXX. Faltaba aqui!!!

			//Calcula el ángulo
			angulo = atan2(y/modulo,x/modulo); //Devuelve un valor entre -pi/2 y pi/2

			if(fabs(my_angle-angulo) > 0.1)
			{
				// Modo giro
				playerc_position2d_set_cmd_car(sim_position2d[my_robot],0,0);

				if(angulo>0)
				{
					if(my_angle<0)
					{
						if (my_angle+3.1415<angulo)
						{
							playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,-0.5,0); //giro agujas relog
						}
						else
						{
							playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,0.5,0); //giro contrario
						}
					}
					else
					{
						if (my_angle>angulo)
						{
							playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,-0.5,0); //giro agujas relog
						}
						else
						{
							playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,0.5,0); //giro contrario
						}
					}
				}
				else
				{
					if(my_angle>0)
					{
						if (my_angle-3.1415<angulo)
						{
							playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,-0.5,0); //giro agujas relog
						}
						else
						{
							playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,0.5,0);//giro contrario
						}
					}
					else
					{
						if (my_angle>angulo)
						{
							playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,-0.5,0); //giro agujas relog
						}
						else
						{
							playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,0.5,0);//giro contrario
						}
					}
				}
			}

			else
			{
				playerc_position2d_set_cmd_car(sim_position2d[my_robot],0.05,0);
			}
		}
}

deployment_data_t* simAAagents(deployment_data_t* d_data, tasks_data* t_data, assigned_robots* ar_data, int* rotos,int obstaculos,playerc_client_t **sim_robots, playerc_position2d_t** sim_position2d,playerc_simulation_t** simulation, playerc_graphics2d_t** sim_graphics2d,playerc_sonar_t** sim_sonar)
{
		/*****************************************************************************************************
		 *********************************** SAME AS adhocSPF ************************************************
		 *****************************************************************************************************/

		//Declaro variables
		int i,contador=0,stop=0,n=0,stopped=0,LastStopped=0;
		srand(time(NULL));

		double mean_power;

		int reader,salir,level;

		int number_robots=d_data->number_robots;
		int map_size=d_data->map_size;

		double* msj_tx=(double*) malloc(sizeof(double) * number_robots);

		//Inicializo variables
		for(i=0;i<number_robots;i++)
		{
			if(d_data->nueva_simulacion)
			{
				//Variables algoritmo
				d_data->robot_data[i].turn=0;
				d_data->robot_data[i].turning_count=0;
				d_data->robot_data[i].walking_count=0;
				d_data->robot_data[i].backbone=0;
				d_data->robot_data[i].stopped=0;
				d_data->robot_data[i].level=0;
				d_data->robot_data[i].registered_to=-1;
				d_data->robot_data[i].num_registered=0;
				d_data->p_data->distance_walked[i]=0; //XXXX. Movido aqui
				d_data->robot_data[i].remaining_battery=ROBOT_INITIAL_BATTERY;
				d_data->FinalAP=-1;
			}
			else
			{
				if(rotos!=NULL)
				{
					if (rotos[i])
					{
						//XXXX. Acumulativo
						//TODO this below you should UNCOMMENT
						//BreakSPF(sim_position2d,d_data->robot_data,i,number_robots,&(d_data->FinalAP));
					}
				}
			}

			d_data->p_data->packets_sent[i]=0;
			d_data->p_data->own_packets_sent[i]=0;
			d_data->p_data->packets_rx[i]=0;
			d_data->p_data->own_packets_rx[i]=0;

		}

		//Robots tell where they are, pose and odometrics and move a bit
		for(i=0;i<number_robots;i++)
		{

			if (d_data->nueva_simulacion)
			{
				playerc_position2d_set_cmd_car(sim_position2d[i],0.005,0);
			}
			if(i%10==0) playerc_client_read(*sim_robots);

			char spider[10];
			sprintf(spider,"spider%d",i+1);

			double px,py,pa;
			playerc_simulation_get_pose2d(*simulation, spider, &px,&py,&pa);
			//printf("Robot  %d empieza en: %f %f %f\n", i,px,py,pa);

			if(i%10==0) playerc_client_read(*sim_robots);

			d_data->p_data->positions[i].px=px;
			d_data->p_data->positions[i].py=py;
			d_data->p_data->positions[i].pa=pa;
		}

		for(i=0;i<number_robots;i++)
		{
			playerc_position2d_set_cmd_car(sim_position2d[i],0,0);
			if(i%10==0)
				playerc_client_read(*sim_robots);
		}


		//Added by XXXX. Escogemos un nodo como AP....
		if (d_data->nueva_simulacion)
		{
			d_data->FinalAP=chooseAP(sim_position2d, d_data->robot_data,number_robots,0,-1);
		}
		d_data->robot_data[d_data->FinalAP].stopped=1;
		d_data->robot_data[d_data->FinalAP].level=1; //No hay nivel 2...

		printf("AP escogido %i",d_data->FinalAP);
		/*****************************************************************************************************
		 *****************************************************************************************************/

		/*****************************************************************************************************
		 *********************************** PLACEHOLDER for AGENT INIT code *********************************
		 *****************************************************************************************************/
		if (d_data->nueva_simulacion)
		{
		// how about the initial assignment? --> the initial assignment in which the area is split
		// into as many grid cells as there are agents is done only once at the beginning FOR NOW.
		// create targets on the grid.
			t_data = create_tasks(t_data, number_robots, map_size, rotos);
			//TODO PLOT this data as crosses or smth. --> not so trivial as it seems.
			//TODO don't let the robots go further such that connectivity breaks
		// start initial negotiation protocol. Assign one robot to a target. Assumes that all robots are eligible for assignment in this round, i.e., willingness for
			//each is positive.
			ar_data = initial_negotiation(t_data, number_robots, map_size, rotos); //TODO don't assign broken robots
		}
		/*****************************************************************************************************
		 *****************************************************************************************************/

	// --------------------------------------------		DEPLOYMENT		----------------------------------------------------

		time_t comienzo=time(NULL);
		time_t LastTime=time(NULL);
		time_t final,comienzo_pausa,final_pausa;
		double pause_time=0;

		//sleep(2); //XXXX. Pausa Para poder ver los mensajes

		//system("reset");
		printf("\nPara pausar la simulación y mostrar el menu de simulación pulse enter\n");

		//Start Algorithm
		while(stop!=1)
		{
			/*****************************************************************************************************
			 *********************************** PLACEHOLDER for AGENT code **************************************
			 *****************************************************************************************************/

			//Wait for new data from server
			playerc_client_read(*sim_robots);

			// calculate willingness

			// check if I need to ask for help. Can agents get broken at this stage?? No, they come broken into this function.

			// read message data structure --> return array of indexes where help is needed.

			// msg_data, index array --> decide on own involvement --> treat each request one by one
				//re calculate willingness for each new target
				//calculate utility
				//decide whether to take it on.
				// marked as assisted in the message data structure based on list of accepted indexes. add to the array with locations to be covered.

			printf("\nThe beginning of the end\n");
			for(i=0;i<number_robots;i++)
			{
				//TODO Move function, using unicycle dynamics.
				// move using uni-cycle dynamics.
				//0)Si esta roto no hace nada. (Lo mismo me lo llevo hacia abajo...)
				if(d_data->robot_data[i].broken==1)
				{
					//printf("Robot %d está roto.\n",i);
					move(ar_data,sim_position2d,d_data->robot_data,i,number_robots,d_data->num_rotos,map_size,obstaculos);
				}
				//XXXX ¿Hace lo mismo en ambos casos?
				// Si no esta roto se mueve
				else
				{
					//printf("Robot %d no está roto.\n",i);
					move(ar_data,sim_position2d,d_data->robot_data,i,number_robots,d_data->num_rotos,map_size,obstaculos);
				}

				//Update positions and calculate distance walked
				player_pose2d_t new_pose;
				new_pose.px= sim_position2d[i]->px;
				new_pose.py= sim_position2d[i]->py;
				new_pose.pa= sim_position2d[i]->pa;

				d_data->p_data->distance_walked[i]+=sqrt(pow(d_data->p_data->positions[i].px-new_pose.px,2)+pow(d_data->p_data->positions[i].py-new_pose.py,2));
				d_data->p_data->positions[i]=new_pose;

				//Send broadcast with position (receiver=-1, estrategia=0)
				clearArrays(msj_tx,number_robots);
				sendAdhoc(d_data, i, i, -1, msj_tx, 0);
				//sendAdhoc(d_data, i, i, -1, msj_tx, 0); //XXX

				//Print graphics over robots
				dibuja(d_data->robot_data, sim_position2d, sim_graphics2d, i, number_robots, d_data->tipo_red,d_data->algoritmo);

				//sleep(0.4);
			}
			/*****************************************************************************************************
			 *****************************************************************************************************/

			/*****************************************************************************************************
			*********************************** SAME AS adhocSPF *************************************************
			******************************************************************************************************/
			//Calculo potencia media:
			if(!contador)
			{
				if(n<1000)
				{

					system("clear");
					d_data->p_data->net_mean_power[n]=netMeanPower(d_data,0);
					printf("Potencia media en la red = %lf\n",d_data->p_data->net_mean_power[n]);
				}
				n++;
			}
			contador=(contador+1)%NET_MEAN_POWER_ITERATIONS;

			//Miro si ha terminado el algoritmo
			stop=1;
			stopped=number_robots;
			for(i=0;i<number_robots;i++)
			{
				//If there is a robot moving and it's not broken, go on
				if(d_data->robot_data[i].stopped==0&&d_data->robot_data[i].broken!=1)
				{
					stop=0;
					stopped--;
				}
			}
			//stopped = 6; // REMOVE this line later
			//Parada forzada cuando la red se estanca pero los nodos se siguen moviendo un poco....
			if (stopped>=0.92*number_robots)
			{

	//			if (LastStopped!=stopped)
	//			{	//Si ha cambiado el numero de robots parados, retomamos el tiempo
	//				LastTime=time(NULL);
	//				LastStopped=stopped;
	//			}
				if (difftime(time(NULL),LastTime)>(120.0*(number_robots-stopped)))
				{	//Esperamos 2 minutos por robot no parado.
					stop=1;
				}
			}
			else
			{
				LastTime=time(NULL);
				//LastStopped=stopped;
			}


			if(kbhit())
			{
				comienzo_pausa=time(NULL);

				reader=-1;
				salir=0;

				for(i=0;i<number_robots;i++)
				{
					playerc_position2d_set_cmd_vel(sim_position2d[i],0,0,0,0);
				}

				while(salir==0)
				{
					system("clear");
					printf ("------------------  MENU DESPLIEGUE  ------------------\n");
					printf("Acciones posibles durante el despliegue:\n");
					printf("0) Reanudar simulación.\n");
					printf("1) Romper/Arreglar un spider.\n");
					printf("2) Terminar despliegue.\n");

					fflush(stdin);
					scanf("%d",&reader);

					while(reader<0||reader>3)
					{
						printf("Introduzca una de esas opciones, por favor...\n");
						fflush(stdin);
						scanf("%d",&reader);
					}
					switch(reader)
					{
						case 0:
							salir=1;
							break;

						case 1:
							printf("Introduce el número del spider a modificar\n");
							fflush(stdin);
							scanf("%d",&reader);
							//changeBroken(sim_position2d,d_data->robot_data,reader,number_robots);
							salir=1;
							break;

						case 2:
							final_pausa=time(NULL);
							pause_time+=difftime(final_pausa,comienzo_pausa);
							final=time(NULL);
							d_data->p_data->depl_time=difftime(final,comienzo)-pause_time;
							free(msj_tx);
							d_data->p_data->num_iterations=n*NET_MEAN_POWER_ITERATIONS;
							return d_data;
					}
				}

				printf("Para pausar la simulación y mostrar el menu de simulación pulse enter\n");

				final_pausa=time(NULL);
				pause_time+=difftime(final_pausa,comienzo_pausa);
			}
		}

		final=time(NULL);
		d_data->p_data->depl_time=difftime(final,comienzo)-pause_time;


		//Aqui acaba el despliegue por lo que parece ---> determinar que nodos mueren y lanzar otro...


		printf("El algoritmo de despliegue ha terminado.\n",i);

		sleep(2);

		d_data->p_data->num_iterations=n*NET_MEAN_POWER_ITERATIONS;
		free(msj_tx);
		/*****************************************************************************************************
		 *****************************************************************************************************/

		return d_data;
}

