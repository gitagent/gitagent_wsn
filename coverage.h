/*---------------------------------------------------------------------------
 *  Proyecto Fin de Carrera: Enjambre de microbots para despliegue autónomo de 
 *                           una red inalámbrica mallada.
 * 
 *  Francisco José Aguilera Leal
 *  Universidad de Málaga
 * --------------------------------------------------------------------------- 
 * Archivo de cabecera de coverage.c
 * ---------------------------------------------------------------------------
*/

#ifndef COVERAGE_H 
#define COVERAGE_H

#include <math.h>
#include "datatypes.h"

#define GRID_CELD	0.2
#define ROBOT_COVERAGE	1.6

double calculateCoverage(deployment_data_t* d_data);
void dibujaCoverage(deployment_data_t* d_data,playerc_client_t** sim_robots, playerc_graphics2d_t** sim_graphics2d);

#endif /*COVERAGE_H*/
