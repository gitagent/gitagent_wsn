/*---------------------------------------------------------------------------
 *  Proyecto Fin de Carrera: Enjambre de microbots para despliegue autónomo de 
 *                           una red inalámbrica mallada.
 * 
 *  Francisco José Aguilera Leal
 *  Universidad de Málaga
 * --------------------------------------------------------------------------- 
 * Archivo de cabecera de dibuja.c
 * ---------------------------------------------------------------------------
*/  

#ifndef DIBUJA_H
#define DIBUJA_H

#include <math.h>
#include <libplayerc/playerc.h>
#include "config.h"
#include "datatypes.h"
#include "auxFunctions.h"

void dibuja(robot_data_t* robot_data, playerc_position2d_t** sim_position2d, playerc_graphics2d_t** sim_graphics2d, int my_robot, int number_robots, network_type_t tipo_red, algorithm_t algoritmo);

#endif /*DIBUJA_H*/
