/*---------------------------------------------------------------------------
 *  Proyecto Fin de Carrera: Enjambre de microbots para despliegue autónomo de 
 *                           una red inalámbrica mallada.
 * 
 *  Francisco José Aguilera Leal
 *  Universidad de Málaga
 * --------------------------------------------------------------------------- 
 * Main con el menu principal.
 * ---------------------------------------------------------------------------
*/


#include <stdio.h>
#include <libplayerc/playerc.h>
#include "config.h"
#include "datatypes.h"
#include "auxFunctions.h"
#include "rendimiento.h"
#include "adhocBA.h" 		// version con BA
#include "adhocSPF.h" 		// version con SPF
#include "APjer.h" 		// version con SPF
#include "AAagents_v2.h" 		// version con SPF
//#include "APjer3.h" 		// version con SPF y obst no conocidos

int main(int argc,const char **argv)
{

	/* ------------------------ MENU INICIAL -----------------------------*/

	int i_reader,opcion=0,no_reset=0,terminar_desp=0,terminar_prog=0;
	char c_reader;
	int n_robots=0;

	int AA_alg_delegation_strategy = -1;

	int map_size=0;
	int escenario=0;
	int* rotos=NULL;
	int obstaculos=0;
	double accum_cycles=0.0;


	deployment_data_t dummy2;	
	deployment_data_t* d_data=&dummy2;

	performance_data_t dummy;
	d_data->p_data=&dummy;
	d_data->p_data->packets_sent=NULL;
	d_data->p_data->wasted_energy=NULL;
	d_data->p_data->own_packets_sent=NULL;
	d_data->p_data->distance_walked=NULL;
	d_data->p_data->positions=NULL;
	d_data->robot_data=NULL;

	long int id_player,id_xterm;

	system("reset");

	printf ("Programa de simulación del despliegue de un enjambre de robots spider\n");
	printf ("Autor: Francisco José Aguilera Leal\n");


	if (argc<3)
	{

		printf("Argument mismatch\n\n");
		return;
	}

	FILE *fichero=fopen(argv[1],"a");
	int puerto=strtol(argv[2],NULL,10);


	while(!terminar_prog)
	{
		/* ------------------------ NUEVO DESPLIEGUE -----------------------------*/

		printf ("\n-----------------  NUEVO DESPLIEGUE  -----------------\n");
		printf ("Choose strateg 1 or 2:\n");
		printf ("1) go to middle point between old on location(s)\n");
		printf ("2) drop old position and go to new one(s) \n");

		while(AA_alg_delegation_strategy==-1)
		{
			scanf("%d",&i_reader);
			if(i_reader==1)
			{
				AA_alg_delegation_strategy = 1;
			}
			else if(i_reader==2)
			{
				AA_alg_delegation_strategy = 2;
			}
		}

		system("clear");

		n_robots=0;
		terminar_desp=0;
		escenario=0;
		d_data->nueva_simulacion=1;
		/* ------------------------ NUEVO DESPLIEGUE -----------------------------*/

		printf ("\n-----------------  NUEVO DESPLIEGUE  -----------------\n");
		printf ("Elija el tamaño del enjambre con un número del 1 al 4:\n");
		printf ("1) 2 robots.\n");
		printf ("2) 6 robots.\n");
		printf ("3) 20 robots.\n");
		printf ("4) 100 robots.\n");

		while(n_robots==0)
		{
			scanf("%d",&i_reader);
			if(i_reader==1)
			{
				n_robots=2;
				map_size=2;
			}
			else if(i_reader==2)
			{
			
				n_robots=6;
				map_size=4;
			}
			else if(i_reader==3)
			{
				n_robots=20;
				map_size=6;
			}
			else if(i_reader==4)
			{
				n_robots=100;
				map_size=15;
			}
		}
	
		system("clear");
		printf ("\n-----------------  NUEVO DESPLIEGUE  -----------------\n");
		printf("¿Qué mapa desea?\n");
		printf("0: Mapa sin obstaculos\n");
		printf("1: Mapa con obstaculos 1\n");
		printf("2: Mapa con obstaculos 2\n");
		i_reader=-1;
		while(i_reader>2||i_reader<0)
		{
			scanf("%d",&i_reader);
		}

		obstaculos=i_reader;	
		char comando[256];

		if(n_robots==2)
		{
			if(i_reader==0)
			{
				sprintf(comando,"xterm -e player -p %i /usr/share/stage/worlds/sim2.cfg &",puerto); //gnome-terminal, eterm, aterm otras formas de hacerlo.
				system(comando);


			}
			else if(i_reader==1)
			{
				sprintf(comando,"xterm -e player -p %i /usr/share/stage/worlds/sim2obs1.cfg &",puerto);
				system(comando);
			}
			else if(i_reader==2)
			{
				sprintf(comando,"xterm -e player -p %i /usr/share/stage/worlds/sim2obs2.cfg &",puerto);
				system(comando);
			}
		}
		else if(n_robots==6)
		{
			if(i_reader==0)
			{
				sprintf(comando,"xterm -e player -p %i /usr/share/stage/worlds/sim6.cfg &",puerto);
				system(comando);
			}
			else if(i_reader==1)
			{
				sprintf(comando,"xterm -e player -p %i /usr/share/stage/worlds/sim6obs1.cfg &",puerto);
				system(comando);
			}
			else if(i_reader==2)
			{
				sprintf(comando,"xterm -e player -p %i /usr/share/stage/worlds/sim6obs2.cfg &",puerto);
				system(comando);
			}
		}
		else if(n_robots==20)
		{
			if(i_reader==0)
			{
				sprintf(comando,"xterm -e player -p %i /usr/share/stage/worlds/sim20.cfg &",puerto);
				system(comando);
			}
			else if(i_reader==1)
			{
				sprintf(comando,"xterm -e player -p %i /usr/share/stage/worlds/sim20obs1.cfg &",puerto);
				system(comando);
			}
			else if(i_reader==2)
			{
				sprintf(comando,"xterm -e player -p %i /usr/share/stage/worlds/sim20obs2.cfg &",puerto);
				system(comando);
			}
		}
		else if(n_robots==100)
		{
			if(i_reader==0)
			{
				sprintf(comando,"xterm -e player -p %i /usr/share/stage/worlds/sim100.cfg &",puerto);
				system(comando);
			}
			else if(i_reader==1)
			{
				sprintf(comando,"xterm -e player -p %i /usr/share/stage/worlds/sim100obs1.cfg &",puerto);
				system(comando);
			}
			else if(i_reader==2)
			{
				sprintf(comando,"xterm -e player -p %i /usr/share/stage/worlds/sim100obs2.cfg &",puerto);
				system(comando);
			}
		}

		system("clear");
		printf ("Cargando enjambre de %d robots.\n",n_robots);
		printf ("\n-----------------  NUEVO DESPLIEGUE  -----------------\n");
		printf ("Elija que escenario desea simular:\n");
		printf ("1) Red Ad-Hoc.\n");
		printf ("2) Red Ad-Hoc con potential fields.\n");
		printf ("3) Red jerárquica con Access Point.\n");
		printf ("4) Red jerárquica con cambio de rol\n");
		printf ("5) Red with Adaptive Autonomous agents\n");

		while(escenario<1||escenario>5)
		{
			scanf("%d",&i_reader);
			if(i_reader==1)
			{
				escenario=1;
				d_data->tipo_red=ADHOC;
				d_data->algoritmo=BA;
			}
			else if(i_reader==2)
			{
				escenario=2;
				d_data->tipo_red=ADHOC;
				d_data->algoritmo=SPF;
			}
			else if(i_reader==3)
			{
				escenario=3;
				d_data->tipo_red=JERARQUIZADA;
				d_data->algoritmo=SPF;
			}
			else if(i_reader==4)
			{
				escenario=4;
				d_data->tipo_red=JERARQUIZADA;
				d_data->algoritmo=SPF;
			}

			else if(i_reader==5)
			{
				escenario=5;
				d_data->tipo_red=ADHOC;
				d_data->algoritmo=AAA;
			}
		}

	

		if(!no_reset)
		{
			system("reset");
		}
		no_reset=0;

		/* --------------- Prepara variables y abre proxies -------------------*/

		inicializaDatosDespliegue(d_data,n_robots,map_size);

		id_player=atoi(cmd_system("pidof player"));
		id_xterm=atoi(cmd_system("pidof xterm"));

		//Create proxies and configure them
		playerc_client_t *sim_robots;
		playerc_position2d_t* sim_position2d[d_data->number_robots];
		playerc_sonar_t* sim_sonar[d_data->number_robots];
		playerc_simulation_t* simulation;
		playerc_graphics2d_t* sim_graphics2d[d_data->number_robots];

		if(!configuraProxys(&sim_robots,sim_position2d,sim_sonar,&simulation, sim_graphics2d, d_data->number_robots,puerto))
		{		
			printf("Hay un problema para configurar los proxys\n");
			return -1;
		}


		/* ------------------------- MENU PRINCIPAL ---------------------------*/

		while(!terminar_desp)
		{
			system("clear");
			printf ("------------------  MENU PRINCIPAL  ------------------\n");
			printf ("Elija una opción:\n");
			printf ("1) Lanzar algoritmo.\n");
			printf ("2) Lanzar algoritmo con uno o mas nodos rotos.\n");
			printf ("3) Mostrar graficas de rendimiento.\n");
			printf ("4) Terminar despliegue.\n");	
	
			opcion=-1;
			while(opcion>4||opcion<1)
			{
				scanf("%d",&opcion);
			}

			if(opcion==1 && rotos!=NULL)
			{
				free(rotos);
				rotos=NULL;
			}

			// Menu para seleccionar los nodos rotos
			if(opcion==2)
			{
				printf("¿Cuántos nodos rotos? (máximo %d)\n", n_robots/2);
				i_reader=0;
				while(i_reader<1||i_reader>n_robots/2)
				{
					scanf("%d",&i_reader);
				}
				//int num_rotos=i_reader;
				d_data->num_rotos=i_reader;
		
				//XXXX.
				//rotos=(int*) malloc(sizeof(int) * n_robots);
				rotos=(int*) calloc(n_robots, sizeof(int)); //XXXX. Mejor CALLOC, inicializa a 0

				while(d_data->num_rotos>0)
				{
					printf ("Elija un nodo roto:\n");
					i_reader=-1;		
					while(i_reader<0||i_reader>=n_robots||rotos[i_reader]==1)
					{	
						printf (" Escriba número del 0 al %d que no haya sido ya escogido.\n", n_robots-1);
						scanf("%d",&i_reader);
					}
					d_data->num_rotos--;
					rotos[i_reader]=1;
				}
			}

			// LLamo al escenario y opcion elegidas.
			if(opcion==3)
			{	
				if(!d_data->nueva_simulacion)
				{
					printf("Se muestran estadisticas\n");
					rendimiento(d_data,&sim_robots,sim_position2d,sim_graphics2d);
				}
				else
				{
					printf("Aun no se ha realizado ninguna simulación\n");	
					makePause();		
				}
				no_reset=1;	
			}
			else if(opcion==4)
			{
				borraDatosDespliegue(d_data,rotos);
				
				//Shutdown proxies
				cierraProxys(&sim_robots,sim_position2d,sim_sonar,&simulation,sim_graphics2d,d_data->number_robots);
				
				system("reset");

				c_reader='0';
				printf("¿Desea iniciar un nuevo despliegue o terminar el programa?(d/t)\n");
				while(c_reader!='d'&&c_reader!='t')
				{
					scanf("%c",&c_reader);
				}
		
				if(c_reader=='t')
				{
					terminar_prog=1;
				}

				terminar_desp=1;

				char cadena[10];
				//sprintf(cadena,"kill %ld",id_player);
				sprintf(cadena,"kill %ld",id_xterm);
				system(cadena);
			}
			else
			{
				//XXXX. Esto es un poco chapuza, mover a otro lado
				double* msj_tx=(double*) malloc(sizeof(double) * d_data->number_robots);	//XXXX. msg_tx la utiliza como booleano, a pesar de que es doble ---> CAMBIAR CUANDO SE PUEDA!!!
				double* remaining_cycles=(double*) malloc(sizeof(double) * d_data->number_robots);
				double distancia_recorrida_anterior[d_data->number_robots];//Igualmente chapuza, aunque sin malloc
				//int num_rotos=0;
				d_data->num_rotos=0;

				if(d_data->nueva_simulacion==0)
				{
					c_reader='0';
					printf("¿Desea continuar con los datos del anterior despliegue?(s/n)\n");
					while(c_reader!='s'&&c_reader!='n')
					{
						scanf("%c",&c_reader);
					
					}
					d_data->nueva_simulacion=0;
					if(c_reader=='n')
					{
						d_data->nueva_simulacion=1;
					}
				}
				if(escenario==1)
				{
					//d_data=simAdhocBA(d_data,rotos,&sim_robots,sim_position2d,&simulation,sim_graphics2d,sim_sonar);
					printf("Este escenario is no longer available, compadre <-- God bless you comrade\n");
				}
				else if(escenario==2)
				{
					int fotograma=0;


					//MAJOR_CHANGE
					clearArrays(distancia_recorrida_anterior,d_data->number_robots);

					fprintf(fichero,"SPF %i nodos\n",d_data->number_robots);
					fprintf(fichero,"ciclos \t rotos \t rel_cove \t cov \t uniform \t  mean_dist \t alive \t mean_power \t mean_snd \t mean_rx \t st_dev_sent \t st_dev_rx \t min_sent \t max_sent \t meantx_noAP \t devtx_noAP min_sent_noAP \t max_sent_noAP \n ");

					//XXXX. De momento simulo hasta que se hayan roto una fraccion. Ajustar mas adelante.
					while (d_data->num_rotos<(0.70*d_data->number_robots))
					{
						d_data=simAdhocSPF(d_data,rotos,obstaculos,&sim_robots,sim_position2d,&simulation,sim_graphics2d,sim_sonar);
						printf("Iteracion finalizada!\n");

//						if ((d_data->nueva_simulacion)||((fotograma==1)&&(d_data->num_rotos>0.125*d_data->number_robots))||((fotograma==2)&&(d_data->num_rotos>0.25*d_data->number_robots))||((fotograma==3)&&(d_data->num_rotos>0.50*d_data->number_robots)))
//						{
//							printf("Porfa, saca la fotito....\n");
//							fotograma++;
//							//while (!kbhit());
//							makePause();
//						}

						d_data->nueva_simulacion=0;

						fprintf(fichero," %lf \t %i \t",accum_cycles,d_data->num_rotos); //XXXX. Guarda el tiempo y el numero de rotos
						save_performance(fichero,d_data); //XXXX. Calcula y guarda las estadisticas

						if (rotos==NULL) rotos=(int*) calloc(d_data->number_robots, sizeof(int)); //Por si no esta inicializado previamente

						int i;
						for (i=0;i<d_data->number_robots; i++)
						{
							//El AP no entra en la cuenta en esta red
							if ((d_data->robot_data[i].level==0)&&(!d_data->robot_data[i].broken))
							{
								d_data->robot_data[i].remaining_battery-=(d_data->p_data->distance_walked[i]-distancia_recorrida_anterior[i])*BATTERY_WASTE_PER_DISTANCE; //Cuidado con la escala!!
								distancia_recorrida_anterior[i]=d_data->p_data->distance_walked[i];
//								if (d_data->robot_data[i].remaining_battery)
//								{
//									rotos[i]=1; //Marca el robot comor roto. Este robot ya no genera ni enruta trafico....
//								}
								// XXXX. NUeva simulacion. Esto es complicadillo, porque requiere un cambio en la topologia
								//que aqui no se simula
								// por ello mejor lo que hago más adelante es forzar una segunda iteración si mueren nodos durante el movimento

							}
						}


						//Vamos a Calcular calcular cuantas transmisiones hace cada uno
						//Se podria simplificar a cuantos paquetes puede enviar un robot antes de morir,
						// pero bueno, por si mas adelante complicamos el modelo de momento voy calculando

						//Reseteamos las estadisticas...
						clearArrays(d_data->p_data->packets_sent,d_data->number_robots);
						clearArrays(d_data->p_data->own_packets_sent,d_data->number_robots);
						clearArrays(d_data->p_data->packets_rx,d_data->number_robots);
						clearArrays(d_data->p_data->own_packets_rx,d_data->number_robots);
						clearArrays(d_data->p_data->wasted_energy,d_data->number_robots);



						for(i=0;i<d_data->number_robots;i++)
						{
							if (!d_data->robot_data[i].broken)
							{	//Solo TX los que no estan rotos

								//XXXX!!! Cuidado! msj_tx es doble pero ahora mismo se está utilizando como booleano
								// ARREGLAR EN CUANTO SE PUEDA!!!!!!!
								clearArrays(msj_tx,d_data->number_robots);

								if (d_data->robot_data[i].level!=1)
								{
									sendAdhoc(d_data, i, i, d_data->FinalAP, msj_tx, ONLY_IF_CLOSER);
								}
							}
						}


						clearArrays(remaining_cycles,d_data->number_robots); //Esto lo hacia antes en cada iteracion ???
						double min_remaining_cycles=ROBOT_INITIAL_BATTERY/ROBOT_WASTE_PER_PACKET; //Maximo valor posible

						//Una vez terminado, calculo cuanto queda para que muera el siguiente
						for(i=0;i<d_data->number_robots;i++)
						{
							//El AP no entra en la cuenta en esta red
							if ((d_data->robot_data[i].level==0)&&(!d_data->robot_data[i].broken))
							{
								remaining_cycles[i]=d_data->robot_data[i].remaining_battery/(d_data->p_data->wasted_energy[i]+ROBOT_DUTYCYCLE_WASTE/ROBOT_PACKET_RATE_HOUR);   //(d_data->p_data->packets_sent[i]*ROBOT_WASTE_PER_PACKET);
								if (min_remaining_cycles>remaining_cycles[i])
									min_remaining_cycles=remaining_cycles[i];
							}
						}

						if (min_remaining_cycles<0.0) min_remaining_cycles=0.0; //XXXX. Esto es por un nodo que ha caido por desplazamiento...

						printf("Pasan %f ciclos hasta que cae un robot \n",min_remaining_cycles);
						accum_cycles+=min_remaining_cycles;

						if (rotos==NULL) rotos=(int*) calloc(d_data->number_robots, sizeof(int)); //Por si no esta inicializado previamente

						d_data->num_rotos=0;
						//Ahora actualizo los valores de bateria
						for(i=0;i<d_data->number_robots;i++)
						{

							d_data->robot_data[i].remaining_battery=d_data->robot_data[i].remaining_battery-(d_data->p_data->wasted_energy[i]+ROBOT_DUTYCYCLE_WASTE/ROBOT_PACKET_RATE_HOUR)*min_remaining_cycles; //-d_data->p_data->packets_sent[i]*ROBOT_WASTE_PER_PACKET*min_remaining_cycles;
							if (d_data->robot_data[i].remaining_battery<=(0.05*ROBOT_INITIAL_BATTERY))
							{	//mato a la vez todos los que tengan menos de un 5% de bateria
								//Asi acelero un opco la simulacion
								//El AP no entra en la cuenta...
								if (d_data->robot_data[i].level==0)
								{
									printf("El robot %i ha pasado a mejor vida\n",i);
									rotos[i]=1;
									d_data->num_rotos++; //cuenta cuántos llevamos
								}
							}
						}
						printf("\n Fin determinacion rotos\n");

						//Aqui faltaria calcular las estadisticas y guardarlas en un fichero.
					}

					printf("\n Fin Simulacion\n");


				}
				else if(escenario==3)
				{


					//MAJOR_CHANGE
					clearArrays(distancia_recorrida_anterior,d_data->number_robots);

					//XXXX...Modificar aqui!!!
					// ---> simular ---> determinar quien muere---> matar ---> resimular ¿hasta cuando?
					//Guardar estadisticas en ficheros....

					fprintf(fichero,"SPJer %i nodos\n",d_data->number_robots);
					fprintf(fichero,"ciclos \t rotos \t rel_cove \t cov \t uniform \t  mean_dist \t alive \t mean_power \t mean_snd \t mean_rx \t st_dev_sent \t st_dev_rx \t min_sent \t max_sent \t meantx_noAP \t devtx_noAP min_sent_noAP \t max_sent_noAP \n ");

					int fotograma=0;


					//De momento simulo hasta que se hayan roto una fraccion. Ajustar mas adelante.
					while (d_data->num_rotos<(0.70*d_data->number_robots))
					{

						d_data=simAPJer(d_data,rotos,obstaculos,&sim_robots,sim_position2d,&simulation,sim_graphics2d,sim_sonar);

						printf("Iteracion finalizada!\n");

//						if ((d_data->nueva_simulacion)||((fotograma==1)&&(d_data->num_rotos>0.125*d_data->number_robots))||((fotograma==2)&&(d_data->num_rotos>0.25*d_data->number_robots))||((fotograma==3)&&(d_data->num_rotos>0.50*d_data->number_robots)))
//						{
//							printf("Porfa, saca la fotito....\n");
//							fotograma++;
//							//while (!kbhit());
//							makePause();
//						}


						d_data->nueva_simulacion=0;

						//Aqui faltaria calcular las estadisticas y guardarlas en un fichero.


						fprintf(fichero," %lf \t %i \t",accum_cycles,d_data->num_rotos); //XXXX. Guarda el tiempo y el numero de rotos
						save_performance(fichero,d_data); //XXXX. Calcula y guarda las estadisticas

						int i;
						for (i=0;i<d_data->number_robots; i++)
						{
							//El AP no entra en la cuenta en esta red
							if ((d_data->robot_data[i].level!=2)&&(!d_data->robot_data[i].broken))
							{
								d_data->robot_data[i].remaining_battery-=(d_data->p_data->distance_walked[i]-distancia_recorrida_anterior[i])*BATTERY_WASTE_PER_DISTANCE; //Cuidado con la escala!!
								distancia_recorrida_anterior[i]=d_data->p_data->distance_walked[i];
//								if (d_data->robot_data[i].remaining_battery)
//								{
//									rotos[i]=1; //Marca el robot comor roto. Este robot ya no genera ni enruta trafico....
//								}
								// XXXX. NUeva simulacion. Esto es complicadillo, porque requiere un cambio en la topologia
								//que aqui no se simula
								// por ello mejor lo que hago más adelante es forzar una segunda iteración si mueren nodos durante el movimento

							}
						}

						//Vamos a Calcular calcular cuantas transmisiones hace cada uno
						//Se podria simplificar a cuantos paquetes puede enviar un robot antes de morir,
						// pero bueno, por si mas adelante complicamos el modelo de momento voy calculando

						//Reseteamos las estadisticas...
						clearArrays(d_data->p_data->packets_sent,d_data->number_robots);
						clearArrays(d_data->p_data->own_packets_sent,d_data->number_robots);
						clearArrays(d_data->p_data->packets_rx,d_data->number_robots);
						clearArrays(d_data->p_data->own_packets_rx,d_data->number_robots);
						clearArrays(d_data->p_data->wasted_energy,d_data->number_robots);

						//int i;
						for(i=0;i<d_data->number_robots;i++)
						{
							if (!d_data->robot_data[i].broken)
							{	//Solo TX los que no estan rotos

								//XXXX!!! Cuidado! msj_tx es doble pero ahora mismo se está utilizando como booleano
								// ARREGLAR EN CUANTO SE PUEDA!!!!!!!
								clearArrays(msj_tx,d_data->number_robots);

								if (d_data->robot_data[i].level==1 || ((d_data->robot_data[i].level==0) && (d_data->robot_data[i].registered_to!=-1)))
								{

									sendAPJerq(d_data, i, i, d_data->FinalAP, msj_tx, 0);
								}
								else if (d_data->robot_data[i].level!=2)
								{
									sendAdhoc(d_data, i, i, -1, msj_tx, 0);
								}
							}

						}

						clearArrays(remaining_cycles,d_data->number_robots); //Esto lo hacia antes en cada iteracion ???
						double min_remaining_cycles=ROBOT_INITIAL_BATTERY/ROBOT_WASTE_PER_PACKET; //Maximo valor posible



						//Una vez terminado, calculo cuanto queda para que muera el siguiente
						for(i=0;i<d_data->number_robots;i++)
						{
							//El AP no entra en la cuenta en esta red
							if ((d_data->robot_data[i].level!=2)&&(!d_data->robot_data[i].broken))
							{
								remaining_cycles[i]=d_data->robot_data[i].remaining_battery/(d_data->p_data->wasted_energy[i]+ROBOT_DUTYCYCLE_WASTE/ROBOT_PACKET_RATE_HOUR);   //(d_data->p_data->packets_sent[i]*ROBOT_WASTE_PER_PACKET);
								//remaining_cycles[i]=d_data->robot_data[i].remaining_battery/(d_data->p_data->packets_sent[i]*ROBOT_WASTE_PER_PACKET);
								if (min_remaining_cycles>remaining_cycles[i])
									min_remaining_cycles=remaining_cycles[i];
							}
						}

						if (min_remaining_cycles<0.0) min_remaining_cycles=0.0; //XXXX. Esto es por un nodo que ha caido por desplazamiento...

						printf("Pasan %f ciclos hasta que cae un robot \n",min_remaining_cycles);
						accum_cycles+=min_remaining_cycles;

						if (rotos==NULL) rotos=(int*) calloc(d_data->number_robots, sizeof(int)); //Por si no esta inicializado previamente

						d_data->num_rotos=0;
						//Ahora actualizo los valores de bateria
						for(i=0;i<d_data->number_robots;i++)
						{

							//d_data->robot_data[i].remaining_battery=d_data->robot_data[i].remaining_battery-d_data->p_data->packets_sent[i]*ROBOT_WASTE_PER_PACKET*min_remaining_cycles;
							d_data->robot_data[i].remaining_battery=d_data->robot_data[i].remaining_battery-(d_data->p_data->wasted_energy[i]+ROBOT_DUTYCYCLE_WASTE/ROBOT_PACKET_RATE_HOUR)*min_remaining_cycles;

							if (d_data->robot_data[i].remaining_battery<=(0.05*ROBOT_INITIAL_BATTERY))
							{	//mato a la vez todos los que tengan menos de un 5% de bateria
								//Asi acelero un opco la simulacion
								//El AP no entra en la cuenta...
								if (d_data->robot_data[i].level!=2)
								{
									printf("El robot %i ha pasado a mejor vida\n",i);
									rotos[i]=1;
									d_data->num_rotos++; //cuenta cuántos llevamos
								}
							}
						}
						printf("\n Fin determinacion rotos\n");



					}

					printf("\n Fin Simulacion\n");

				}
				else if(escenario==4)
				{
					//JERARQUICO MODIFICADO!!!!
					//XXXX...Modificar aqui!!!
					// ---> simular ---> determinar quien muere---> matar ---> resimular ¿hasta cuando?
					//Guardar estadisticas en ficheros....

					fprintf(fichero,"SPJerRoleSwitch %i nodos\n",d_data->number_robots);
					fprintf(fichero,"ciclos \t rotos \t rel_cove \t cov \t uniform \t  mean_dist \t alive \t mean_power \t mean_snd \t mean_rx \t st_dev_sent \t st_dev_rx \t min_sent \t max_sent \t meantx_noAP \t devtx_noAP min_sent_noAP \t max_sent_noAP \n ");
					int fotograma=0;

					//De momento simulo hasta que se hayan roto una fraccion. Ajustar mas adelante.
					while (d_data->num_rotos<(0.70*d_data->number_robots))
					{

						d_data=simAPJer(d_data,rotos,obstaculos,&sim_robots,sim_position2d,&simulation,sim_graphics2d,sim_sonar);

						printf("Iteracion finalizada!\n");

//						if ((d_data->nueva_simulacion)||((fotograma==1)&&(d_data->num_rotos>0.125*d_data->number_robots))||((fotograma==2)&&(d_data->num_rotos>0.25*d_data->number_robots))||((fotograma==3)&&(d_data->num_rotos>0.50*d_data->number_robots)))
//						{
//							printf("Porfa, saca la fotito....\n");
//							fotograma++;
//							//while (!kbhit());
//							makePause();
//						}


						d_data->nueva_simulacion=0;

						//Aqui faltaria calcular las estadisticas y guardarlas en un fichero.


						fprintf(fichero," %lf \t %i \t",accum_cycles,d_data->num_rotos); //XXXX. Guarda el tiempo y el numero de rotos
						save_performance(fichero,d_data); //XXXX. Calcula y guarda las estadisticas


						//Vamos a Calcular calcular cuantas transmisiones hace cada uno
						//Se podria simplificar a cuantos paquetes puede enviar un robot antes de morir,
						// pero bueno, por si mas adelante complicamos el modelo de momento voy calculando

						//Reseteamos las estadisticas...
						clearArrays(d_data->p_data->packets_sent,d_data->number_robots);
						clearArrays(d_data->p_data->own_packets_sent,d_data->number_robots);
						clearArrays(d_data->p_data->packets_rx,d_data->number_robots);
						clearArrays(d_data->p_data->own_packets_rx,d_data->number_robots);
						clearArrays(d_data->p_data->wasted_energy,d_data->number_robots);

						int i;
						double carga_media_red=0.0;
						for(i=0;i<d_data->number_robots;i++)
						{
							if (!d_data->robot_data[i].broken)
							{	//Solo TX los que no estan rotos

								//XXXX!!! Cuidado! msj_tx es doble pero ahora mismo se está utilizando como booleano
								// ARREGLAR EN CUANTO SE PUEDA!!!!!!!
								clearArrays(msj_tx,d_data->number_robots);

								if (d_data->robot_data[i].level==1 || ((d_data->robot_data[i].level==0) && (d_data->robot_data[i].registered_to!=-1)))
								{

									sendAPJerq(d_data, i, i, d_data->FinalAP, msj_tx, 0);
								}
								else if (d_data->robot_data[i].level!=2)
								{
									sendAdhoc(d_data, i, i, -1, msj_tx, 0);
								}


								//Aprovecho para calcular la carga
								if (d_data->robot_data[i].level!=2)
								{
									carga_media_red+=d_data->robot_data[i].remaining_battery;
								}

							}

						}


						carga_media_red=carga_media_red/(d_data->number_robots-d_data->num_rotos-1);//El AP no cuenta, por eso resto 1.

						clearArrays(remaining_cycles,d_data->number_robots); //Esto lo hacia antes en cada iteracion ???
						double min_remaining_cycles=ROBOT_INITIAL_BATTERY/ROBOT_WASTE_PER_PACKET; //Maximo valor posible




						//Una vez terminado, calculo cuanto queda para que muera el siguiente o haya un cambio de estado...
						for(i=0;i<d_data->number_robots;i++)
						{
							//El AP no entra en la cuenta en esta red
							if ((d_data->robot_data[i].level!=2)&&((d_data->robot_data[i].low_battery))&&(!d_data->robot_data[i].broken))
							{ //XXXX. Escenario 4. Nodos con nivel de bateria bajo!!
								remaining_cycles[i]=d_data->robot_data[i].remaining_battery/(d_data->p_data->wasted_energy[i]+ROBOT_DUTYCYCLE_WASTE/ROBOT_PACKET_RATE_HOUR);   //(d_data->p_data->packets_sent[i]*ROBOT_WASTE_PER_PACKET);
								//remaining_cycles[i]=d_data->robot_data[i].remaining_battery/(d_data->p_data->packets_sent[i]*ROBOT_WASTE_PER_PACKET);
								if (min_remaining_cycles>remaining_cycles[i])
									min_remaining_cycles=remaining_cycles[i];
							}
							else if ((d_data->robot_data[i].level!=2)&&(!(d_data->robot_data[i].low_battery))&&(!d_data->robot_data[i].broken))
							{
								//XXXX Nodos con nivel de bateria aun alto...
								//remaining_cycles[i]=(d_data->robot_data[i].remaining_battery-0.15*ROBOT_INITIAL_BATTERY)/(d_data->p_data->packets_sent[i]*ROBOT_WASTE_PER_PACKET);
								//remaining_cycles[i]=(d_data->robot_data[i].remaining_battery-0.15*carga_media_red)/(d_data->p_data->packets_sent[i]*ROBOT_WASTE_PER_PACKET);

								remaining_cycles[i]=(d_data->robot_data[i].remaining_battery-0.15*carga_media_red)/(d_data->p_data->wasted_energy[i]+ROBOT_DUTYCYCLE_WASTE/ROBOT_PACKET_RATE_HOUR);   //(d_data->p_data->packets_sent[i]*ROBOT_WASTE_PER_PACKET);
								if ((min_remaining_cycles>remaining_cycles[i])&&(remaining_cycles[i]>0.0))
									min_remaining_cycles=remaining_cycles[i]; //XXXX Lo de comprobar si es mayor que 0 es por seguridad. No deberia de suceder....
							}

						}

						printf("Pasan %f ciclos hasta que cae un robot \n",min_remaining_cycles);
						accum_cycles+=min_remaining_cycles;

						if (rotos==NULL) rotos=(int*) calloc(d_data->number_robots, sizeof(int)); //Por si no esta inicializado previamente

						d_data->num_rotos=0;
						//Ahora actualizo los valores de bateria
						for(i=0;i<d_data->number_robots;i++)
						{

							//d_data->robot_data[i].remaining_battery=d_data->robot_data[i].remaining_battery-d_data->p_data->packets_sent[i]*ROBOT_WASTE_PER_PACKET*min_remaining_cycles;
							d_data->robot_data[i].remaining_battery=d_data->robot_data[i].remaining_battery-(d_data->p_data->wasted_energy[i]+ROBOT_DUTYCYCLE_WASTE/ROBOT_PACKET_RATE_HOUR)*min_remaining_cycles;
							if (d_data->robot_data[i].remaining_battery<=(0.05*ROBOT_INITIAL_BATTERY))
							{	//mato a la vez todos los que tengan menos de un 5% de bateria
								//Asi acelero un opco la simulacion
								//El AP no entra en la cuenta...
								if (d_data->robot_data[i].level!=2)
								{
									printf("El robot %i ha pasado a mejor vida\n",i);
									rotos[i]=1;
									d_data->num_rotos++; //cuenta cuántos llevamos
								}
							}

							if (d_data->robot_data[i].remaining_battery<=(0.2*carga_media_red )) /*ROBOT_INITIAL_BATTERY))*/
							{	//Paso a bajo consumo a la vez a todos los que tengan entre 0.15 y 0.2 de bateria
									//Asi acelero un opco la simulacion
									//El AP no entra en la cuenta...
								if (d_data->robot_data[i].level!=2)
								{
									printf("El robot %i entra en alarma... \n",i);
									d_data->robot_data[i].low_battery=1; //
									//rotos[i]=1;
									//d_data->num_rotos++; //cuenta cuántos llevamos
								}

							}
							else if (d_data->robot_data[i].remaining_battery>=(0.4*carga_media_red)) /* Aplico cierta histeresis */
							{ //Los robots que superen el 40% del valor medio de  bateria vuelven al tajo....
								d_data->robot_data[i].low_battery=0; //
								printf("El robot %i arrima el hombro \n",i);
							}


						}
						printf("\n Fin determinacion rotos\n");



					}

					printf("\n Fin Simulacion\n");

				}
				else if(escenario==5) // Skenari im.
				{
					int fotograma=0;


					//MAJOR_CHANGE - Why?
					clearArrays(distancia_recorrida_anterior,d_data->number_robots);

					fprintf(fichero,"SPF %i nodos\n",d_data->number_robots);
					fprintf(fichero,"ciclos \t rotos \t rel_cove \t cov \t uniform \t  mean_dist \t alive \t mean_power \t mean_snd \t mean_rx \t st_dev_sent \t st_dev_rx \t min_sent \t max_sent \t meantx_noAP \t devtx_noAP min_sent_noAP \t max_sent_noAP \n ");

					//XXXX. De momento simulo hasta que se hayan roto una fraccion. Ajustar mas adelante.
					//init tasks_data
					tasks_data tt;
					tasks_data* t_data=&tt;

					int br = 0;
					int packets_per_agent[d_data->number_robots];
					//Count how many broken robots at startup
					if(!rotos==NULL)
						for(int i=0; i< d_data->number_robots; i++)
						{
							printf("\nbroken %d", rotos[i]);
							if(rotos[i] == 1) br++;
						}

					// INFO: create as many tasks as active robots
					// INFO: the t_data data structure contains the ids and locations (xpos, ypos) of each task (location to be followed)
					t_data->num_tasks = d_data->number_robots-br;
					t_data->task_ids = (int*) malloc(sizeof(int) * t_data->num_tasks);
					t_data->xpos = (float*) malloc(sizeof(float) * t_data->num_tasks);
					t_data->ypos = (float*) malloc(sizeof(float) * t_data->num_tasks);

					//init assigned_robots
					assigned_robots tutu;
					assigned_robots* ar_data=&tutu;
					// INFO: The ar_data data structure contains the assignment of agents to tasks (locations)
					// INFO: each field has a length of num_tasks*number_robots
					// INFO: from index i 0 to num_tasks-1 we have the assignment information for the first robot (in ar_data->tasks_assigned). If the element is 1 then robot 0 is assigned to task i, otherwise not
					// INFO: The assignment for the second robot (index 1) is within 1*num_tasks to num_tasks+num_tasks - 1.
					// INFO: for robot with index j, the assignment data is found at ar_data->tasks_assigned[j*num_tasks] til ar_data->tasks_assigned[j*num_tasks + num_tasks-1]
					ar_data->robots = (int*) malloc(sizeof(int) * t_data->num_tasks * d_data->number_robots);
					ar_data->tasks_assigned = (int*) malloc(sizeof(int) * t_data->num_tasks * d_data->number_robots);
					ar_data->xpos = (float*) malloc(sizeof(float) * t_data->num_tasks * d_data->number_robots);
					ar_data->ypos = (float*) malloc(sizeof(float) * t_data->num_tasks * d_data->number_robots);

					// INFO: this data structure contains the help requests sent by one robot, as well as the responses given by every robot
					help_protocol_data hpd;
					help_protocol_data* hpd_data=&hpd;

					hpd_data->previous_step_battery = (double*) malloc(sizeof(double) * d_data->number_robots);
					for(int i=0; i< d_data->number_robots; i++)
					{
						hpd_data->previous_step_battery[i] = d_data->robot_data[i].remaining_battery;
						packets_per_agent[i] = 0;
					}
					// INFO: if a robot i requests help then hpd_data->requested_help[i] is set to 1, otherwise it is 0
					hpd_data->requested_help = (int*) malloc(sizeof(int) * d_data->number_robots);
					hpd_data->started_negotiation = (int*) malloc(sizeof(int) * d_data->number_robots);
					// INFO: if a robot i needs to give an answer (willingness + utility) to robot j it will set hpd_data->giving_help[j*number_robots + i]
					hpd_data->giving_help = (float*) malloc(sizeof(float) * d_data->number_robots * d_data->number_robots);
					hpd_data->msgs_exchanged= (int*) malloc(sizeof(int) * d_data->number_robots);
					for(int i=0; i< d_data->number_robots; i++)
					{
						hpd_data->msgs_exchanged[i] = 0;
					}
					// INFO: this variable denotes whether a round of spf should take place. a round of spf takes place after a negotiation is detected as concluded
					hpd_data->round_of_SPF = 0;
					hpd_data->unfinished_negotiations = 0;
					hpd_data->total_negotiations = 0;
					hpd_data->total_negotiations_AP = 0;
					hpd_data->unfinished_negotiations_AP = 0;
					// INFO: the delegation strategy refers to how an agent deals when it accepts to help other. either by dropping its own location and going to the new one, or by going to
					// INFO: a middle point
					printf("Delegation strategy: %d\n", AA_alg_delegation_strategy);
					while (d_data->num_rotos<(0.70*d_data->number_robots))
					{
						printf("Iteration started!\n");
						d_data=simAAagents2(d_data,t_data, ar_data, hpd_data,rotos,obstaculos,&sim_robots,sim_position2d,&simulation,sim_graphics2d,sim_sonar, AA_alg_delegation_strategy, packets_per_agent);
						printf("Iteracion finalizada!\n");

//						if ((d_data->nueva_simulacion)||((fotograma==1)&&(d_data->num_rotos>0.125*d_data->number_robots))||((fotograma==2)&&(d_data->num_rotos>0.25*d_data->number_robots))||((fotograma==3)&&(d_data->num_rotos>0.50*d_data->number_robots)))
//						{
//							printf("Porfa, saca la fotito....\n");
//							fotograma++;
//							//while (!kbhit());
//							makePause();
//						}

						d_data->nueva_simulacion=0;

						fprintf(fichero," %lf \t %i \t",accum_cycles,d_data->num_rotos); //XXXX. Guarda el tiempo y el numero de rotos
						save_performance(fichero,d_data); //XXXX. Calcula y guarda las estadisticas

						if (rotos==NULL) rotos=(int*) calloc(d_data->number_robots, sizeof(int)); //Por si no esta inicializado previamente

						int i;
						for (i=0;i<d_data->number_robots; i++)
						{
							//El AP no entra en la cuenta en esta red
							if ((d_data->robot_data[i].level==0)&&(!d_data->robot_data[i].broken))
							{
								d_data->robot_data[i].remaining_battery-=(d_data->p_data->distance_walked[i]-distancia_recorrida_anterior[i])*BATTERY_WASTE_PER_DISTANCE; //Cuidado con la escala!!
								distancia_recorrida_anterior[i]=d_data->p_data->distance_walked[i];
//								if (d_data->robot_data[i].remaining_battery)
//								{
//									rotos[i]=1; //Marca el robot comor roto. Este robot ya no genera ni enruta trafico....
//								}
								// XXXX. NUeva simulacion. Esto es complicadillo, porque requiere un cambio en la topologia
								//que aqui no se simula
								// por ello mejor lo que hago más adelante es forzar una segunda iteración si mueren nodos durante el movimento

							}
						}

						//Vamos a Calcular calcular cuantas transmisiones hace cada uno
						//Se podria simplificar a cuantos paquetes puede enviar un robot antes de morir,
						// pero bueno, por si mas adelante complicamos el modelo de momento voy calculando

						//Reseteamos las estadisticas...
						clearArrays(d_data->p_data->packets_sent,d_data->number_robots);
						clearArrays(d_data->p_data->own_packets_sent,d_data->number_robots);
						clearArrays(d_data->p_data->packets_rx,d_data->number_robots);
						clearArrays(d_data->p_data->own_packets_rx,d_data->number_robots);
						clearArrays(d_data->p_data->wasted_energy,d_data->number_robots);



						for(i=0;i<d_data->number_robots;i++)
						{
							if (!d_data->robot_data[i].broken)
							{	//Solo TX los que no estan rotos

								//XXXX!!! Cuidado! msj_tx es doble pero ahora mismo se está utilizando como booleano
								// ARREGLAR EN CUANTO SE PUEDA!!!!!!!
								clearArrays(msj_tx,d_data->number_robots);

								if (d_data->robot_data[i].level!=1)
								{
									sendAdhoc(d_data, i, i, d_data->FinalAP, msj_tx, ONLY_IF_CLOSER);
								}
							}
						}

						clearArrays(remaining_cycles,d_data->number_robots); //Esto lo hacia antes en cada iteracion ???
						double min_remaining_cycles=ROBOT_INITIAL_BATTERY/ROBOT_WASTE_PER_PACKET; //Maximo valor posible

						//Una vez terminado, calculo cuanto queda para que muera el siguiente
						for(i=0;i<d_data->number_robots;i++)
						{
							//El AP no entra en la cuenta en esta red
							if ((d_data->robot_data[i].level==0)&&(!d_data->robot_data[i].broken))
							{
								remaining_cycles[i]=d_data->robot_data[i].remaining_battery/(d_data->p_data->wasted_energy[i]+ROBOT_DUTYCYCLE_WASTE/ROBOT_PACKET_RATE_HOUR);   //(d_data->p_data->packets_sent[i]*ROBOT_WASTE_PER_PACKET);
								if (min_remaining_cycles>remaining_cycles[i])
									min_remaining_cycles=remaining_cycles[i];
							}
						}

						if (min_remaining_cycles<0.0) min_remaining_cycles=0.0; //XXXX. Esto es por un nodo que ha caido por desplazamiento...

						printf("Pasan %f ciclos hasta que cae un robot \n",min_remaining_cycles);
						accum_cycles+=min_remaining_cycles;

						if (rotos==NULL) rotos=(int*) calloc(d_data->number_robots, sizeof(int)); //Por si no esta inicializado previamente

						d_data->num_rotos=0;
						//Ahora actualizo los valores de bateria
						for(i=0;i<d_data->number_robots;i++)
						{
							hpd_data->previous_step_battery[i] = d_data->robot_data[i].remaining_battery;
							d_data->robot_data[i].remaining_battery=d_data->robot_data[i].remaining_battery-(d_data->p_data->wasted_energy[i]+ROBOT_DUTYCYCLE_WASTE/ROBOT_PACKET_RATE_HOUR)*min_remaining_cycles; //-d_data->p_data->packets_sent[i]*ROBOT_WASTE_PER_PACKET*min_remaining_cycles;
							if (d_data->robot_data[i].remaining_battery<=(0.05*ROBOT_INITIAL_BATTERY))
							{	//mato a la vez todos los que tengan menos de un 5% de bateria
								//Asi acelero un opco la simulacion
								//El AP no entra en la cuenta...
								if (d_data->robot_data[i].level==0)
								{
									printf("El robot %i ha pasado a mejor vida\n",i);
									rotos[i]=1;
									d_data->num_rotos++; //cuenta cuántos llevamos
								}
							}
						}
						printf("\n Fin determinacion rotos\n");

						//Aqui faltaria calcular las estadisticas y guardarlas en un fichero.
					}

					printf("\n Fin Simulacion\n");
					printf("\n Total negotiations started: %d", hpd_data->total_negotiations);
					printf("\n Total negotiations started by AP: %d", hpd_data->total_negotiations_AP);
					printf("\n Total negotiations interrupted: %d", hpd_data->unfinished_negotiations);
					free(ar_data->robots);
					free(ar_data->tasks_assigned);
					free(ar_data->xpos);
					free(ar_data->ypos);
					free(t_data->task_ids);
					free(t_data->xpos);
					free(t_data->ypos);
					free(hpd_data->giving_help);
					free(hpd_data->msgs_exchanged);
					free(hpd_data->previous_step_battery);
					free(hpd_data->requested_help);

				}
				//Libero rotos
				if (rotos!=NULL)
				{
					free(rotos);
					rotos=NULL;
				}

				free(msj_tx);
				free(remaining_cycles);
			}
		}
	}
	system("reset");


	fclose(fichero);

	return 1;
} 
