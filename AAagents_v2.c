/*
 * AAagents_v2.c
 *
 *  Created on: Sep 12, 2019
 *      Author: debian
 */


/*---------------------------------------------------------------------------
 *  Proyecto Fin de Carrera: Enjambre de microbots para despliegue autónomo de
 *                           una red inalámbrica mallada.
 *
 *  Francisco José Aguilera Leal
 *  Universidad de Málaga
 * ---------------------------------------------------------------------------
 * Despliegue de red jerarquizada con Access Point.
 * Uso de los campos de potencial sociales.
 * ---------------------------------------------------------------------------
*/

#include "adhocSPF.h"
#include "APjer.h" //Para aprovechar la funcion ChooseAP

#include "config.h"


/* ---------------------------------------------------------------------------
 * changeBroken - Modifica el estado de un robot (roto o funcionando).
 * --------------------------------------------------------------------------- */
static void changeBroken(playerc_position2d_t** sim_position2d, robot_data_t* robot_data, int my_robot, int number_robots)
{
	int i;
	if(my_robot>=0 && my_robot < number_robots)
	{
		if(robot_data[my_robot].broken==0)
		{
			robot_data[my_robot].broken=1;
			robot_data[my_robot].stopped=1;
		}
		else
		{
			robot_data[my_robot].broken=0;
			robot_data[my_robot].stopped=0;
		}
	}
}

static void BreakSPF(playerc_position2d_t** sim_position2d, robot_data_t* robot_data, int my_robot, int number_robots,int *AP)
{
	int i;
	if(my_robot>=0 && my_robot < number_robots)
	{
		if(robot_data[my_robot].broken==0)
		{
			robot_data[my_robot].broken=1;
			robot_data[my_robot].stopped=1;
		}
	}

	if (*AP==my_robot)
	{
		*AP=chooseAP(sim_position2d, robot_data,number_robots,0,-1);
	}
//		else
//		{
//			robot_data[my_robot].broken=0;
//			robot_data[my_robot].stopped=0;
//		}
//	}
}

/* ---------------------------------------------------------------------------
 * potentialLaw - Calcula la fuerza
 * --------------------------------------------------------------------------- */
static double potentialLaw(double distance, double c1, double c2, double sigma1, double sigma2, double xoff, double yoff)
{
	double f = -c1/pow((distance+xoff),sigma1) + c2/pow((distance+xoff),sigma2) + yoff;
	return f;
}

/* ---------------------------------------------------------------------------
 * vectorTotal - Calcula el vector de fuerza ejercido por la red sobre
 *		 el robot
   --------------------------------------------------------------------------- */
static void vectorTotal(playerc_position2d_t** sim_position2d, robot_data_t* robot_data, int number_robots, int num_rotos, int map_size, int my_robot, double* x, double* y,int obstaculos)
{
	double f,d,c1,c2,sigma1,sigma2,xoff,yoff;
	int j,no_moverse=0;
	int i=0;
	int cercanos=0;

	*x=0;
	*y=0;

//	if (i==0)
//	{
//		printf("Help me");
//	}


	// Dibuja lineas hasta los robots en rango
	for(i=0;i<number_robots;i++)
	{
		double distance=sqrt(pow(sim_position2d[my_robot]->px-sim_position2d[i]->px,2)+pow(sim_position2d[my_robot]->py-sim_position2d[i]->py,2));
		if(i!=my_robot && distance<=RADIO_COVERAGE_SPF && !robot_data[i].broken)
		{
			cercanos++;
		}
	}


	//1) Esta roto?
	if((robot_data[my_robot].broken==1)||(robot_data[my_robot].level!=0))
	{
		// De momento estoy quieto. (Luego lo movere hacia abajo)
		no_moverse=1;

		//XXXX. Añadida condicion de forma que si es el AP, tampoco se mueve

	}

	else
	{
		for(j=0;j<number_robots;j++)
		{
			// Se aleja 2 m de los que no estan rotos
			if (my_robot!=j)
			{
				d=calculateDistance(sim_position2d, j, my_robot);

				if( robot_data[j].broken==0)
				{
					f=0;


					//Fuerza de repulsion hasta 1.5 m
					c1=20;
					c2=0;
					sigma1=7;
					sigma2=1; //Anulado
					xoff=0;
					yoff=0;
					f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
				}
				// Intenta no chocarse con los rotos
				else
				{
					// Repulsion hasta 0.4 m
					c1=0.001;
					c2=0;
					sigma1=8;
					sigma2=1;// Da igual este termino se anula
					xoff=0;
					yoff=0;
					f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);

				}

				*x=*x+f*(sim_position2d[j]->px - sim_position2d[my_robot]->px)/d;
				*y=*y+f*(sim_position2d[j]->py - sim_position2d[my_robot]->py)/d; //XXXX. Se divide por d???

				//Fuerza adicional de cohesion de la red
				if (robot_data[j].level>0)
				{
					f=20*((double)num_rotos/(double)number_robots)/((double)(cercanos+1));
					*x=*x+f*(sim_position2d[j]->px - sim_position2d[my_robot]->px);///d;
					*y=*y+f*(sim_position2d[j]->py - sim_position2d[my_robot]->py);///d; //XXXX. Se divide por d???
				}



			}


			//XXXX, PEDAZO DE ERROR QUE HABÏA AQUI. AL ESTAR ESTO FUERA del for
			//El nodo 0 daba NAN (divide por 0 la primera vez)
			//Para el resto, se alteraba el vector de fuerza
//			*x=*x+f*(sim_position2d[j]->px - sim_position2d[i]->px)/d;
//			*y=*y+f*(sim_position2d[j]->py - sim_position2d[i]->py)/d;
		}

	}

	// Repulsion de los muros laterales hasta 0.4 m.
	c1=0.001;
	c2=0;
	sigma1=8;
	sigma2=1; // Da igual este termino se anula
	xoff=0;
	yoff=0;

	// Muro norte:
	d=fabs(map_size/2-sim_position2d[my_robot]->py);
	f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
	*y=*y+f*(map_size/2 - sim_position2d[my_robot]->py)/d;

	// Muro sur:
	d=fabs(-map_size/2-sim_position2d[my_robot]->py);
	f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
	*y=*y+f*(-map_size/2-sim_position2d[my_robot]->py)/d;

	// Muro oeste:
	d=fabs(-map_size/2-sim_position2d[my_robot]->px);
	f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
	*x=*x+f*(-map_size/2-sim_position2d[my_robot]->px)/d;

	// Muro este:
	d=fabs(map_size/2-sim_position2d[my_robot]->px);
	f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
	*x=*x+f*(map_size/2 - sim_position2d[my_robot]->px)/d;

	// Escenario de obstaculos 1
	if(obstaculos==1)
	{
		// Repulsion muros interiores hasta 0.4 m
		c1=0.001; //XXXX. Aumentado (era 0.001) a ver si los obstaculos repelen un poco mas
		c2=0;
		sigma1=12;	//XXXX ¿12?¿Diferente a la otra (muertos y muros fronterizos)?
		sigma2=1;// Da igual este termino se anula
		xoff=0;
		yoff=0;

		// Muro superior
		if(sim_position2d[my_robot]->px>-0.5*map_size/5 && sim_position2d[my_robot]->px<0.5*map_size/5)
		{
			d=fabs(map_size/5-sim_position2d[my_robot]->py);
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*y=*y+f*(map_size/5-sim_position2d[my_robot]->py)/d;
		}
		else if(sim_position2d[my_robot]->px<-0.5*map_size/5)
		{
			d=sqrt(pow(-0.5*map_size/5-sim_position2d[my_robot]->px,2)+pow(map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(-0.5*map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(map_size/5-sim_position2d[my_robot]->py)/d;
		}
		else if(sim_position2d[my_robot]->px>0.5*map_size/5)
		{
			d=sqrt(pow(0.5*map_size/5-sim_position2d[my_robot]->px,2)+pow(map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(0.5*map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(map_size/5-sim_position2d[my_robot]->py)/d;
		}

		// Muro central
		if(sim_position2d[my_robot]->py<0 && sim_position2d[my_robot]->py>-0.5*map_size/5)
		{
			d=fabs(0-sim_position2d[my_robot]->px);
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(0-sim_position2d[my_robot]->px)/d;
		}
		else if(sim_position2d[my_robot]->py<-0.5*map_size/5)
		{
			d=sqrt(pow(0-sim_position2d[my_robot]->px,2)+pow(-0.5*map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(0-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(-map_size/5-sim_position2d[my_robot]->py)/d;
		}
		else if(sim_position2d[my_robot]->py>0)
		{
			d=sqrt(pow(0-sim_position2d[my_robot]->px,2)+pow(0-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(0-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(0-sim_position2d[my_robot]->py)/d;
		}

		// Muro derecho
		if(sim_position2d[my_robot]->py<-0.5*map_size/5 && sim_position2d[my_robot]->py>-1.5*map_size/5)
		{
			d=fabs(1.5*map_size/5-sim_position2d[my_robot]->px);
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(1.5*map_size/5-sim_position2d[my_robot]->px)/d; //XXXX. Aqui ponia 1-5 en lugar de 1.5
		}
		else if(sim_position2d[my_robot]->py>-0.5*map_size/5)
		{
			d=sqrt(pow(1.5*map_size/5-sim_position2d[my_robot]->px,2)+pow(-0.5*map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(1.5*map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(-0.5*map_size/5-sim_position2d[my_robot]->py)/d;
		}
		else if(sim_position2d[my_robot]->py<-1.5*map_size/5)
		{
			d=sqrt(pow(1.5*map_size/5-sim_position2d[my_robot]->px,2)+pow(-1.5*map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(1.5*map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(-1.5*map_size/5-sim_position2d[my_robot]->py)/d;
		}

		// Muro izquierdo
		if(sim_position2d[my_robot]->px>-1.5*map_size/5 && sim_position2d[my_robot]->px<-0.5*map_size/5)
		{
			d=fabs(-1.5*map_size/5-sim_position2d[my_robot]->py);
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*y=*y+f*(-1.5*map_size/5-sim_position2d[my_robot]->py)/d; //XXXX ¿modificar de 1.5 a 1.57?
		}
		else if(sim_position2d[my_robot]->px<-1.5*map_size/5)
		{
			d=sqrt(pow(-1.5*map_size/5-sim_position2d[my_robot]->px,2)+pow(-1.5*map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(-1.5*map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(-1.5*map_size/5-sim_position2d[my_robot]->py)/d;
		}
		else if(sim_position2d[my_robot]->px>-0.5*map_size/5)
		{
			d=sqrt(pow(-0.5*map_size/5-sim_position2d[my_robot]->px,2)+pow(-1.5*map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(-0.5*map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(-1.5*map_size/5-sim_position2d[my_robot]->py)/d;
		}
	}

	// Escenario de obstaculos 2
	if(obstaculos==2)
	{
		// Repulsion muros interiores hasta 0.4 m
		c1=0.001;
		c2=0;
		sigma1=12;		//XXXX. ¿¿¿Por qué diferente al de los otros???
		sigma2=1;// Da igual este termino se anula
		xoff=0;
		yoff=0;

		// Muro superior izquierdo
		if(sim_position2d[my_robot]->py<1.5*map_size/5 && sim_position2d[my_robot]->py>map_size/5)
		{
			d=fabs(-map_size/5-sim_position2d[my_robot]->px);
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(-map_size/5-sim_position2d[my_robot]->px)/d;
		}
		else if(sim_position2d[my_robot]->py>1.5*map_size/5)
		{
			d=sqrt(pow(-map_size/5-sim_position2d[my_robot]->px,2)+pow(1.5*map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(-map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(1.5*map_size/5-sim_position2d[my_robot]->py)/d;
		}
		else if(sim_position2d[my_robot]->py<map_size/5)
		{
			d=sqrt(pow(-map_size/5-sim_position2d[my_robot]->px,2)+pow(map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(-map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(map_size/5-sim_position2d[my_robot]->py)/d;
		}

		// Muro derecho
		if(sim_position2d[my_robot]->px>0 && sim_position2d[my_robot]->px<map_size/5)
		{
			d=fabs(0-sim_position2d[my_robot]->py);
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*y=*y+f*(0-sim_position2d[my_robot]->py)/d;
		}
		else if(sim_position2d[my_robot]->px<0)
		{
			d=sqrt(pow(0-sim_position2d[my_robot]->px,2)+pow(0-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(0-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(0-sim_position2d[my_robot]->py)/d;
		}
		else if(sim_position2d[my_robot]->px>map_size/5)
		{
			d=sqrt(pow(map_size/5-sim_position2d[my_robot]->px,2)+pow(0-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(0-sim_position2d[my_robot]->py)/d;
		}

		// Muro inferior izquierdo
		if(sim_position2d[my_robot]->py>-1.5*map_size/5 && sim_position2d[my_robot]->py<-map_size/5)
		{
			d=fabs(-map_size/5-sim_position2d[my_robot]->px);
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(-map_size/5-sim_position2d[my_robot]->px)/d;
		}
		else if(sim_position2d[my_robot]->py<-1.5*map_size/5)
		{
			d=sqrt(pow(-map_size/5-sim_position2d[my_robot]->px,2)+pow(-1.5*map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(-map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(-1.5*map_size/5-sim_position2d[my_robot]->py)/d;
		}
		else if(sim_position2d[my_robot]->py>-map_size/5)
		{
			d=sqrt(pow(-map_size/5-sim_position2d[my_robot]->px,2)+pow(-map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(-map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(-map_size/5-sim_position2d[my_robot]->py)/d;
		}
	}

	if(no_moverse)
	{
		*x=0;
		*y=0;
	}
}

/* ---------------------------------------------------------------------------
 * moverse - Controla el movimiento de los robots
 * --------------------------------------------------------------------------- */
static void moverse(playerc_position2d_t** sim_position2d, robot_data_t* robot_data, int my_robot, int number_robots, int num_rotos, int map_size,int obstaculos)
{
	double x;
	double y;
	double modulo;
	double angulo;
	double my_angle=sim_position2d[my_robot]->pa;


	//Calcula el vector resultante de todas las fuerzas
	vectorTotal(sim_position2d,robot_data,number_robots,num_rotos,map_size,my_robot,&x,&y,obstaculos);

	//Solo si el modulo de ese vector es mayor que un cierto valor, muevo al robot
	modulo=sqrt(pow(x,2)+pow(y,2));

	//if (my_robot==0) printf("vect %f %f ",x,y); XXXX DEPURACION

	if (modulo<UMBRAL_MOVIMIENTO)
	{
		playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,0,0);
		playerc_position2d_set_cmd_car(sim_position2d[my_robot],0,0);

		//XXXX: Faltaba aqui????...
		robot_data[my_robot].stopped=1;

	}
	else
	{

		robot_data[my_robot].stopped=0; //XXXX. Faltaba aqui!!!

		//Calcula el ángulo
		angulo = atan2(y/modulo,x/modulo); //Devuelve un valor entre -pi/2 y pi/2

		if(fabs(my_angle-angulo) > 0.1)
		{
			// Modo giro
			playerc_position2d_set_cmd_car(sim_position2d[my_robot],0,0);

			if(angulo>0)
			{
				if(my_angle<0)
				{
					if (my_angle+3.1415<angulo)
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,-0.5,0); //giro agujas relog
					}
					else
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,0.5,0); //giro contrario
					}
				}
				else
				{
					if (my_angle>angulo)
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,-0.5,0); //giro agujas relog
					}
					else
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,0.5,0); //giro contrario
					}
				}
			}
			else
			{
				if(my_angle>0)
				{
					if (my_angle-3.1415<angulo)
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,-0.5,0); //giro agujas relog
					}
					else
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,0.5,0);//giro contrario
					}
				}
				else
				{
					if (my_angle>angulo)
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,-0.5,0); //giro agujas relog
					}
					else
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,0.5,0);//giro contrario
					}
				}
			}
		}
			// Modo coche
/*
			double movement=0.05*sin(my_angle-angulo);

			if(angulo>0)
			{
				if(my_angle<0)
				{
					if (my_angle+3.1415<angulo)
					{
						playerc_position2d_set_cmd_car(sim_position2d[my_robot],movement*(movement>0),-0.5);
					}
					else
					{
						playerc_position2d_set_cmd_car(sim_position2d[my_robot],movement*(movement>0),0.5);
					}
				}
				else
				{
					if (my_angle>angulo)
					{
						playerc_position2d_set_cmd_car(sim_position2d[my_robot],movement*(movement>0),-0.5);
					}
					else
					{
						playerc_position2d_set_cmd_car(sim_position2d[my_robot],movement*(movement>0),0.5);
					}
				}
			}
			else
			{
				if(my_angle>0)
				{
					if (my_angle-3.1415<angulo)
					{
						playerc_position2d_set_cmd_car(sim_position2d[my_robot],movement*(movement>0),-0.5);
					}
					else
					{
						playerc_position2d_set_cmd_car(sim_position2d[my_robot],movement*(movement>0),0.5);
					}
				}
				else
				{
					if (my_angle>angulo)
					{
						playerc_position2d_set_cmd_car(sim_position2d[my_robot],movement*(movement>0),-0.5);
					}
					else
					{
						playerc_position2d_set_cmd_car(sim_position2d[my_robot],movement*(movement>0),0.5);
					}
				}
			}
		}
*/
		else
		{
			playerc_position2d_set_cmd_car(sim_position2d[my_robot],0.05,0);
		}
	}
}

/* ---------------------------------------------------------------------------
 * ---------------------------------------------------------------------------
 *
 * simAdhocSPF - Función principal que controla el despliegue.
 *
 * Parametros:
 *		d_data: Datos sobre el despliegue de la red
 *		rotos: Arrays de robots rotos al comienzo
 *		obstaculos: Referencia para los obstaculos del mundo
 *		sim_robots: Proxys de los robots
 *		sim_position2d: Proxys a sus interfaces de posicion
 *		simulation: Proxy a la interfaz simulacion
 *		sim_graphics2d: Proxys a sus interfaces de gráficos
 *
 * ---------------------------------------------------------------------------
 * --------------------------------------------------------------------------- */

// INFO: Function to move the agent toward a point. This point is calculated as a midpoint of the assigned locations to the robot as found in ar_data data structure
static double force_2_target(double distance, double k1, double k2)
{
	double f = k1 * (1 - pow(M_E, -k2 * distance));
	return f;
}
static void vectorTotal_2(playerc_position2d_t** sim_position2d, robot_data_t* robot_data, int number_robots, int num_rotos, int map_size, int my_robot, double* x, double* y,int obstaculos)
{
	double f,d,c1,c2,sigma1,sigma2,xoff,yoff;
	int j,no_moverse=0;
	int i=0;
	int cercanos=0;

	*x=0;
	*y=0;

//	if (i==0)
//	{
//		printf("Help me");
//	}


	// Dibuja lineas hasta los robots en rango
	for(i=0;i<number_robots;i++)
	{
		double distance=sqrt(pow(sim_position2d[my_robot]->px-sim_position2d[i]->px,2)+pow(sim_position2d[my_robot]->py-sim_position2d[i]->py,2));
		if(i!=my_robot && distance<=RADIO_COVERAGE_SPF && !robot_data[i].broken)
		{
			cercanos++;
		}
	}


	//1) Esta roto?
	if((robot_data[my_robot].broken==1)||(robot_data[my_robot].level!=0))
	{
		// De momento estoy quieto. (Luego lo movere hacia abajo)
		no_moverse=1;

		//XXXX. Añadida condicion de forma que si es el AP, tampoco se mueve

	}

	else
	{
		for(j=0;j<number_robots;j++)
		{
			// Se aleja 2 m de los que no estan rotos
			if (my_robot!=j)
			{
				d=calculateDistance(sim_position2d, j, my_robot);

				// Repulsion hasta 0.4 m
				c1=0.001;
				c2=0;
				sigma1=8;
				sigma2=1;// Da igual este termino se anula
				xoff=0;
				yoff=0;
				f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);

				*x=*x+f*(sim_position2d[j]->px - sim_position2d[my_robot]->px)/d;
				*y=*y+f*(sim_position2d[j]->py - sim_position2d[my_robot]->py)/d; //XXXX. Se divide por d???

				//Fuerza adicional de cohesion de la red
				/*if (robot_data[j].level>0)
				{
					f=20*((double)num_rotos/(double)number_robots)/((double)(cercanos+1));
					*x=*x+f*(sim_position2d[j]->px - sim_position2d[my_robot]->px);///d;
					*y=*y+f*(sim_position2d[j]->py - sim_position2d[my_robot]->py);///d; //XXXX. Se divide por d???
				}*/

			}
		}

	}

	// Repulsion de los muros laterales hasta 0.4 m.
	c1=0.001;
	c2=0;
	sigma1=8;
	sigma2=1; // Da igual este termino se anula
	xoff=0;
	yoff=0;

	// Muro norte:
	d=fabs(map_size/2-sim_position2d[my_robot]->py);
	f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
	*y=*y+f*(map_size/2 - sim_position2d[my_robot]->py)/d;

	// Muro sur:
	d=fabs(-map_size/2-sim_position2d[my_robot]->py);
	f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
	*y=*y+f*(-map_size/2-sim_position2d[my_robot]->py)/d;

	// Muro oeste:
	d=fabs(-map_size/2-sim_position2d[my_robot]->px);
	f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
	*x=*x+f*(-map_size/2-sim_position2d[my_robot]->px)/d;

	// Muro este:
	d=fabs(map_size/2-sim_position2d[my_robot]->px);
	f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
	*x=*x+f*(map_size/2 - sim_position2d[my_robot]->px)/d;

	// Escenario de obstaculos 1
	if(obstaculos==1)
	{
		// Repulsion muros interiores hasta 0.4 m
		c1=0.001; //XXXX. Aumentado (era 0.001) a ver si los obstaculos repelen un poco mas
		c2=0;
		sigma1=12;	//XXXX ¿12?¿Diferente a la otra (muertos y muros fronterizos)?
		sigma2=1;// Da igual este termino se anula
		xoff=0;
		yoff=0;

		// Muro superior
		if(sim_position2d[my_robot]->px>-0.5*map_size/5 && sim_position2d[my_robot]->px<0.5*map_size/5)
		{
			d=fabs(map_size/5-sim_position2d[my_robot]->py);
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*y=*y+f*(map_size/5-sim_position2d[my_robot]->py)/d;
		}
		else if(sim_position2d[my_robot]->px<-0.5*map_size/5)
		{
			d=sqrt(pow(-0.5*map_size/5-sim_position2d[my_robot]->px,2)+pow(map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(-0.5*map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(map_size/5-sim_position2d[my_robot]->py)/d;
		}
		else if(sim_position2d[my_robot]->px>0.5*map_size/5)
		{
			d=sqrt(pow(0.5*map_size/5-sim_position2d[my_robot]->px,2)+pow(map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(0.5*map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(map_size/5-sim_position2d[my_robot]->py)/d;
		}

		// Muro central
		if(sim_position2d[my_robot]->py<0 && sim_position2d[my_robot]->py>-0.5*map_size/5)
		{
			d=fabs(0-sim_position2d[my_robot]->px);
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(0-sim_position2d[my_robot]->px)/d;
		}
		else if(sim_position2d[my_robot]->py<-0.5*map_size/5)
		{
			d=sqrt(pow(0-sim_position2d[my_robot]->px,2)+pow(-0.5*map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(0-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(-map_size/5-sim_position2d[my_robot]->py)/d;
		}
		else if(sim_position2d[my_robot]->py>0)
		{
			d=sqrt(pow(0-sim_position2d[my_robot]->px,2)+pow(0-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(0-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(0-sim_position2d[my_robot]->py)/d;
		}

		// Muro derecho
		if(sim_position2d[my_robot]->py<-0.5*map_size/5 && sim_position2d[my_robot]->py>-1.5*map_size/5)
		{
			d=fabs(1.5*map_size/5-sim_position2d[my_robot]->px);
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(1.5*map_size/5-sim_position2d[my_robot]->px)/d; //XXXX. Aqui ponia 1-5 en lugar de 1.5
		}
		else if(sim_position2d[my_robot]->py>-0.5*map_size/5)
		{
			d=sqrt(pow(1.5*map_size/5-sim_position2d[my_robot]->px,2)+pow(-0.5*map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(1.5*map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(-0.5*map_size/5-sim_position2d[my_robot]->py)/d;
		}
		else if(sim_position2d[my_robot]->py<-1.5*map_size/5)
		{
			d=sqrt(pow(1.5*map_size/5-sim_position2d[my_robot]->px,2)+pow(-1.5*map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(1.5*map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(-1.5*map_size/5-sim_position2d[my_robot]->py)/d;
		}

		// Muro izquierdo
		if(sim_position2d[my_robot]->px>-1.5*map_size/5 && sim_position2d[my_robot]->px<-0.5*map_size/5)
		{
			d=fabs(-1.5*map_size/5-sim_position2d[my_robot]->py);
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*y=*y+f*(-1.5*map_size/5-sim_position2d[my_robot]->py)/d; //XXXX ¿modificar de 1.5 a 1.57?
		}
		else if(sim_position2d[my_robot]->px<-1.5*map_size/5)
		{
			d=sqrt(pow(-1.5*map_size/5-sim_position2d[my_robot]->px,2)+pow(-1.5*map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(-1.5*map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(-1.5*map_size/5-sim_position2d[my_robot]->py)/d;
		}
		else if(sim_position2d[my_robot]->px>-0.5*map_size/5)
		{
			d=sqrt(pow(-0.5*map_size/5-sim_position2d[my_robot]->px,2)+pow(-1.5*map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(-0.5*map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(-1.5*map_size/5-sim_position2d[my_robot]->py)/d;
		}
	}

	// Escenario de obstaculos 2
	if(obstaculos==2)
	{
		// Repulsion muros interiores hasta 0.4 m
		c1=0.001;
		c2=0;
		sigma1=12;		//XXXX. ¿¿¿Por qué diferente al de los otros???
		sigma2=1;// Da igual este termino se anula
		xoff=0;
		yoff=0;

		// Muro superior izquierdo
		if(sim_position2d[my_robot]->py<1.5*map_size/5 && sim_position2d[my_robot]->py>map_size/5)
		{
			d=fabs(-map_size/5-sim_position2d[my_robot]->px);
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(-map_size/5-sim_position2d[my_robot]->px)/d;
		}
		else if(sim_position2d[my_robot]->py>1.5*map_size/5)
		{
			d=sqrt(pow(-map_size/5-sim_position2d[my_robot]->px,2)+pow(1.5*map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(-map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(1.5*map_size/5-sim_position2d[my_robot]->py)/d;
		}
		else if(sim_position2d[my_robot]->py<map_size/5)
		{
			d=sqrt(pow(-map_size/5-sim_position2d[my_robot]->px,2)+pow(map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(-map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(map_size/5-sim_position2d[my_robot]->py)/d;
		}

		// Muro derecho
		if(sim_position2d[my_robot]->px>0 && sim_position2d[my_robot]->px<map_size/5)
		{
			d=fabs(0-sim_position2d[my_robot]->py);
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*y=*y+f*(0-sim_position2d[my_robot]->py)/d;
		}
		else if(sim_position2d[my_robot]->px<0)
		{
			d=sqrt(pow(0-sim_position2d[my_robot]->px,2)+pow(0-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(0-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(0-sim_position2d[my_robot]->py)/d;
		}
		else if(sim_position2d[my_robot]->px>map_size/5)
		{
			d=sqrt(pow(map_size/5-sim_position2d[my_robot]->px,2)+pow(0-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(0-sim_position2d[my_robot]->py)/d;
		}

		// Muro inferior izquierdo
		if(sim_position2d[my_robot]->py>-1.5*map_size/5 && sim_position2d[my_robot]->py<-map_size/5)
		{
			d=fabs(-map_size/5-sim_position2d[my_robot]->px);
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(-map_size/5-sim_position2d[my_robot]->px)/d;
		}
		else if(sim_position2d[my_robot]->py<-1.5*map_size/5)
		{
			d=sqrt(pow(-map_size/5-sim_position2d[my_robot]->px,2)+pow(-1.5*map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(-map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(-1.5*map_size/5-sim_position2d[my_robot]->py)/d;
		}
		else if(sim_position2d[my_robot]->py>-map_size/5)
		{
			d=sqrt(pow(-map_size/5-sim_position2d[my_robot]->px,2)+pow(-map_size/5-sim_position2d[my_robot]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
			*x=*x+f*(-map_size/5-sim_position2d[my_robot]->px)/d;
			*y=*y+f*(-map_size/5-sim_position2d[my_robot]->py)/d;
		}
	}

	if(no_moverse)
	{
		*x=0;
		*y=0;
	}
}

void move_uni_dynamics(playerc_position2d_t** sim_position2d, robot_data_t* robot_data, tasks_data* t_data, assigned_robots* ar_data, int my_robot, int number_robots, int num_rotos, int map_size,int obstaculos, int type_motion, int id_AP)
{
	//calculate the desired displacement of the robot
	double x, y;
	int followed=0;
	double dx = 0.0;
	double dy = 0.0;
	double current_theta = sim_position2d[my_robot]->pa;

	if (type_motion == 1) // Move toward new location
	{
		for(int i=0; i<t_data->num_tasks; i++)
		{
			if(ar_data->tasks_assigned[my_robot*t_data->num_tasks+i]==1)
			{
				followed++;
				dx += ar_data->xpos[my_robot*t_data->num_tasks+i] - sim_position2d[my_robot]->px;
				dy += ar_data->ypos[my_robot*t_data->num_tasks+i] - sim_position2d[my_robot]->py;
				printf("\nRobot %d x=%f y=%f, going for ", my_robot, sim_position2d[my_robot]->px, sim_position2d[my_robot]->py);
				printf("\ntask %d x=%f y=%f", i, ar_data->xpos[my_robot*t_data->num_tasks+i], ar_data->ypos[my_robot*t_data->num_tasks+i]);
				printf("\ndx=%f dy=%f", dx, dy);
			}
		}
	}
	else if (type_motion == 2) // move toward Collection Point, in this case AP.
	{
		dx += sim_position2d[id_AP]->px - sim_position2d[my_robot]->px;
		dy += sim_position2d[id_AP]->py - sim_position2d[my_robot]->py;
	}
	double velocity = sqrt(dx*dx+dy*dy);
	printf("\nRobot %d total displacement %f", my_robot, velocity);
	double desired_theta = atan2(dy, dx);
	x = 0.0;
	y = 0.0;
	double f = 0.0;
	//if(velocity >= 10000000000000000.0)
	double move = 0.4;
	if(velocity > move)
	{
		//calculate repulsion forces from obstacles, other robots, and walls
		vectorTotal_2(sim_position2d,robot_data,number_robots,num_rotos,map_size,my_robot,&x,&y,obstaculos);
		//calculate attraction force of the goal
		double k1, k2;
		k1 = 1.0;
		k2 = 1.0;
		f=force_2_target(velocity, k1, k2);

		x += f*dx;
		y += f*dy;
		//x = dx;
		//y = dy;

		velocity = sqrt(x*x+y*y);
		desired_theta = atan2(y, x);
	}

	//if(velocity < 10000000000000000.0 || (robot_data[my_robot].broken==1) || (robot_data[my_robot].level!=0))
	if(velocity < move|| (robot_data[my_robot].broken==1) || (robot_data[my_robot].level!=0))
	{
		playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,0,0);
		playerc_position2d_set_cmd_car(sim_position2d[my_robot],0,0);

		//XXXX: Faltaba aqui????...
		robot_data[my_robot].stopped=1;
	}
	else
	{
		robot_data[my_robot].stopped=0;
		if(fabs(current_theta-desired_theta) > 0.1)
		{
			// Modo giro
			playerc_position2d_set_cmd_car(sim_position2d[my_robot],0,0);

			if(desired_theta>0)
			{
				if(current_theta<0)
				{
					if (current_theta+3.1415<desired_theta)
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,-0.5,0); //giro agujas relog
					}
					else
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,0.5,0); //giro contrario
					}
				}
				else
				{
					if (current_theta>desired_theta)
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,-0.5,0); //giro agujas relog
					}
					else
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,0.5,0); //giro contrario
					}
				}
			}
			else
			{
				if(current_theta>0)
				{
					if (current_theta-3.1415<desired_theta)
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,-0.5,0); //giro agujas relog
					}
					else
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,0.5,0);//giro contrario
					}
				}
				else
				{
					if (current_theta>desired_theta)
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,-0.5,0); //giro agujas relog
					}
					else
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,0.5,0);//giro contrario
					}
				}
			}
		}
		else
		{
			playerc_position2d_set_cmd_car(sim_position2d[my_robot],0.05,0);
		}
	}


	//if current position closer than the UMBRIAL distance to the destination don't move

	//calculate the speed and angle

	//set the speed and angle in player/stage
}
// INFO: The final positions of robots after each round of spf (initial or during the simulation) are fixed as task locations over which robots negotiate
tasks_data* location2cover(tasks_data* t_data, int num_robots, playerc_position2d_t** sim_position2d, int* broken) //should return the data structure array or array of data structures?
{
	int br = 0;
	//Count how many broken robots at startup
	if(!broken==NULL)
		for(int i=0; i< num_robots; i++)
		{
			printf("\nbroken %d", broken[i]);
			if(broken[i] == 1) br++;
		}

	for(int i=0; i< num_robots; i++)
		{
			if(br==0)
			{
				t_data->task_ids[i] = i;
				t_data->xpos[i] = sim_position2d[i]->px;
				t_data->ypos[i] =sim_position2d[i]->py;
				printf("\n Location to cover idx: %d, xpos: %f, ypos: %f", i, t_data->xpos[i], t_data->ypos[i]);
			}
			else
			{
				if(broken[i] == 0)
				{
					t_data->task_ids[i] = i;
					t_data->xpos[i] = sim_position2d[i]->px;
					t_data->ypos[i] = sim_position2d[i]->py;
					printf("\n Location to cover idx: %d, xpos: %f, ypos: %f", i, t_data->xpos[i], t_data->ypos[i]);
				}
				else
				{
					printf("\n Robot broken idx: %d, its position not considered in the negotiation", i);
					t_data->task_ids[i] = -1;
				}
			}

		}

	return t_data;
}
// INFO: Calculates the willingness of the robot based on the battery levels, and the nr of locations that it is following

//double calc_willingness(double battery, assigned_robots* ar_data, int num_tasks, int my_robot)//gets array of factors.
double calc_willingness(double battery, playerc_position2d_t** sim_position2d, int AP_id, int my_robot, double* w_H, double* w_H20)//gets array of factors.
{
	double willingness = 0.0;
	double dist = 0.0;
	double distance2AP = 0.0;
	double bl0 = 0.0;
	double bl1 = 0.0;
	double blh = 0.0;
	double blh20 = 0.0;

	//calculate distance from AP, my own location, ap location
	dist = (sim_position2d[my_robot]->px - sim_position2d[AP_id]->px)*(sim_position2d[my_robot]->px - sim_position2d[AP_id]->px);
	dist += (sim_position2d[my_robot]->py - sim_position2d[AP_id]->py)*(sim_position2d[my_robot]->py - sim_position2d[AP_id]->py);
	distance2AP = sqrt(dist);
	//calculate estimated battery expenditure for going to AP, bl0
	bl0 = distance2AP*BATTERY_WASTE_PER_DISTANCE;
	bl1 = bl0 + 0.3*bl0;
	blh = bl0 + 0.1*bl0;
	blh20 = blh + 0.2*(bl1 - blh);
	//calculate the willingness
	if(battery <= bl0) willingness = -1.0;
	else if(bl0 < battery && battery <= bl1) willingness = -(battery-bl0)/battery;
	else if (battery > bl1) willingness = (battery-bl1)/battery;
	else printf("BAD VALUE");

	*w_H = - (blh-bl0)/blh;
	*w_H20 = - (blh20-bl0)/blh20;
	//calculate willingness at blh

	/*
	int no_factors = 2;
	double f1, f2;
	int my_tasks = 0;
	 for(int i=0; i<num_tasks;i++)
		if(ar_data->tasks_assigned[my_robot*num_tasks+i] == 1)
		{
			my_tasks++;
		}
	//printf("Robot %d, total tasks %d, battery %f, init battery %d\n", my_robot, my_tasks, battery, ROBOT_INITIAL_BATTERY);
	if(my_tasks > 0) f1=1.0/(float)my_tasks;
	else f1=1.0;

	if(f1>=0.3) f1=f1/no_factors;
	else f1=-f1/no_factors;
	f1=0;

	if (battery<=(0.05*ROBOT_INITIAL_BATTERY))
	{
		willingness=-1.0;
	}
	else if (battery<=(0.4*ROBOT_INITIAL_BATTERY))
	{
		f2=-battery/(ROBOT_INITIAL_BATTERY*no_factors);

		willingness = f1+f2;
	}
	else
	{
		f2=battery/(ROBOT_INITIAL_BATTERY*no_factors);

		willingness = f1+f2;
	}*/
	printf("\nRobot %d, willingness %f, w_H %f, w_H20 %f", my_robot, willingness, *w_H, *w_H20);
	return willingness;
}
// INFO: Calculates utility of moving somewhere. Depends solely on the distance from such point
double calc_utility(playerc_position2d_t** sim_position2d, double battery, int my_robot, int robot_2help, int AP_id, int isitAP, double w, int packets_per_agent[])
{
	//Calculate the utility based on the distance the robot would have to go
	double b_newpoint, b_np_AP, b_state;
	double b_trafficA, b_trafficB;
	double utility = 0.0;
	double dist;

	//calculate how much battery I need to go to the new point
	if(!(isitAP == 1))
	{
		dist = (sim_position2d[my_robot]->px - sim_position2d[robot_2help]->px)*(sim_position2d[my_robot]->px - sim_position2d[robot_2help]->px);
		dist += (sim_position2d[my_robot]->py - sim_position2d[robot_2help]->py)*(sim_position2d[my_robot]->py - sim_position2d[robot_2help]->py);
		b_newpoint = sqrt(dist)*BATTERY_WASTE_PER_DISTANCE;
		//calculate how much battery I need to go from the new point to AP (or any collection point)
		dist = (sim_position2d[robot_2help]->px - sim_position2d[AP_id]->px)*(sim_position2d[robot_2help]->px - sim_position2d[AP_id]->px);
		dist += (sim_position2d[robot_2help]->py - sim_position2d[AP_id]->py)*(sim_position2d[robot_2help]->py - sim_position2d[AP_id]->py);
		b_np_AP = sqrt(dist)*BATTERY_WASTE_PER_DISTANCE;
		//Calculate what battery is left after robot moves to new point and AP
		b_state = 1 - (b_newpoint + b_np_AP)/battery;

		if(packets_per_agent[my_robot] == 0 || packets_per_agent[robot_2help] == 0) utility = 0.0;
		else
		{
			b_trafficA = battery / (packets_per_agent[my_robot]*ROBOT_WASTE_PER_PACKET);
			b_trafficB = battery / (packets_per_agent[robot_2help]*ROBOT_WASTE_PER_PACKET);
			if(b_state >= 0.3)
			{
				utility = b_state + (b_trafficA - b_trafficB) / (b_trafficA + b_trafficB); //OR b_trafficB - b_trafficA
			}
			else utility = -w;
		}

	}
	/*
	int followed=0;
	double dx = 0.0;
	double dy = 0.0;
	int condition = 0;
	if(isitAP == 1) condition = -1;
	else condition = 1;

	for(int i=0; i<num_tasks; i++)
	{	//printf("\n checking out tasks for robot: %d, task %d status %d", robot_2help, i, ar_data->tasks_assigned[robot_2help*num_tasks+i]);
		if(ar_data->tasks_assigned[robot_2help*num_tasks+i]==condition)
		{
			followed++;
			dx += ar_data->xpos[robot_2help*num_tasks+i] - sim_position2d[my_robot]->px;
			dy += ar_data->ypos[robot_2help*num_tasks+i] - sim_position2d[my_robot]->py;
			printf("\nCalculating utility");
			printf("\nRobot %d x=%f y=%f, going for ", my_robot, sim_position2d[my_robot]->px, sim_position2d[my_robot]->py);
			printf("\ntask %d x=%f y=%f", i, ar_data->xpos[robot_2help*num_tasks+i], ar_data->ypos[robot_2help*num_tasks+i]);
			printf("\ndx=%f dy=%f", dx, dy);
		}
	}
	double vel = sqrt(dx*dx+dy*dy);
	if(vel==0) utility=0.0;
	else utility=1/vel;*/
	printf("\nUtility is %f of robot %d for robot %d", utility, my_robot, robot_2help);
	return utility;
}
// INFO: assigns robots to their final location after SPF rounds
assigned_robots* initial_assignment(assigned_robots* ar_data, tasks_data* t_data, int num_robots, int* broken)
{
	int br = 0;
	//Check if there are broken
	if(!broken==NULL)
	{
		br = 1;
	}

	for(int i=0; i < num_robots; i++)
	{
		if(br==0)
		{
			for(int j=0; j<t_data->num_tasks; j++)
			{
				if(i==j)
				{
					//ar_data->robots[i*t_data->num_tasks+j] = 1;
					ar_data->tasks_assigned[i*t_data->num_tasks+j] = 1;
					ar_data->xpos[i*t_data->num_tasks+j] = t_data->xpos[j];
					ar_data->ypos[i*t_data->num_tasks+j] = t_data->ypos[j];
					//printf("\n Robot %d to cover: %d, xpos: %f, ypos: %f",i, t_data->task_ids[j], t_data->xpos[j], t_data->ypos[j]);
				}
				else ar_data->tasks_assigned[i*t_data->num_tasks+j] = 0;
			}

		}
		else
		{
			if(broken[i] == 0)
			{
				for(int j=0; j<t_data->num_tasks; j++)
				{
					if(i==j)
					{
						//ar_data->robots[i*t_data->num_tasks+j] = 1;
						ar_data->tasks_assigned[i*t_data->num_tasks+j] = 1;
						ar_data->xpos[i*t_data->num_tasks+j] = t_data->xpos[j];
						ar_data->ypos[i*t_data->num_tasks+j] = t_data->ypos[j];
						//printf("\n Robot %d to cover: %d, xpos: %f, ypos: %f", i, t_data->task_ids[j], t_data->xpos[j], t_data->ypos[j]);
					}
					else ar_data->tasks_assigned[i*t_data->num_tasks+j] = 0;
				}

			}
			else
			{
				for(int j=0; j<t_data->num_tasks; j++)
								{
					ar_data->tasks_assigned[i*t_data->num_tasks+j] = 0;

				}
			}
		}
	}


	for(int i = 0; i < num_robots; i++)// assumes that a task id matches the id of a non broken agent, i.e., the agent on that location
		{
			printf("\n Robot %d wrt tasks: ", i);
			for(int j=0; j<t_data->num_tasks; j++)
			printf(" %d ", ar_data->tasks_assigned[i*t_data->num_tasks+j]);
		}

	return ar_data;
}

deployment_data_t* simAAagents2(deployment_data_t* d_data, tasks_data* t_data, assigned_robots* ar_data,help_protocol_data* hpd_data,int* rotos,int obstaculos,playerc_client_t **sim_robots, playerc_position2d_t** sim_position2d,playerc_simulation_t** simulation, playerc_graphics2d_t** sim_graphics2d,playerc_sonar_t** sim_sonar, int AA_alg_delegation_strategy, int packets_per_agent[])
//Declaro variables
{
	int i,contador=0,stop=0,n=0,stopped=0,LastStopped=0;
	srand(time(NULL));

	double mean_power;

	int reader,salir,level;

	int number_robots=d_data->number_robots;
	int map_size=d_data->map_size;

	double* msj_tx=(double*) malloc(sizeof(double) * number_robots);

	//Inicializo variables
	for(i=0;i<number_robots;i++)
	{
		if(d_data->nueva_simulacion)
		{
			//Variables algoritmo
			d_data->robot_data[i].turn=0;
			d_data->robot_data[i].turning_count=0;
			d_data->robot_data[i].walking_count=0;
			d_data->robot_data[i].backbone=0;
			d_data->robot_data[i].stopped=0;
			d_data->robot_data[i].level=0;
			d_data->robot_data[i].registered_to=-1;
			d_data->robot_data[i].num_registered=0;
			d_data->p_data->distance_walked[i]=0; //XXXX. Movido aqui
			d_data->robot_data[i].remaining_battery=ROBOT_INITIAL_BATTERY;
			d_data->FinalAP=-1;

			//variables relating to the help protocols
			hpd_data->requested_help[i]=0;


		}
		else
		{
			if(rotos!=NULL)
			{
				if (rotos[i])
				{
					//XXXX. Acumulativo
					BreakSPF(sim_position2d,d_data->robot_data,i,number_robots,&(d_data->FinalAP));

				}
			}

			// INFO: if there is a new dead robot then its tasks are marked with -1 in ar_data. This is to allow the AP to detect such tasks that are left untracked
			printf("\n New iteration robot assignment %d wrt tasks, accounting for dead robots: ", i);
			for(int j=0; j<t_data->num_tasks; j++)
			{
				if(rotos[i])
				{
					if(ar_data->tasks_assigned[i*t_data->num_tasks+j]==1)
						ar_data->tasks_assigned[i*t_data->num_tasks+j] = -1;
					if(hpd_data->started_negotiation[i] == 1)
					{
						hpd_data->unfinished_negotiations++;
						hpd_data->started_negotiation[i] = 0;
					}
				}
				printf(" %d ", ar_data->tasks_assigned[i*t_data->num_tasks+j]);
			}


		}
	//		else
	//		{
	//			d_data->robot_data[i].broken|=0;
	//		}

		//Variables para medir rendimiento
		//d_data->p_data->distance_walked[i]=0;//XXXX. Solo si nueva
		d_data->p_data->packets_sent[i]=0;
		d_data->p_data->own_packets_sent[i]=0;
		d_data->p_data->packets_rx[i]=0;
		d_data->p_data->own_packets_rx[i]=0;

	}

	//Robots tell where they are, pose and odometrics and move a bit
	for(i=0;i<number_robots;i++)
	{

		if (d_data->nueva_simulacion)
		{
			//XXXX ¿necesario si no nueva simulacion???
			//En caso de serlo, hacerlo solo para los no rotos que no sean AP
	//			if (!d_data->robot_data[i].broken)
	//			{
				playerc_position2d_set_cmd_car(sim_position2d[i],0.005,0);
	//			}
		}
		if(i%10==0) playerc_client_read(*sim_robots);

		char spider[10];
		sprintf(spider,"spider%d",i+1);

		double px,py,pa;
		playerc_simulation_get_pose2d(*simulation, spider, &px,&py,&pa);
		//printf("Robot  %d empieza en: %f %f %f\n", i,px,py,pa);

		if(i%10==0) playerc_client_read(*sim_robots);

		d_data->p_data->positions[i].px=px;
		d_data->p_data->positions[i].py=py;
		d_data->p_data->positions[i].pa=pa;
	}

	for(i=0;i<number_robots;i++)
	{
		playerc_position2d_set_cmd_car(sim_position2d[i],0,0);
		if(i%10==0)
			playerc_client_read(*sim_robots);
	}


	//Added by XXXX. Escogemos un nodo como AP....
	if (d_data->nueva_simulacion)
	{
		d_data->FinalAP=chooseAP(sim_position2d, d_data->robot_data,number_robots,0,-1);
	}
	d_data->robot_data[d_data->FinalAP].stopped=1;
	d_data->robot_data[d_data->FinalAP].level=1; //No hay nivel 2...

	printf("AP escogido %i",d_data->FinalAP);


	// --------------------------------------------		DEPLOYMENT		----------------------------------------------------

	time_t comienzo=time(NULL);
	time_t LastTime=time(NULL);
	time_t final,comienzo_pausa,final_pausa;
	double pause_time=0;

	//sleep(2); //XXXX. Pausa Para poder ver los mensajes

	//system("reset");
	printf("Para pausar la simulación y mostrar el menu de simulación pulse enter\n");
	int negotiation_concluded = 0;
	// INFO: write data to file. This logs data used for off-line plotting
	FILE *log = NULL;
	log = fopen("./agent_log_data.log", "a");
	if(log==NULL)
	{
		printf("ERROR: cannot OPEN log file");
		return -1;
	}
	// INFO: Start of Adaptive Algorithm
	// INFO: there is only one negotiation round per call of this function simAAagents2
	// INFO: There are two steps. First negotiations, if any, take place. After these are settled, and ar_data updated, comes the second step, i.e. agents that should move, will move.
	int negotiation_round = 4;
	double rate_battery_drop = 0.0; // this is not used for now
	double willingness[number_robots];
	double w_H[number_robots];
	double w_H20[number_robots];
	for(int w=0; w<number_robots; w++)
	{
		//TODO calculate willingness for each. set toggle for negotiation
		//TODO define what happens when the robot breaks
		willingness[w] = calc_willingness(d_data->robot_data[i].remaining_battery, sim_position2d, d_data->FinalAP, w, &w_H[w], &w_H20[w]);

		//TODO if willingness = -1 for 1 agent then set: negotiation_round = 1;
		printf("\n Willingness %f for robot %d, w_H %f, w_H20 %f", willingness[w], w, w_H[w], w_H20[w]);
		if(willingness[w] == -1) negotiation_round = 1;
	}

	while(stop!=1)
	{

		//Wait for new data from server
		playerc_client_read(*sim_robots);


		//First deal with negotiations if it is not the initial round or a round with SPF
		if (!d_data->nueva_simulacion && !hpd_data->round_of_SPF && negotiation_round <= 3)
		{
			while(negotiation_round <= 3)
			{
				negotiation_round++; //When this variable becomes 4 then the negotiation round is over, the agents will then continue to move to the new destinations if any.
				for(i=0;i<number_robots;i++)
				{
					// INFO: if the robot is broken it will not take part in the negotiation
					if(d_data->robot_data[i].broken==1)
					{

						//printf("Broken robot %d, won't communicate \n", i);
						willingness[i] = -1.0;

					}
					else // INFO: if the agent is alive then it negotiates depending on whether it is AP or not as described below in the comments
					{
						// INFO: Calculate willingness, once per call of this function simAAagents2
						//willingness[i] = calc_willingness(d_data->robot_data[i].remaining_battery, ar_data, t_data->num_tasks, i);

						if(d_data->FinalAP==i) // INFO: if the agent is AP it only generates help requests, and handles tasks marked as -1 in ar_data. The AP does not answer to help requests
						{
							//printf("\nThis is the AP which will check if there is any unhandled task in the network\n");
							// The AP negotiates for one task per call of this function simAAagents2

							if(hpd_data->requested_help[i]==0)
							{
								int found = 0;
								for(int t=0; t<number_robots; t++)
								{
									for(int task=0; task<t_data->num_tasks;task++)
									{
										if(ar_data->tasks_assigned[t*number_robots+task]==-1 && found == 0)
										{
											printf("AP started negotiation for %d\n", t);
											//In order for the AP to signal which needs help, the requested
											hpd_data->started_negotiation[i]=1;
											hpd_data->total_negotiations++;
											hpd_data->total_negotiations_AP++;
											hpd_data->requested_help[i]=1; //INFO: the AP concludes a negotiation after one iteration, thus hpd_data->requested_help[i]=3
											hpd_data->robot2help = t;
											hpd_data->msgs_exchanged[i]++;
											found = 1;
											break;
										}
									}
								}
							}
							else if(hpd_data->requested_help[i]==3)
							{
								printf("AP %d will conclude negotiation,\n", i);
								hpd_data->requested_help[i]=5;
								// INFO: Check who wants to help me
								int assigned_robot = -1;
								float wu = -100.0;
								// INFO: the AP checks all the answers it got and picks the robot with highest willingness+utility
								hpd_data->started_negotiation[i]=0;
								for(int j=0; j<number_robots; j++)
								{
									if(!(i==j) && !(d_data->FinalAP==j)) // INFO: I won't get help from myself or AP
									{
										if(hpd_data->giving_help[i*number_robots+j] > 0.0)
										{
											printf("Robot %d wants to help, with wu %.2f,\n", j, hpd_data->giving_help[i*number_robots+j]);
											if(hpd_data->giving_help[i*number_robots+j] > wu)
											{
												wu = hpd_data->giving_help[i*number_robots+j];
												assigned_robot = j;
											}
										}
									}
								}
								printf("Robot %d is assigned with wu %.2f,\n", assigned_robot, wu);
								// INFO: clear the responses given to robot i
								for(int jj=0; jj<number_robots; jj++)
								{
									hpd_data->giving_help[i*number_robots+jj] = -1.0;
								}

								// INFO: change ar_data, putting the id of the newly assigned agent instead of the one that requested for help.
								printf("BEFORE AP assignment\n");
								for(int jj=0; jj<number_robots; jj++)
								{
									for(int j=0; j<t_data->num_tasks; j++)
										printf(" %d ", ar_data->tasks_assigned[jj*t_data->num_tasks+j]);
									printf("\n");
								}

								for(int j=0; j<t_data->num_tasks; j++)
								{
									printf("for robot %d, wrt task %d status=%d\n", hpd_data->robot2help, j, ar_data->tasks_assigned[hpd_data->robot2help*t_data->num_tasks+j]);
									if(ar_data->tasks_assigned[hpd_data->robot2help*t_data->num_tasks+j] == -1)
									{
										if(assigned_robot==-1) // INFO: if noone is assigned this tasks remains marked as -1.
										{
											ar_data->tasks_assigned[hpd_data->robot2help*t_data->num_tasks+j] = -1; // INFO: this means that the task is not assigned, this is what the AP handles.
										}
										else
										{
											ar_data->tasks_assigned[hpd_data->robot2help*t_data->num_tasks+j] = 0; // INFO: agent that had this task, drops it.
											printf("delegation strategy: %d", AA_alg_delegation_strategy);
											if(AA_alg_delegation_strategy == 1)
											{
												printf("AP assigns according to the first strategy");
												ar_data->tasks_assigned[assigned_robot*t_data->num_tasks+j] = 1; // INFO: assigned agent add location as a task in ar_data
												ar_data->xpos[assigned_robot*t_data->num_tasks+j] = ar_data->xpos[hpd_data->robot2help*t_data->num_tasks+j];
												ar_data->ypos[assigned_robot*t_data->num_tasks+j] = ar_data->ypos[hpd_data->robot2help*t_data->num_tasks+j];
											}
											else
											{
												printf("AP assigns according to the second strategy");
												for(int cancel=0; cancel<t_data->num_tasks; cancel++)
												{
													ar_data->tasks_assigned[assigned_robot*t_data->num_tasks+cancel] = 0; // INFO: assigned agent initially drops previous locations
												}
												ar_data->tasks_assigned[assigned_robot*t_data->num_tasks+j] = 1;
												ar_data->xpos[assigned_robot*t_data->num_tasks+j] = ar_data->xpos[hpd_data->robot2help*t_data->num_tasks+j];
												ar_data->ypos[assigned_robot*t_data->num_tasks+j] = ar_data->ypos[hpd_data->robot2help*t_data->num_tasks+j];
											}

											hpd_data->msgs_exchanged[i]++;
										}
										negotiation_concluded++;
									}
								}
								printf("AFTER AP assignment\n");

								for(int jj=0; jj<number_robots; jj++)
								{
									for(int j=0; j<t_data->num_tasks; j++)
										printf(" %d ", ar_data->tasks_assigned[jj*t_data->num_tasks+j]);
									printf("\n");
								}

							}

						}
						else // INFO: if agent is not AP the behaviour is based on the calculated willingness
						{
							rate_battery_drop = (d_data->robot_data[i].remaining_battery-hpd_data->previous_step_battery[i])/ROBOT_INITIAL_BATTERY;

							if(willingness[i] <= 0.0) // INFO: in this case the agent is not dead yet but it's getting there. this agent won't move and will start a negotiation round
								// INFO: to reassign its tasks
							{
								// INFO: does not apply at this stage: if my agent goes from a high battery drop rate to negative willingness then whatever process initiated can be stopped, and the
								// INFO: does not apply at this stage: agent can proceed to behave as it would for the negative willingness
								int own_tasks = 0;
								for(int ty = 0; ty < t_data->num_tasks; ty++)
								{
									if(ar_data->tasks_assigned[i*t_data->num_tasks+ty] == 1)
									{
										own_tasks++;
									}
								}
								if(hpd_data->requested_help[i]==8)hpd_data->requested_help[i]=0;

								if(hpd_data->requested_help[i]==0 && own_tasks>0) // INFO: agent just started the negotiation, agent only starts if it has tasks in task list
								{
									printf("Robot %d started the negotiation due to negative willingness,\n", i);
									hpd_data->started_negotiation[i]=1;
									hpd_data->total_negotiations++;
									hpd_data->requested_help[i]=1;
									hpd_data->msgs_exchanged[i]++;
								}
								else if(hpd_data->requested_help[i]==3) // INFO: agent will conclude the negotiation, as with AP this happens after one more call of simAAgents2
								{
									printf("Robot %d will conclude negotiation,\n", i);
									hpd_data->requested_help[i]=0;
									// Check who wants to help me
									int assigned_robot = -1;
									float wu = -100.0;
									// INFO: finding the assigned robot and assignment happens as in the AP case
									hpd_data->started_negotiation[i]=0;
									for(int j=0; j<number_robots; j++)
									{
										if(!(i==j) && !(d_data->FinalAP==j)) //I won't get help from myself
										{
											if(hpd_data->giving_help[i*number_robots+j] > 0.0)
											{
												printf("Robot %d wants to help, with wu %.2f,\n", j, hpd_data->giving_help[i*number_robots+j]);
												if(hpd_data->giving_help[i*number_robots+j] > wu)
												{
													wu = hpd_data->giving_help[i*number_robots+j];
													assigned_robot = j;
												}
											}
										}
									}
									printf("Robot %d is assigned with wu %.2f,\n", assigned_robot, wu);
									// INFO: clear the responses given to robot i
									for(int jj=0; jj<number_robots; jj++)
									{
										hpd_data->giving_help[i*number_robots+jj] = -1.0;
									}

									// INFO: change ar_data, putting the id of the newly assigned agent instead of the one that requested for help.

									for(int j=0; j<t_data->num_tasks; j++)
									{
										if(ar_data->tasks_assigned[i*t_data->num_tasks+j] == 1)
										{
											if(assigned_robot==-1)
											{
												ar_data->tasks_assigned[i*t_data->num_tasks+j] = -1; // INFO: this means that the task is not assigned, this is what the AP handles.
											}
											else
											{
												ar_data->tasks_assigned[i*t_data->num_tasks+j] = 0;
												if(AA_alg_delegation_strategy == 1)
												{
													ar_data->tasks_assigned[assigned_robot*t_data->num_tasks+j] = 1;
													ar_data->xpos[assigned_robot*t_data->num_tasks+j] = ar_data->xpos[i*t_data->num_tasks+j];
													ar_data->ypos[assigned_robot*t_data->num_tasks+j] = ar_data->ypos[i*t_data->num_tasks+j];
												}
												else
												{
													for(int cancel=0; cancel<t_data->num_tasks; cancel++)
													{
														ar_data->tasks_assigned[assigned_robot*t_data->num_tasks+cancel] = 0;
													}
													ar_data->tasks_assigned[assigned_robot*t_data->num_tasks+j] = 1;
													ar_data->xpos[assigned_robot*t_data->num_tasks+j] = ar_data->xpos[i*t_data->num_tasks+j];
													ar_data->ypos[assigned_robot*t_data->num_tasks+j] = ar_data->ypos[i*t_data->num_tasks+j];
												}
												negotiation_concluded++;
												hpd_data->msgs_exchanged[i]++;
											}
										}
									}
								}
								//Don't move
								//Don't route
							}
							else // INFO: if the willingness is positive, the agent will handle the help requests it has gotten and send its answer as w+u, i.e., willingness+utility
							{
								//printf("Handle help requests\n");
								for(int x = 0; x<number_robots; x++)
								{
									if(hpd_data->requested_help[x]==2)
									{
										//If an agent has willingness lower than w_H its request is considered, otherwise we consider only the 20% of those requests.
										double u;
										if(willingness[x] < w_H20[x])
										{
											int isitAP = 0;
											int robot2help = x;
											hpd_data->msgs_exchanged[i]++;
											if(d_data->FinalAP==x)
											{
												isitAP = 1;
												robot2help = hpd_data->robot2help;
												printf("\nThe AP is asking for help for robot %d ", hpd_data->robot2help);
											}

											//w = calc_willingness(d_data->robot_data[i].remaining_battery, ar_data, t_data->num_tasks, i);
											//TODO change utility function to reflect the calculation we did.
											u = calc_utility(sim_position2d, d_data->robot_data[i].remaining_battery, i, robot2help, d_data->FinalAP, isitAP, willingness[i], packets_per_agent);
										}
										else u = -1.0;

										hpd_data->giving_help[x*number_robots+i] = willingness[i] + u;
									}
								}
							}
						}
					}
				}
				// INFO: update hpd_data->requested_help such that negotiation progresses as it should on the next step
				for(int i=0; i<number_robots; i++)
				{
					if(hpd_data->requested_help[i]==5)hpd_data->requested_help[i]=0; //if the AP has finished one negotiation it can continue with the rest.
					if(hpd_data->requested_help[i]>=1)hpd_data->requested_help[i]++;
				}
			}

		}
		// INFO: put the logging for matlab plotter here once per call of simAAgents2
		if(negotiation_round == 4)
		{
			negotiation_round++;
			for(i=0;i<number_robots;i++)
			{
				fprintf(log, "%d ", d_data->robot_data[i].broken);
				if(d_data->FinalAP==i) fprintf(log, "%d ", 1);
				else fprintf(log, "%d ", 0);
				fprintf(log, "%f %f ;", willingness[i], d_data->robot_data[i].remaining_battery);
				for(int t=0; t<number_robots; t++)
				{
					if(!(i==t))
						{
							if(hpd_data->requested_help[t]==1)
							{
								fprintf(log, "%d;", t+1);
							}
						}

				}
				fprintf(log, " ,;");
				for(int t=0; t<number_robots; t++)
				{
					if(!(i==t)) if(hpd_data->requested_help[t]==2) fprintf(log, "%d,%f;", t+1, hpd_data->giving_help[t*number_robots+i] - willingness[i]);

				}
				fprintf(log, " ,,;");
				int own_tasks = 0;
				for(int ty = 0; ty < t_data->num_tasks; ty++)
				{
					if(ar_data->tasks_assigned[i*t_data->num_tasks+ty] == 1)
					{
						fprintf(log, "%d,%f,%f;", ty+1, ar_data->xpos[i*t_data->num_tasks+ty], ar_data->ypos[i*t_data->num_tasks+ty]);
						printf("Robot %d assigned %d with position %f %f", i, ty, ar_data->xpos[i*t_data->num_tasks+ty], ar_data->ypos[i*t_data->num_tasks+ty]);
						own_tasks++;
					}
				}
				fprintf(log, " %d ", own_tasks);
				fprintf(log, " %d\n ", hpd_data->msgs_exchanged[i]);

			}
		}

		//if (!d_data->nueva_simulacion && !hpd_data->round_of_SPF) ;
		if (hpd_data->round_of_SPF) fprintf(log, "SPF");


		// INFO: Move robots if necessary
		for(i=0;i<number_robots;i++)
		{
			//0)Si esta roto no hace nada. (Lo mismo me lo llevo hacia abajo...)

			if(d_data->robot_data[i].broken==1)
			{
				//printf("Robot %d está roto.\n",i);
				//Use adhocSPF if first iteration
				if (d_data->nueva_simulacion)
				 {
					moverse(sim_position2d,d_data->robot_data,i,number_robots,d_data->num_rotos,map_size,obstaculos);
				 }
				else
				{
					//printf("Broken robot %d, won't move\n", i);

					//if(!hpd_data->round_of_SPF) fprintf(log, "%f,%f; ", sim_position2d[i]->px, sim_position2d[i]->py);
					;
				}
			}
			//XXXX ¿Hace lo mismo en ambos casos?
			// Si no esta roto se mueve
			else
			{
				//printf("Robot %d no está roto.\n",i);
				// INFO: Use adhocSPF if first iteration or subsequent spf round
				if (d_data->nueva_simulacion || hpd_data->round_of_SPF)
				{
					moverse(sim_position2d,d_data->robot_data,i,number_robots,d_data->num_rotos,map_size,obstaculos);

				}
				else
				{
					//fprintf(log, "%f,%f; ", sim_position2d[i]->px, sim_position2d[i]->py);
					int type_motion = 0;

					if(willingness[i] > 0.0)
					{
						type_motion = 1;
						printf("\nMove toward targets\n");
						move_uni_dynamics(sim_position2d,d_data->robot_data, t_data, ar_data,i,number_robots,d_data->num_rotos,map_size,obstaculos, type_motion, d_data->FinalAP);
					}
					else if(willingness[i] < w_H[i])
					{
						type_motion = 2;
						printf("\nMove toward targets - Hysteresis\n");
						move_uni_dynamics(sim_position2d,d_data->robot_data, t_data, ar_data,i,number_robots,d_data->num_rotos,map_size,obstaculos, type_motion, d_data->FinalAP);
					}

				}
			}

			//Update positions and calculate distance walked
			player_pose2d_t new_pose;
			new_pose.px= sim_position2d[i]->px;
			new_pose.py= sim_position2d[i]->py;
			new_pose.pa= sim_position2d[i]->pa;

			d_data->p_data->distance_walked[i]+=sqrt(pow(d_data->p_data->positions[i].px-new_pose.px,2)+pow(d_data->p_data->positions[i].py-new_pose.py,2));
			d_data->p_data->positions[i]=new_pose;

			//Send broadcast with position (receiver=-1, estrategia=0)
			clearArrays(msj_tx,number_robots);
			sendAdhoc(d_data, i, i, -1, msj_tx, 0);
			//sendAdhoc(d_data, i, i, -1, msj_tx, 0); //XXX

			//Print graphics over robots
			dibuja(d_data->robot_data, sim_position2d, sim_graphics2d, i, number_robots, d_data->tipo_red,d_data->algoritmo);

			//sleep(0.4);
		}

		//Calculo potencia media:
		if(!contador)
		{
			if(n<1000)
			{

				system("clear");
				d_data->p_data->net_mean_power[n]=netMeanPower(d_data,0);
				printf("Potencia media en la red = %lf\n",d_data->p_data->net_mean_power[n]);
			}
			n++;
		}
		contador=(contador+1)%NET_MEAN_POWER_ITERATIONS;

		//Miro si ha terminado el algoritmo
		stop=1;
		stopped=number_robots;
		for(i=0;i<number_robots;i++)
		{
			//If there is a robot moving and it's not broken, go on
			if(d_data->robot_data[i].stopped==0&&d_data->robot_data[i].broken!=1)
			{
				stop=0;
				stopped--;
			}
		}

		//Parada forzada cuando la red se estanca pero los nodos se siguen moviendo un poco....
		float stopping_condition = 0;
		if (hpd_data->round_of_SPF || d_data->nueva_simulacion) stopping_condition = 0.92*number_robots;
		else stopping_condition = 1.0*number_robots;

		printf("\n Stopped %d, stopping condition %f", stopped, stopping_condition);

		if (stopped>=stopping_condition)
		{

	//			if (LastStopped!=stopped)
	//			{	//Si ha cambiado el numero de robots parados, retomamos el tiempo
	//				LastTime=time(NULL);
	//				LastStopped=stopped;
	//			}
			if (difftime(time(NULL),LastTime)>(120.0*(number_robots-stopped)))
			{	//Esperamos 2 minutos por robot no parado.
				stop=1;
			}
		}
		else
		{
			LastTime=time(NULL);
			//LastStopped=stopped;
		}


		if(kbhit())
		{
			comienzo_pausa=time(NULL);

			reader=-1;
			salir=0;

			for(i=0;i<number_robots;i++)
			{
				playerc_position2d_set_cmd_vel(sim_position2d[i],0,0,0,0);
			}

			while(salir==0)
			{
				system("clear");
				printf ("------------------  MENU DESPLIEGUE  ------------------\n");
				printf("Acciones posibles durante el despliegue:\n");
				printf("0) Reanudar simulación.\n");
				printf("1) Romper/Arreglar un spider.\n");
				printf("2) Terminar despliegue.\n");

				fflush(stdin);
				scanf("%d",&reader);

				while(reader<0||reader>3)
				{
					printf("Introduzca una de esas opciones, por favor...\n");
					fflush(stdin);
					scanf("%d",&reader);
				}
				switch(reader)
				{
					case 0:
						salir=1;
						break;

					case 1:
						printf("Introduce el número del spider a modificar\n");
						fflush(stdin);
						scanf("%d",&reader);
						changeBroken(sim_position2d,d_data->robot_data,reader,number_robots);
						salir=1;
						break;

					case 2:
						final_pausa=time(NULL);
						pause_time+=difftime(final_pausa,comienzo_pausa);
						final=time(NULL);
						d_data->p_data->depl_time=difftime(final,comienzo)-pause_time;
						free(msj_tx);
						d_data->p_data->num_iterations=n*NET_MEAN_POWER_ITERATIONS;
						return d_data;
				}
			}

			printf("Para pausar la simulación y mostrar el menu de simulación pulse enter\n");

			final_pausa=time(NULL);
			pause_time+=difftime(final_pausa,comienzo_pausa);
		}
	}

	final=time(NULL);
	d_data->p_data->depl_time=difftime(final,comienzo)-pause_time;

	// INFO: logging below
	if(!d_data->nueva_simulacion && !hpd_data->round_of_SPF)
		{
			for(int ty = 0; ty < number_robots; ty++)
			{
				fprintf(log, "%f,%f; ", sim_position2d[ty]->px, sim_position2d[ty]->py);
			}
			fprintf(log, "#");
			fprintf(log, "\n");
			//write to log the locations of the tasks
			for(int ty = 0; ty < t_data->num_tasks; ty++)
			{
				if(t_data->task_ids[ty]>=0)
				{
					fprintf(log, "%d %f %f#", t_data->task_ids[ty]+1, t_data->xpos[ty], t_data->ypos[ty]);
				}
				else
				{
					fprintf(log, "%d %f %f#", ty+1, -10.0, -10.0);
				}
			}
			fprintf(log, "|\n");
		}
	if (hpd_data->round_of_SPF) fprintf(log, "\n");
	fclose(log);

	if (hpd_data->round_of_SPF) // INFO: if a spf round is finished 'redefine' tasks, and robot task assignement
	{
		hpd_data->round_of_SPF = 0;
		t_data = location2cover(t_data, number_robots, sim_position2d, rotos);
		ar_data = initial_assignment(ar_data, t_data, number_robots, rotos);
		printf("\nLocations to cover are updated after the SPF round\n");
	}

	if(negotiation_concluded > 0) // INFO: if there is at least one concluded negotiation start the SPF
	{
		printf("\nNegotiations concluded on this round %d\n", negotiation_concluded);
		negotiation_concluded = 0;
		hpd_data->round_of_SPF = 1;
	}
	//Aqui acaba el despliegue por lo que parece ---> determinar que nodos mueren y lanzar otro...
	if (d_data->nueva_simulacion)
	{
		t_data = location2cover(t_data, number_robots, sim_position2d, rotos);
		ar_data = initial_assignment(ar_data, t_data, number_robots, rotos);
		printf("\nLocations to cover are created and assignment is complete. Agents negotiate their positioning with respect to these\n");
	}

	printf("El algoritmo de despliegue ha terminado.\n",i);

	sleep(2);

	d_data->p_data->num_iterations=n*NET_MEAN_POWER_ITERATIONS;
	free(msj_tx);

return d_data;

}
