/*---------------------------------------------------------------------------
  *  Proyecto Fin de Carrera: Enjambre de microbots para despliegue autónomo de
 *                           una red inalámbrica mallada.
 * 
 *  Francisco José Aguilera Leal
 *  Universidad de Málaga
 * --------------------------------------------------------------------------- 
 * Despliegue de red jerarquizada con Access Point. 
 * Uso de los campos de potencial social (SPF).
 * ---------------------------------------------------------------------------
*/

#include "APjer.h"

/* ---------------------------------------------------------------------------
 * chooseAP - Elige que robot debe ser el punto de acceso.
 * --------------------------------------------------------------------------- */

//XXXX. La quito como estatica para escoger un AP en el modelo SPF no jerarquico


int chooseAP( playerc_position2d_t** sim_position2d, robot_data_t* robot_data, int number_robots, int old_AP, int no_choose)
{
	int i;
	int j;
	int nodes_in_range;

	int AP=-1;

	//Escojo de AP el que mas robots tenga a su alcance (siempre y cuando este a menos de 1 m de la estación base (en 0,0)
	int most_nodes_in_range=0;
	for(i=0;i<number_robots;i++)
	{
		if(robot_data[i].broken==0 && i!=no_choose)
		{
			//Primero compruebo que esta a menos de una cierta distancia de la estación base y luego si es el AP óptimo
			double mpx=sim_position2d[i]->px;
			double mpy=sim_position2d[i]->py;

			double distance=sqrt(mpx*mpx+mpy*mpy);
			
			if(distance <= 1)
			{	
				nodes_in_range=0;
				for(j=0;j<number_robots;j++)
				{

					if(calculateDistance(sim_position2d, j, i) < CHOOSE_AP_DIST && i!=j)
					{
						nodes_in_range++;
					} 
				}
				if(nodes_in_range>most_nodes_in_range)
				{
					most_nodes_in_range=nodes_in_range;
					AP=i;
				}
			}
		}
	}
	if(AP!=-1)
	{
		return AP;
	}	
	else
	{

		return old_AP;
	}
}

/* ---------------------------------------------------------------------------
 * changeLevel -  Cambia el nivel de un robot. 
 * --------------------------------------------------------------------------- */
static void changeLevel(playerc_position2d_t** sim_position2d, robot_data_t* robot_data, int my_robot, int new_level,int number_robots,  int* AP)
{
	int i,cancelar=0;
	if(my_robot>=0 && my_robot<number_robots && new_level>=0 && new_level<3 && robot_data[my_robot].level!=new_level && robot_data[my_robot].broken!=1)
	{
		if(robot_data[my_robot].level!=new_level || new_level==0)
		{
			// Si era nivel 2:
			if(robot_data[my_robot].level==2)
			{
				// Si es nivel 2, buscar AP óptimo y hacerlo AP	
				if(chooseAP(sim_position2d,robot_data,number_robots,*AP,my_robot)!=*AP)
				{	
					*AP=chooseAP(sim_position2d,robot_data,number_robots,*AP,my_robot);			
					robot_data[*AP].level=2;
					robot_data[*AP].num_registered=robot_data[my_robot].num_registered;
					robot_data[*AP].registered_to=*AP;
					robot_data[*AP].stopped=1;


					//XXXX. No habria que mirar si estamos en rango???
					//Cambio el registro de los level 1 al nuevo AP
					for(i=0;i<number_robots;i++)
					{
						if(robot_data[i].level==1)
						{
							robot_data[i].registered_to=*AP;
							//XXXX. No habria que resetear num_registered por si acaso???
							//XXXX si el nuevo AP fuese level 1 no habria que quitar sus nodos nivel 0?
							//XXXX. Respuesta a esto ultimo, parece que los propios nodos de nivel 0 se encargan de ello
						}
					}
				}
				else
				{
					cancelar=1;
				}
			}

			// Si era nivel 1:
			if(robot_data[my_robot].level==1)
			{
				// Si pasa a nivel 0, desregristrar todos sus nivel 0 registrados y bajar numregisted en AP
				for(i=0;i<number_robots;i++)
				{
					if(robot_data[i].registered_to==my_robot)
					{
						robot_data[i].registered_to=-1;
						robot_data[i].stopped=0;
					}
				}

				// Descontar uno a num_registered del AP
				robot_data[*AP].num_registered--;
			}

			// Si era nivel 0 registrado:
			else if(robot_data[my_robot].registered_to!=-1)
			{ 
				robot_data[robot_data[my_robot].registered_to].num_registered--;
			}

			// Si pasa a ser AP, quitar el otro AP y copiar sus cosas.
			if(new_level==2)
			{
				robot_data[my_robot].num_registered=robot_data[*AP].num_registered;
				robot_data[*AP].level=0;
				robot_data[*AP].registered_to=-1;
				robot_data[*AP].num_registered=0;
				robot_data[my_robot].stopped=1;
				robot_data[my_robot].registered_to=*AP;
				*AP=my_robot;

				//Cambio el registro de los level 1 al nuevo AP
				//XXXX: Esto ya lo ha hecho antes, no?
				//No deberia comprobar el rango???
				//¿No habria que quitar sus niveles 0?
				//XXXX. Respuesta a esto ultimo, parece que los propios nodos de nivel 0 se encargan de engancharse a un nivel 1 cercano en cada iteracion
				for(i=0;i<number_robots;i++)
				{
					if(robot_data[i].level==1)
					{
						robot_data[i].registered_to=*AP;
					}
				}
			}

			if(cancelar!=1)
			{
				//Si pasa a nivel 1:
				if(new_level==1)
				{
					robot_data[*AP].num_registered++;
					robot_data[my_robot].registered_to=*AP;
					robot_data[my_robot].num_registered=0;
					robot_data[my_robot].stopped=0;
				}

				if(new_level==0)
				{
					robot_data[my_robot].registered_to=-1;
					robot_data[my_robot].num_registered=0;
					robot_data[my_robot].stopped=0;
				}

				// Finalmente cambio nivel
				robot_data[my_robot].level=new_level;
			}
		}
	}
}

/* ---------------------------------------------------------------------------
 * changeBroken - Modifica el estado de un robot (roto o funcionando).
 * --------------------------------------------------------------------------- */
void changeBrokenJer(playerc_position2d_t** sim_position2d, robot_data_t* robot_data, int my_robot, int number_robots,int* AP)
{
	int i;
	if(my_robot>=0 && my_robot < number_robots)
	{
		if(robot_data[my_robot].broken==0)
		{
			if(robot_data[my_robot].level>0)
			{	//XXXX: Pa que comprueba nada si hace lo mismo en los dos casos???
				changeLevel(sim_position2d,robot_data,my_robot,0,number_robots,AP);
			}
			else
			{
				changeLevel(sim_position2d,robot_data,my_robot,0,number_robots,AP);
			}
			robot_data[my_robot].broken=1;
			robot_data[my_robot].stopped=1;
		}
		else
		{
			robot_data[my_robot].broken=0;
			robot_data[my_robot].stopped=0;
		}
	}
}

//Rompe un nodo sí o si
void BreakJer(playerc_position2d_t** sim_position2d, robot_data_t* robot_data, int my_robot, int number_robots,int* AP)
{
	int i;
	if(my_robot>=0 && my_robot < number_robots)
	{
		if(robot_data[my_robot].broken==0)
		{
			if(robot_data[my_robot].level>0)
			{	//XXXX: Pa que comprueba nada si hace lo mismo en los dos casos???
				changeLevel(sim_position2d,robot_data,my_robot,0,number_robots,AP);
			}
			else
			{
				changeLevel(sim_position2d,robot_data,my_robot,0,number_robots,AP);
			}
			robot_data[my_robot].broken=1;
			robot_data[my_robot].stopped=1;
		}
//XXXX: No hay else que valga
//		else
//		{
//			robot_data[my_robot].broken=0;
//			robot_data[my_robot].stopped=0;
//		}
	}
}

/* ---------------------------------------------------------------------------
 * joinLevel1 - Comprueba si el robot debe convertirse en nivel 1
 * --------------------------------------------------------------------------- */
static int joinLevel1(playerc_position2d_t** sim_position2d, robot_data_t* robot_data, int my_robot, int number_robots)
{
	int AP=0;
	int num_level_1=0;
	
	int i;
	
	//Miro si faltan nodos de nivel 1
	for (i=0;i<number_robots;i++)
	{
		if(robot_data[i].level==1)
		{
			num_level_1++;
		}
		else if(robot_data[i].level==2)
		{
			AP=i;
		}
	}

	if(num_level_1>=number_robots/4+1)
	{
		return 0;
	}

	if (robot_data[my_robot].low_battery)
	{	//XXXX. Si tiene bateria baja, no lo elijo (modo jerarquico 2)
		//Cuando no se simila esta variante, low_battery siempre vale 0 en todos los robots
		return 0;
	}

	//Miro si esta en rango del AP
	if(calculateDistance(sim_position2d, my_robot, AP)>MAX_AP_DIST)
	{
		return 0;
	}

	//Miro que no tenga ningun otro nodo level 1 cerca
	for (i=0;i<number_robots;i++)
	{
		if(robot_data[i].level==1 && i!=my_robot)
		{
			if( calculateDistance(sim_position2d, my_robot, i) < MIN_L1L1_DIST ) //Demasiado cerca
			{
				return 0;
			}
		}
	}

	return 1; //Cumple las condiciones
}

/* ---------------------------------------------------------------------------
 * registering - Comprueba si el robot debe registrarse en uno de nivel 1
 * //XXXX. cuando el padre pasa de 1 a 2, la llamada a esta funcion provoca que el hijo se reenganche
 * --------------------------------------------------------------------------- */
static int registering(playerc_position2d_t** sim_position2d, robot_data_t* robot_data, int my_robot, int number_robots)
{
	int i;

	//Miro si hay algun level 1 al alcance que tenga huecos libres
	for (i=0;i<number_robots;i++)
	{
		if(i!=my_robot && robot_data[i].level==1)
		{
			if( (robot_data[i].num_registered < MAX_REGISTERED) && (calculateDistance(sim_position2d, my_robot, i)<MAX_L1L0_DIST) )
			{
				//Si no estoy registrado en ninguno, me registro.
				if(robot_data[my_robot].registered_to==-1)
				{
					robot_data[my_robot].registered_to=i;
					robot_data[i].num_registered++;
					return 1;
				}
				//Si ya estoy registrado en otro pero este esta mas cerca, me cambio. (Necesario superar un umbral) 
				else if(calculateDistance(sim_position2d, my_robot, i) + MIN_DIFF_L0L1_DIST < calculateDistance(sim_position2d, my_robot, robot_data[my_robot].registered_to))			
				{
					robot_data[robot_data[my_robot].registered_to].num_registered--;
					robot_data[my_robot].registered_to=i;
					robot_data[i].num_registered++;
					return 1;
				}
			}
		}
	}

	return 0;
}

/* ---------------------------------------------------------------------------
 * potentialLaw - Calcula la fuerza
 * --------------------------------------------------------------------------- */
static double potentialLaw(double distance, double c1, double c2, double sigma1, double sigma2, double xoff, double yoff)
{
	double f = -c1/pow((distance+xoff),sigma1) + c2/pow((distance+xoff),sigma2) + yoff;
	return f;
}

/* ---------------------------------------------------------------------------
 * vectorTotal - Calcula el vector de fuerza ejercido por la red sobre
 *		 el robot
   --------------------------------------------------------------------------- */
static void vectorTotal(playerc_position2d_t** sim_position2d, robot_data_t* robot_data, int number_robots, int map_size, int i, double* x, double* y,int obstaculos)
{
	int j;
	double f,d;
	double c1,c2,sigma1,sigma2,xoff,yoff;

	int no_moverse=0;
	
	*x=0;
	*y=0;
	//Debe de ver a que grupo pertenece: roto, AP, nivel1, nivel0, nivel0libre

	//1) Esta roto?
	if(robot_data[i].broken==1)
	{
		// De momento estoy quieto. (Luego lo movere hacia abajo)
		no_moverse=1;
	}

	//1) Es el AP? 
	else if(robot_data[i].level==2)
	{
		for(j=0;j<number_robots;j++) 
		{	
			if(i!=j)
			{
				f=0;
				d=calculateDistance(sim_position2d, j, i);
				// Repulsion hasta 0.4 m
				c1=0.01;
				c2=0;
				sigma1=8;
				sigma2=1;// Da igual este termino se anula
				xoff=0;
				yoff=0;
				f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
				if(abs(f)>UMBRAL_MOVIMIENTO)
				{
					no_moverse=1; //XXXX. ¿Es esto correcto????
					//En la practica como se comprueba que mayor y menor que umbral de movimiento, en efecto no se mueve
					//pero no hacia falta complicarse tanto para que el AP no se moviese...
				}
			}
		}
	}
	//Es level 1?
	else if(robot_data[i].level==1)
	{
		for(j=0;j<number_robots;j++) 
		{	
			if(i!=j)
			{
				f=0;
				d=calculateDistance(sim_position2d, j, i);
				//Level 2 le repele hasta 2 m
				if(robot_data[j].level==2)
				{
					//Clustering hasta 2
					c1=20;
					c2=6;
					sigma1=2;
					sigma2=0.2; //XXXX. por ahora el único que tiene atraccion....
					xoff=0;
					yoff=0;
					f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);

					//printf("%f\n",(sim_position2d[j]->px - sim_position2d[i]->px)/d);
					//printf("%f\n",f*(sim_position2d[j]->px - sim_position2d[i]->px)/d);
				}
				//Con los otros level 1 se repele hasta 2 m
				else if(robot_data[j].level==1)
				{
					//Repulsion hasta 2
					c1=60;
					c2=0;
					sigma1=7;
					sigma2=1; //Anulado
					xoff=0;
					yoff=0;
					f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
				}
				//Con los level 0 libres se repele hasta 2 m
				else if(robot_data[j].level==0 && robot_data[j].registered_to==-1 && robot_data[j].broken==0)
				{
					//Repulsion hasta 2
					c1=60;
					c2=0;
					sigma1=7;
					sigma2=1; //Anulado
					xoff=0;
					yoff=0;
					f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
				}
				// Para no chocar con el resto
				else
				{
					// Repulsion hasta 0.4 m
					c1=0.001;
					c2=0;
					sigma1=8;
					sigma2=1;// Da igual este termino se anula
					xoff=0;
					yoff=0;
					f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
					//if(abs(f)>UMBRAL_MOVIMIENTO)
					//{
				//		no_moverse=1;
				//	}
				}
				*x=*x+f*(sim_position2d[j]->px - sim_position2d[i]->px)/d; //XXXX ¿hay que dividir por d? ¿No altera esto los exponentes?
				*y=*y+f*(sim_position2d[j]->py - sim_position2d[i]->py)/d;
			}
		}
	}
	
	//Es level 0 registrado?
	else if(robot_data[i].level==0 && robot_data[i].registered_to!=-1)
	{
		for(j=0;j<number_robots;j++) 
		{	
			if(i!=j)
			{
				f=0;
				d=calculateDistance(sim_position2d, j, i);
				if(robot_data[j].level==1 && robot_data[i].registered_to==j) //Le atrae su level 1 hasta d
				{
					//Fuerza de atracción hasta 1 m
					c1=10;
					c2=10;
					sigma1=3;
					sigma2=0.2;
					xoff=0;
					yoff=0;
					f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);	
				}
				// Se mantiene a una cierta distancia de los level 0 registrados
				else if(robot_data[j].level==0 && robot_data[j].registered_to!=-1)
				{
					//Repulsión hasta 1.5 m
					c1=3;
					c2=0;
					sigma1=8;
					sigma2=1; //Anulado
					xoff=0;
					yoff=0;
					f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
				}
				// Para no chocar con el resto
				else
				{
					// Repulsion hasta 0.4 m
					c1=0.001;
					c2=0;
					sigma1=8;
					sigma2=1;// Da igual este termino se anula
					xoff=0;
					yoff=0;
					f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
				}
				*x=*x+f*(sim_position2d[j]->px - sim_position2d[i]->px)/d;
				*y=*y+f*(sim_position2d[j]->py - sim_position2d[i]->py)/d;
			}	
		}
	}
	//Es level 0 libre?
	else if(robot_data[i].level==0)
	{
		double d_next_l1=0;
		int next_l1=-1;
		double d_l1=0;
		for(j=0;j<number_robots;j++) 
		{	
			if(i!=j)
			{
				f=0;
				d=calculateDistance(sim_position2d, j, i);
				// Le atraen el nodo level 1 mas cercano sin completar. (Calculo de cual es)
				if(robot_data[j].level==1 && robot_data[j].num_registered<MAX_REGISTERED) 
				{
					d_l1=d;
					if(d_next_l1>d_l1 || d_next_l1==0)
					{
						d_next_l1=d_l1;	
						next_l1=j;	
					}
				}
				// Le repelen los level1(menos que el AP)
				else if(robot_data[j].level==1 && robot_data[j].num_registered==MAX_REGISTERED)
				{
					//Fuerza de repulsion hasta 2 m
					c1=60;
					c2=0;
					sigma1=7;
					sigma2=1; //Anulado
					xoff=0;
					yoff=0;
					f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
					
				}
				// Se aleja 2 m de los l0 libres
				if(robot_data[j].level==0 && robot_data[j].registered_to==-1 && robot_data[j].broken==0)
				{
					//Fuerza de repulsion hasta 2 m
					c1=60;
					c2=0;
					sigma1=7;
					sigma2=1; //Anulado
					xoff=0;
					yoff=0;
					f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
				}
				// El resto para no chocarse
				else
				{
					// Repulsion hasta 0.4 m
					c1=0.001;
					c2=0;
					sigma1=8;
					sigma2=1;// Da igual este termino se anula
					xoff=0;
					yoff=0;
					f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
				}
				*x=*x+f*(sim_position2d[j]->px - sim_position2d[i]->px)/d;
				*y=*y+f*(sim_position2d[j]->py - sim_position2d[i]->py)/d;
			}
		}

		//Fuerza atractiva hacia el l1 mas cercano 
		if(next_l1!=-1)
		{
			//Fuerza de atracción hasta 1 m
			d=calculateDistance(sim_position2d, next_l1, i);
			c1=10;
			c2=10;
			sigma1=3;
			sigma2=0.2;
			xoff=0;
			yoff=0;
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);// Calcular params para ser atraido hasta una dist
			*x=*x+f*(sim_position2d[next_l1]->px - sim_position2d[i]->px)/d;
			*y=*y+f*(sim_position2d[next_l1]->py - sim_position2d[i]->py)/d;
		}
	}

	// Repulsion de los muros laterales hasta 0.4 m.
	c1=0.001;
	c2=0;
	sigma1=8;
	sigma2=1; // Da igual este termino se anula
	xoff=0;
	yoff=0;

	// Muro norte:
	d=fabs(map_size/2-sim_position2d[i]->py);
	f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
	*y=*y+f*(map_size/2 - sim_position2d[i]->py)/d;

	// Muro sur:	
	d=fabs(-map_size/2-sim_position2d[i]->py);
	f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
	*y=*y+f*(-map_size/2-sim_position2d[i]->py)/d;

	// Muro oeste:
	d=fabs(-map_size/2-sim_position2d[i]->px);
	f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff);
	*x=*x+f*(-map_size/2-sim_position2d[i]->px)/d;

	// Muro este:
	d=fabs(map_size/2-sim_position2d[i]->px);
	f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
	*x=*x+f*(map_size/2 - sim_position2d[i]->px)/d;

	// Escenario de obstaculos 1
	if(obstaculos==1)
	{
		// Repulsion muros interiores hasta 0.4 m
		c1=0.001;
		c2=0;
		sigma1=12;
		sigma2=1;// Da igual este termino se anula
		xoff=0;
		yoff=0;

		// Muro superior
		if(sim_position2d[i]->px>-0.5*map_size/5 && sim_position2d[i]->px<0.5*map_size/5)
		{
			d=fabs(map_size/5-sim_position2d[i]->py);
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*y=*y+f*(map_size/5-sim_position2d[i]->py)/d;
		}
		else if(sim_position2d[i]->px<-0.5*map_size/5)
		{
			d=sqrt(pow(-0.5*map_size/5-sim_position2d[i]->px,2)+pow(map_size/5-sim_position2d[i]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(-0.5*map_size/5-sim_position2d[i]->px)/d;
			*y=*y+f*(map_size/5-sim_position2d[i]->py)/d;
		}
		else if(sim_position2d[i]->px>0.5*map_size/5)
		{
			d=sqrt(pow(0.5*map_size/5-sim_position2d[i]->px,2)+pow(map_size/5-sim_position2d[i]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(0.5*map_size/5-sim_position2d[i]->px)/d;
			*y=*y+f*(map_size/5-sim_position2d[i]->py)/d;
		}

		// Muro central
		if(sim_position2d[i]->py<0 && sim_position2d[i]->py>-0.5*map_size/5)
		{
			d=fabs(0-sim_position2d[i]->px);
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(0-sim_position2d[i]->px)/d;
		}
		else if(sim_position2d[i]->py<-0.5*map_size/5)
		{
			d=sqrt(pow(0-sim_position2d[i]->px,2)+pow(-0.5*map_size/5-sim_position2d[i]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(0-sim_position2d[i]->px)/d;
			*y=*y+f*(-map_size/5-sim_position2d[i]->py)/d;
		}
		else if(sim_position2d[i]->py>0)
		{
			d=sqrt(pow(0-sim_position2d[i]->px,2)+pow(0-sim_position2d[i]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(0-sim_position2d[i]->px)/d;
			*y=*y+f*(0-sim_position2d[i]->py)/d;
		}

		// Muro derecho
		if(sim_position2d[i]->py<-0.5*map_size/5 && sim_position2d[i]->py>-1.5*map_size/5)
		{
			d=fabs(1.5*map_size/5-sim_position2d[i]->px);
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(1.5*map_size/5-sim_position2d[i]->px)/d;
		}
		else if(sim_position2d[i]->py>-0.5*map_size/5)
		{
			d=sqrt(pow(1.5*map_size/5-sim_position2d[i]->px,2)+pow(-0.5*map_size/5-sim_position2d[i]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(1.5*map_size/5-sim_position2d[i]->px)/d;
			*y=*y+f*(-0.5*map_size/5-sim_position2d[i]->py)/d;
		}
		else if(sim_position2d[i]->py<-1.5*map_size/5)
		{
			d=sqrt(pow(1.5*map_size/5-sim_position2d[i]->px,2)+pow(-1.5*map_size/5-sim_position2d[i]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(1.5*map_size/5-sim_position2d[i]->px)/d;
			*y=*y+f*(-1.5*map_size/5-sim_position2d[i]->py)/d;
		}

		// Muro izquierdo
		if(sim_position2d[i]->px>-1.5*map_size/5 && sim_position2d[i]->px<-0.5*map_size/5)
		{
			d=fabs(-1.5*map_size/5-sim_position2d[i]->py);
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*y=*y+f*(-1.5*map_size/5-sim_position2d[i]->py)/d; //XXXX. Aqui ponia 1-5 en lugar de 1.5
		}
		else if(sim_position2d[i]->px<-1.5*map_size/5)
		{
			d=sqrt(pow(-1.5*map_size/5-sim_position2d[i]->px,2)+pow(-1.5*map_size/5-sim_position2d[i]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(-1.5*map_size/5-sim_position2d[i]->px)/d;
			*y=*y+f*(-1.5*map_size/5-sim_position2d[i]->py)/d;
		}
		else if(sim_position2d[i]->px>-0.5*map_size/5)
		{
			d=sqrt(pow(-0.5*map_size/5-sim_position2d[i]->px,2)+pow(-1.5*map_size/5-sim_position2d[i]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(-0.5*map_size/5-sim_position2d[i]->px)/d;
			*y=*y+f*(-1.5*map_size/5-sim_position2d[i]->py)/d;
		}
	}	

	// Escenario de obstaculos 2
	if(obstaculos==2)
	{
		// Repulsion muros interiores hasta 0.4 m
		c1=0.001;
		c2=0;
		sigma1=12;
		sigma2=1;// Da igual este termino se anula
		xoff=0;
		yoff=0;

		// Muro superior izquierdo
		if(sim_position2d[i]->py<1.5*map_size/5 && sim_position2d[i]->py>map_size/5)
		{
			d=fabs(-map_size/5-sim_position2d[i]->px);
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(-map_size/5-sim_position2d[i]->px)/d;
		}
		else if(sim_position2d[i]->py>1.5*map_size/5)
		{
			d=sqrt(pow(-map_size/5-sim_position2d[i]->px,2)+pow(1.5*map_size/5-sim_position2d[i]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(-map_size/5-sim_position2d[i]->px)/d;
			*y=*y+f*(1.5*map_size/5-sim_position2d[i]->py)/d;
		}
		else if(sim_position2d[i]->py<map_size/5)
		{
			d=sqrt(pow(-map_size/5-sim_position2d[i]->px,2)+pow(map_size/5-sim_position2d[i]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(-map_size/5-sim_position2d[i]->px)/d;
			*y=*y+f*(map_size/5-sim_position2d[i]->py)/d;
		}

		// Muro derecho
		if(sim_position2d[i]->px>0 && sim_position2d[i]->px<map_size/5)
		{
			d=fabs(0-sim_position2d[i]->py);
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*y=*y+f*(0-sim_position2d[i]->py)/d;
		}
		else if(sim_position2d[i]->px<0)
		{
			d=sqrt(pow(0-sim_position2d[i]->px,2)+pow(0-sim_position2d[i]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(0-sim_position2d[i]->px)/d;
			*y=*y+f*(0-sim_position2d[i]->py)/d;
		}
		else if(sim_position2d[i]->px>map_size/5)
		{
			d=sqrt(pow(map_size/5-sim_position2d[i]->px,2)+pow(0-sim_position2d[i]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(map_size/5-sim_position2d[i]->px)/d;
			*y=*y+f*(0-sim_position2d[i]->py)/d;
		}

		// Muro inferior izquierdo
		if(sim_position2d[i]->py>-1.5*map_size/5 && sim_position2d[i]->py<-map_size/5)
		{
			d=fabs(-map_size/5-sim_position2d[i]->px);
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(-map_size/5-sim_position2d[i]->px)/d;
		}
		else if(sim_position2d[i]->py<-1.5*map_size/5)
		{
			d=sqrt(pow(-map_size/5-sim_position2d[i]->px,2)+pow(-1.5*map_size/5-sim_position2d[i]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(-map_size/5-sim_position2d[i]->px)/d;
			*y=*y+f*(-1.5*map_size/5-sim_position2d[i]->py)/d;
		}
		else if(sim_position2d[i]->py>-map_size/5)
		{
			d=sqrt(pow(-map_size/5-sim_position2d[i]->px,2)+pow(-map_size/5-sim_position2d[i]->py,2));
			f=potentialLaw(d,c1,c2,sigma1,sigma2,xoff,yoff); 
			*x=*x+f*(-map_size/5-sim_position2d[i]->px)/d;
			*y=*y+f*(-map_size/5-sim_position2d[i]->py)/d;
		}
	}

	if(no_moverse)
	{
		*x=0;
		*y=0;
	}
}

/* ---------------------------------------------------------------------------
 * moverse - Maneja el movimiento del robot
 * --------------------------------------------------------------------------- */
static void moverse(playerc_position2d_t** sim_position2d, robot_data_t* robot_data, int my_robot, int number_robots, int map_size,int obstaculos)
{	
	double x;
	double y;
	double modulo;
	double angulo;
	double my_angle=sim_position2d[my_robot]->pa;


	//Calcula el vector resultante de todas las fuerzas
	vectorTotal(sim_position2d,robot_data,number_robots,map_size,my_robot,&x,&y,obstaculos);	
		
	//Solo si el modulo de ese vector es mayor que un cierto valor, muevo al robot
	modulo=sqrt(pow(x,2)+pow(y,2));
	//printf("modulo=%f\n",modulo);
		//printf("xtotal(%d)=%f\n",my_robot,x);
		//printf("ytotal(%d)=%f\n",my_robot,y);
	if(modulo<UMBRAL_MOVIMIENTO)
	{
		playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,0,0);
		playerc_position2d_set_cmd_car(sim_position2d[my_robot],0,0);

		//XXXX: Faltaba aqui????...
		robot_data[my_robot].stopped=1;

	}
	else
	{

		robot_data[my_robot].stopped=0; //XXXX. Faltaba aqui!!!

		//Calcula el ángulo
		angulo = atan2(y/modulo,x/modulo); //Devuelve un valor entre -pi/2 y pi/2

		//Moverse en esa direccion
		if(fabs(my_angle-angulo) > 0.1)
		{
			playerc_position2d_set_cmd_car(sim_position2d[my_robot],0,0);

			if(angulo>0)
			{
				if(my_angle<0)
				{
					if (my_angle+3.1415<angulo)
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,-0.5,0); //giro agujas relog
					}
					else
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,0.5,0); //giro contrario 
					}
				}
				else
				{
					if (my_angle>angulo)
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,-0.5,0); //giro agujas relog
					}
					else
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,0.5,0); //giro contrario 
					}
				}
			}
			else
			{
				if(my_angle>0)
				{
					if (my_angle-3.1415<angulo)
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,-0.5,0); //giro agujas relog
					}
					else
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,0.5,0);//giro contrario 
					}
				}
				else
				{
					if (my_angle>angulo)
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,-0.5,0); //giro agujas relog
					}
					else
					{
						playerc_position2d_set_cmd_vel(sim_position2d[my_robot],0,0,0.5,0);//giro contrario 
					}
				}
			}
		}
		else
		{
			playerc_position2d_set_cmd_car(sim_position2d[my_robot],0.05,0);
		}
	}
}		

/* ---------------------------------------------------------------------------
 * ---------------------------------------------------------------------------
 *
 * simAPjer - Función principal que controla el despliegue.
 *
 * Parametros:
 *		d_data: Datos sobre el despliegue de la red
 *		rotos: Arrays de robots rotos al comienzo
 *		obstaculos: Referencia para los obstaculos del mundo
 *		sim_robots: Proxys de los robots
 *		sim_position2d: Proxys a sus interfaces de posicion
 *		simulation: Proxy a la interfaz simulacion
 *		sim_graphics2d: Proxys a sus interfaces de gráficos
 * 
 * --------------------------------------------------------------------------- 
 * --------------------------------------------------------------------------- */
deployment_data_t* simAPJer(deployment_data_t* d_data,int* rotos,int obstaculos,playerc_client_t **sim_robots, playerc_position2d_t** sim_position2d, playerc_simulation_t** simulation, playerc_graphics2d_t** sim_graphics2d,playerc_sonar_t** sim_sonar)
{
	//Declaro variables 
	int i,contador=0,stop=0,n=0,stopped=0,LastStopped=0;;
	srand(time(NULL));

	double mean_power;

	int reader,salir,level;

	int number_robots=d_data->number_robots;
	int map_size=d_data->map_size;

	double* msj_tx=(double*) malloc(sizeof(double) * number_robots);

	//Inicializo variables
	for(i=0;i<number_robots;i++)
	{
		if(d_data->nueva_simulacion)
		{
			//Variables algoritmo
			d_data->robot_data[i].turn=0;
			d_data->robot_data[i].turning_count=0;
			d_data->robot_data[i].walking_count=0;
			d_data->robot_data[i].backbone=0;
			d_data->robot_data[i].stopped=0;
			d_data->robot_data[i].low_battery=0;
			d_data->robot_data[i].level=0;
			d_data->robot_data[i].registered_to=-1;
			d_data->robot_data[i].num_registered=0;
			d_data->robot_data[i].remaining_battery=ROBOT_INITIAL_BATTERY;
			d_data->FinalAP=-1;

			d_data->p_data->distance_walked[i]=0; //XXXX. Movido aqui

			if(rotos!=NULL)
			{
				d_data->robot_data[i].broken|=rotos[i];
			}
			else
			{
			d_data->robot_data[i].broken|=0;
			}
		}
		else
		{ //XXXX. modificado, de forma que se traten correctamente los nodos eliminados...
			if(rotos!=NULL)
			{
				//d_data->robot_data[i].broken|=rotos[i];
				if (rotos[i])
				{
					BreakJer(sim_position2d,d_data->robot_data,i,number_robots,&(d_data->FinalAP));
				}
				else if ((d_data->robot_data[i].low_battery)&&(d_data->robot_data[i].level==1))
				{	//XXXX. Si era router y baja a nivel critico de bateria....
					changeLevel(sim_position2d,d_data->robot_data,i,0,number_robots,&(d_data->FinalAP));

				}
			}
//			else
//			{
//				d_data->robot_data[i].broken|=0;
//			}
		}

		//Variables para medir rendimiento
		//d_data->p_data->distance_walked[i]=0; //XXXX. No!!, solo reset si nueva...
		d_data->p_data->packets_sent[i]=0;
		d_data->p_data->own_packets_sent[i]=0;
		d_data->p_data->packets_rx[i]=0;
		d_data->p_data->own_packets_rx[i]=0;

	}

	//Robots tell where they are, pose and odometrics and move a bit 
	for(i=0;i<number_robots;i++) 
	{	
		if (d_data->nueva_simulacion)
		{	//XXXX ¿Era necesario hacerlo si no es nueva?
			//Entonces, no aplicarlo a algunos nodos...
			playerc_position2d_set_cmd_car(sim_position2d[i],0.005,0);
			playerc_client_read(*sim_robots); //XXXX. Esto lo hace en SPF y no salen tantos warnings
		}
		char spider[10];
		sprintf(spider,"spider%d",i+1);

		double px,py,pa;	
		playerc_simulation_get_pose2d(*simulation, spider, &px,&py,&pa);
		playerc_client_read(*sim_robots); //XXXX. Esto lo hace en SPF y no salen tantos warnings
		//printf("Robot  %d empieza en: %f %f %f\n", i,px,py,pa);

		d_data->p_data->positions[i].px=px;
		d_data->p_data->positions[i].py=py;
		d_data->p_data->positions[i].pa=pa;
	}
	
	sleep(1);

	for(i=0;i<number_robots;i++)
	{
		playerc_position2d_set_cmd_car(sim_position2d[i],0,0);
		if(i%10==0)
		{
			playerc_client_read(*sim_robots); //XXXX. Esto lo hace en SPF y no salen tantos warnings
		}
	}

	//Elijo un AP	
	int AP;
	int newAP;

	if(d_data->nueva_simulacion)
	{
		playerc_client_read(*sim_robots);	//XXXX. Para que ??
		AP=chooseAP(sim_position2d, d_data->robot_data,number_robots,0,-1);
		d_data->robot_data[AP].level=2;
		d_data->robot_data[AP].stopped=1;
		d_data->robot_data[AP].registered_to=AP;
	}
	else
	{
		for(i=0;i<number_robots;i++)
		{
			if(d_data->robot_data[i].level==2)
			{
				AP=i;
			}

		}
		//XXXX. Lo he guardado de antes, comprobar....
		// Se supone que al matar nodos se ha podido actualizar el AP.
		if (AP!=d_data->FinalAP)
		{
			printf("Houston, tenemos un problema...AP!=FinalAP\n");
			while (1);
		}

	}

 	/* COMIENZO DEL DESPLIEGUE */

	time_t comienzo=time(NULL);
	time_t LastTime=time(NULL);
	time_t final,comienzo_pausa,final_pausa;
	double pause_time=0;

	//system("reset");
	printf("Para pausar la simulación y mostrar el menu de simulación pulse enter\n");

	//Start Algorithm
	while(stop!=1)
	{

		//Wait for new data from server
		playerc_client_read(*sim_robots);
		
		for(i=0;i<number_robots;i++)
		{

			//XXXX ¿¿?? Que se comprueba realmente aqui si hace lo mismo en todos los casos??

			//0)Si esta roto no hace nada. (Lo mismo me lo llevo hacia abajo...)
			if(d_data->robot_data[i].broken==1)
			{
				//printf("Robot %d está roto.\n",i);
				moverse(sim_position2d,d_data->robot_data,i,number_robots,map_size,obstaculos);
			}

			//1) Es el AP? APbehaviour (o puedo intentar que se vaya hacia el centro)
			else if(d_data->robot_data[i].level==2)
			{
				//printf("Robot %d es el AP.\n",i);
				moverse(sim_position2d,d_data->robot_data,i,number_robots,map_size,obstaculos);
			}

			//2) Es level 1? Level1Behaviour
			else if(d_data->robot_data[i].level==1)
			{
				//printf("Robot %d es level 1.\n",i);
				moverse(sim_position2d,d_data->robot_data,i,number_robots,map_size,obstaculos);
			}

			//3) Es level 0? 
			else if(d_data->robot_data[i].level==0)
			{
				//printf("Robot %d es level 0.\n",i);
				// Esta en rango del AP? Si lo esta, y num_level_1<num_robots/4 se hace level1. (si registrado se desregistra)
				if(joinLevel1(sim_position2d, d_data->robot_data, i, number_robots))				{
					//printf("Robot %d se convierte en level 1.\n",i);
					changeLevel(sim_position2d,d_data->robot_data,i,1,number_robots,&AP);
				}
				
				// Si no lo esta: Esta registrado? level 0 registrado behaviour 
				else if(d_data->robot_data[i].registered_to!=-1)
				{
					//printf("Robot %d esta registrado en %d.\n",i,d_data->robot_data[i].registered_to);
					if(registering(sim_position2d, d_data->robot_data, i, number_robots))
					{
						//printf("Robot %d se cambia a %d.\n",i,d_data->robot_data[i].registered_to);
					}
					moverse(sim_position2d,d_data->robot_data,i,number_robots,map_size,obstaculos);
				}
				
				// Si tampoco lo esta y esta en rango de un level1 que no tenga mas de X robots registrados? Se registra.
				else if(registering(sim_position2d, d_data->robot_data, i, number_robots))
				{
					//Me registro y reseteo la backbone y todas las demas mierdas.
					moverse(sim_position2d,d_data->robot_data,i,number_robots,map_size,obstaculos);
					//printf("Robot %d se registra en %d.\n",i,d_data->robot_data[i].registered_to);
				}
				
				// Si no esta en rango de ninguno se mueve libremente.
				else
				{
					moverse(sim_position2d,d_data->robot_data,i,number_robots,map_size,obstaculos);
				}		
			}

	
			//Update positions and calculate distance walked
			player_pose2d_t new_pose;
			new_pose.px= sim_position2d[i]->px;
			new_pose.py= sim_position2d[i]->py;
			new_pose.pa= sim_position2d[i]->pa;

			d_data->p_data->distance_walked[i]+=sqrt(pow(d_data->p_data->positions[i].px-new_pose.px,2)+pow(d_data->p_data->positions[i].py-new_pose.py,2));
			d_data->p_data->positions[i]=new_pose;

			//If l1 or l0 registered, send position to AP (receiver=AP, estrategia=0)
			clearArrays(msj_tx,number_robots);
			if(d_data->robot_data[i].level==1 || (d_data->robot_data[i].level==0 && d_data->robot_data[i].registered_to!=-1))
			{			
				sendAPJerq(d_data, i, i, AP, msj_tx, 0);
			}
			//Else send broadcast with position (receiver=-1, estrategia=0)
			else
			{
				sendAdhoc(d_data, i, i, -1, msj_tx, 0);
			}

			//Print graphics over robots			
			dibuja(d_data->robot_data, sim_position2d, sim_graphics2d, i, number_robots, d_data->tipo_red,d_data->algoritmo);
			
		}

		//Cada 50 iteraciones recalculo cual es el AP óptimo y si es otro lo cambio.
		/*if(contador==49)
		{
			newAP=chooseAP(sim_position2d, d_data->robot_data, number_robots,AP,-1);
			if(newAP!=AP)
			{
				changeLevel(sim_position2d,d_data->robot_data,newAP,2,number_robots,&AP);
			}	
		}*/

		//Miro si ha terminado el algoritmo
		stop=1;
		stopped=number_robots;
		for(i=0;i<number_robots;i++)
		{
			//If there is a robot moving and it's not broken, go on
			if(d_data->robot_data[i].stopped==0&&d_data->robot_data[i].broken!=1)
			{
				stop=0;
				stopped--;
			}
		}
		
		//Parada cuando la red se estanca pero los nodos se siguen moviendo un poco....
		if (stopped>=0.90*number_robots)
		{

//			if (LastStopped!=stopped)
//			{	//Si ha cambiado el numero de robots parados, retomamos el tiempo
//				LastTime=time(NULL);
//				LastStopped=stopped;
//			}
			if (difftime(time(NULL),LastTime)>(120.0*(number_robots-stopped)))
			{	//Esperamos 2 minutos por robot no parado.
				stop=1;
			}
		}
		else
		{
			LastTime=time(NULL);
			//LastStopped=stopped;
		}


		//Calculo potencia media:
		if(!contador)
		{
			if(n<1000)
			{
				system("clear");
				//mean_power=meanPower(d_data,d_data->tipo_red,1);
				//printf("Potencia media de %d = %lf\n",1,mean_power);
				d_data->p_data->net_mean_power[n]=netMeanPower(d_data,0);
				//printf("Potencia media en la red = %lf\n",d_data->p_data->net_mean_power[n]);
			}
			n++;
		}
		contador=(contador+1)%NET_MEAN_POWER_ITERATIONS;
			
		if(kbhit())
		{
			comienzo_pausa=time(NULL);

			reader=-1;
			salir=0;

			for(i=0;i<number_robots;i++)
			{
				playerc_position2d_set_cmd_vel(sim_position2d[i],0,0,0,0);
			}

			while(salir==0)
			{
				system("clear");
				printf ("------------------  MENU DESPLIEGUE  ------------------\n");
				printf("Acciones posibles durante el despliegue:\n");
				printf("0) Reanudar simulación.\n");
				printf("1) Modificar el nivel de un spider.\n");
				printf("2) Romper/Arreglar un spider.\n");
				printf("3) Terminar despliegue.\n");

				fflush(stdin);
				scanf("%d",&reader);
			
				while(reader<0||reader>3)
				{
					printf("Introduzca una de esas opciones, por favor...\n");
					fflush(stdin);
					scanf("%d",&reader);
				}				
				switch(reader)
				{
					case 0:
						salir=1;
						break;
					
					case 1:
						printf("Introduce el número del spider a modificar\n"); 
						fflush(stdin);
						scanf("%d",&reader);
						level=-1;
						printf("Introduce el nivel deseado.\n"); 
						fflush(stdin);
						scanf("%d",&level);
						changeLevel(sim_position2d,d_data->robot_data,reader,level,number_robots,&AP);
						salir=1;
						break;
					
					case 2:
						printf("Introduce el número del spider a modificar\n");
						fflush(stdin); 
						scanf("%d",&reader);
						changeBrokenJer(sim_position2d,d_data->robot_data,reader,number_robots,&AP);
						salir=1;
						break;
					
					case 3:
						final_pausa=time(NULL);
						pause_time+=difftime(final_pausa,comienzo_pausa);	
						final=time(NULL);
						d_data->p_data->depl_time=difftime(final,comienzo)-pause_time;
						d_data->p_data->num_iterations=n*NET_MEAN_POWER_ITERATIONS;
						free(msj_tx);
						return d_data;
				}
			}

			printf("Para pausar la simulación y mostrar el menu de simulación pulse enter\n");
			
			final_pausa=time(NULL);
			pause_time+=difftime(final_pausa,comienzo_pausa);
		}
	}

	final=time(NULL);
	d_data->p_data->depl_time=difftime(final,comienzo)-pause_time;

	//Simulacion finalizada
	//XXXX. ... guardo el AP para terenlo al matar nodos
	d_data->FinalAP=AP;

	printf("El algoritmo de despliegue ha terminado.\n",i);

	sleep(2);

	d_data->p_data->num_iterations=n*NET_MEAN_POWER_ITERATIONS;
	free(msj_tx);

	return d_data;	//¿¿Por que se devuelve d_data? ¿No sería mejor devolver otra cosa?
}

