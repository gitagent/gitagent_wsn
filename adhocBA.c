/*---------------------------------------------------------------------------
 *  Proyecto Fin de Carrera: Enjambre de microbots para despliegue autónomo de 
 *                           una red inalámbrica mallada.
 * 
 *  Francisco José Aguilera Leal
 *  Universidad de Málaga
 * --------------------------------------------------------------------------- 
 * Despliegue de red ad-hoc. 
 * Uso del algoritmo de backbone y movimiento aleatorio.
 * ---------------------------------------------------------------------------
*/

#include "adhocBA.h"

/* ---------------------------------------------------------------------------
 * signalIntensity - Intensidad de la señal recibida
 * --------------------------------------------------------------------------- */
static double signalIntensity(playerc_position2d_t** sim_position2d, int my_robot, int other_robot)
{
	double intensity=0;
	double mpx,opx,mpy,opy,mpa,opa;
	double distance;		

	mpx=sim_position2d[my_robot]->px;
	mpy=sim_position2d[my_robot]->py;
	mpa=sim_position2d[my_robot]->pa;

	opx=sim_position2d[other_robot]->px;
	opy=sim_position2d[other_robot]->py;
	opa=sim_position2d[other_robot]->pa;

	distance=sqrt(pow((mpx-opx),2)+pow((mpy-opy),2));
	
	if(distance < RADIO_COVERAGE_BA)
	{
		intensity=1.0/(distance*distance);
	}

	return intensity;
}

/* ---------------------------------------------------------------------------
 * shouldJoin - Evalua si un robot debe unirse a la Backbone
 * --------------------------------------------------------------------------- */
static int shouldJoin(playerc_position2d_t** sim_position2d, robot_data_t* robot_data, int my_robot, int number_robots)
{
	int i;

	//Miro si hay algun robot de la backbone cerca
	for(i=0;i<number_robots;i++)
	{
		//Compare with all except itself and broken nodes
		if(i!=my_robot&&robot_data[i].broken!=1)
		{
			//If so, don't join
			if( (signalIntensity(sim_position2d, my_robot, i)>=MAX_WIFI_INTENSITY) && (robot_data[i].backbone==1))
			{
				return 0;
			}
		}
	}

	//Si no hay pero hay algun robot cerca debo unirme
	for(i=0;i<number_robots;i++)
	{
		//Compare with all except itself and broken nodes
		if(i!=my_robot&&robot_data[i].broken!=1)
		{
			//If so, join
			if(signalIntensity(sim_position2d, my_robot, i)>=MAX_WIFI_INTENSITY)
			{
				return 1;
			}
		}
	}

	return 0;

}

/* ---------------------------------------------------------------------------
 * shouldJoin - Evalua si un robot debe unirse a la Backbone
 * --------------------------------------------------------------------------- */
static void changeBackbone(robot_data_t* robot_data, int my_robot, int number_robots)
{
	if(my_robot>=0 && my_robot < number_robots && robot_data[my_robot].broken!=1)
	{
		if(robot_data[my_robot].backbone==0)
		{
			robot_data[my_robot].backbone=1;
			robot_data[my_robot].stopped=1;
		}
		else
		{
			robot_data[my_robot].backbone=0;
			robot_data[my_robot].stopped=0;
		}
	}
}

/* ---------------------------------------------------------------------------
 * shouldJoin - Evalua si un robot debe unirse a la Backbone
 * --------------------------------------------------------------------------- */
static void changeBroken(robot_data_t* robot_data, int my_robot, int number_robots)
{
	if(my_robot>=0 && my_robot < number_robots)
	{
		if(robot_data[my_robot].broken==0)
		{
			robot_data[my_robot].backbone=0;
			robot_data[my_robot].broken=1;
			robot_data[my_robot].stopped=1;
		}
		else
		{
			robot_data[my_robot].backbone=0;
			robot_data[my_robot].broken=0;
			robot_data[my_robot].stopped=0;
		}
	}
}

/* ---------------------------------------------------------------------------
 * shouldJoin - Evalua si un robot debe unirse a la Backbone
 * --------------------------------------------------------------------------- */
static int shouldStay(playerc_position2d_t** sim_position2d, robot_data_t* robot_data, int my_robot, int number_robots)
{
	int i;

	int backbone_contacts=0;
	int contacts=0;

	double intensity,dmax;

	//Miro cuantos robots estan cerca y cuantos son de la backbone
	for(i=0;i<number_robots;i++)
	{
		//Compare with all except itself and broken nodes
		if(i!=my_robot&&robot_data[i].broken!=1)
		{
			//Store the intensity level
			if( (signalIntensity(sim_position2d, my_robot, i)>=MAX_WIFI_INTENSITY) && (robot_data[i].backbone==1))
			{
				intensity=signalIntensity(sim_position2d, my_robot, i);
				backbone_contacts++;
				contacts++;
			}
			else if(signalIntensity(sim_position2d, my_robot, i)>=MAX_WIFI_INTENSITY)
			{
				contacts++;
			}
		}
	}

	
	//Si solo hay un robot en contacto y es de la backbone
	if((contacts==1)&&(backbone_contacts==1))
	{
		/* Si esta a más de un 8/10 de Wifi distance (cerca del limite) */
		if(intensity<=MAX_WIFI_INTENSITY/(0.8*0.8))
		{
			return 1;
		}
	}

	else
	{
		return 0;
	}
}

/* ---------------------------------------------------------------------------
 * shouldJoin - Evalua si un robot debe unirse a la Backbone
 * --------------------------------------------------------------------------- */
static void avoidObstacles(playerc_position2d_t** sim_position2d, playerc_sonar_t** sim_sonar,int myRobot, robot_data_t* robot_data,int new)
{
	double minDistance=AVOID_DISTANCE;
	int i;

	//Nuevo obstaculo. Elijo hacia que lado esquivarlo
	if(new=1)
	{
		for (i=0; i<sim_sonar[myRobot]->scan_count;i++)
		{
			if(sim_sonar[myRobot]->scan[i]<minDistance)
			{
				minDistance=sim_sonar[myRobot]->scan[i];
				//if(i>sim_sonar[myRobot]->scan_count/2)
				//{
					robot_data[myRobot].turn=1;
				//}
				//else
				//{
				//	robot_data[myRobot].turn=-1;
				//}
			}
		}
	}

	//We turn left or right
	if( robot_data[myRobot].turn==1 )
	{	
		//Turn left
		playerc_position2d_set_cmd_vel(sim_position2d[myRobot],0,0,0.5,1);
	}
	
	else if( robot_data[myRobot].turn==-1)
	{	
		//Turn right
		playerc_position2d_set_cmd_vel(sim_position2d[myRobot],0,0,-0.5,1);
	}

}

/* ---------------------------------------------------------------------------
 * moveFreely - Mueve al robot de forma aleatoria
 * --------------------------------------------------------------------------- */
static void moveFreely(playerc_position2d_t** sim_position2d, int myRobot, robot_data_t* robot_data)
{
	//Si esta girando y lleva ya demasiado ---> Andar recto
	if(robot_data[myRobot].turning==1)
	{
		if(robot_data[myRobot].turning_count==0)
		{
			robot_data[myRobot].turning=0;
			robot_data[myRobot].walking_count=rand() % 40;
			playerc_position2d_set_cmd_car(sim_position2d[myRobot],0.05,0);
		}		
		else
		{
			robot_data[myRobot].turning_count--;
		}
	}

	//Si esta andando recto y lleva ya demasiado ---> Girar
	else
	{
	 	if(robot_data[myRobot].walking_count==0)
		{
			robot_data[myRobot].turning=1;
			robot_data[myRobot].turning_count=rand() % 4;
			if(rand()%2==1)
			{
				playerc_position2d_set_cmd_vel(sim_position2d[myRobot],0,0,0.5,0);
			}
			else
			{
				playerc_position2d_set_cmd_vel(sim_position2d[myRobot],0,0,-0.5,0);
			}
		}
		else
		{
			robot_data[myRobot].walking_count--;
		}

	}
}

/* ---------------------------------------------------------------------------
 * wander - Controla el movimiento de los robots respecto a los obstaculos
 * --------------------------------------------------------------------------- */
static void wander( playerc_position2d_t** sim_position2d, playerc_sonar_t** sim_sonar,int myRobot, robot_data_t* robot_data)
{	
	int i;
	int obstacle=0;
	int new=0;


	//Check if obstacles
	for (i=0; i<sim_sonar[myRobot]->scan_count;i++)
	{
		if(sim_sonar[myRobot]->scan[i]<AVOID_DISTANCE)
		{
			obstacle=1;
		}
	}
	
	//There aren't
	if(obstacle==0)
	{
		moveFreely(sim_position2d,myRobot,robot_data);
		
		robot_data[myRobot].turn=0;

	}
	//There are and it is new
	else 
	{	
		//New obstacle
		if (robot_data[myRobot].turn==0)
		{
			new=1;
		}
		avoidObstacles(sim_position2d,sim_sonar,myRobot,robot_data,new);
		robot_data[myRobot].turning=1;
		robot_data[myRobot].turning_count=rand()%5;
		robot_data[myRobot].walking_count=0;
	}
}

/* ---------------------------------------------------------------------------
 * ---------------------------------------------------------------------------
 *
 * simAdhocBA - Función principal que controla el despliegue.
 *
 * Parametros:
 *		d_data: Datos sobre el despliegue de la red
 *		rotos: Arrays de robots rotos al comienzo
 *		sim_robots: Proxys de los robots
 *		sim_position2d: Proxys a sus interfaces de posicion
 *		simulation: Proxy a la interfaz simulacion
 *		sim_graphics2d: Proxys a sus interfaces de gráficos
 *		sim_sonar: Proxys a las interfaces sonar
 * 
 * --------------------------------------------------------------------------- 
 * --------------------------------------------------------------------------- */
deployment_data_t* simAdhocBA(deployment_data_t* d_data, int* rotos,playerc_client_t** sim_robots,playerc_position2d_t** sim_position2d, playerc_simulation_t** simulation,playerc_graphics2d_t** sim_graphics2d,playerc_sonar_t** sim_sonar)
{
	//Declaro variables 
	int i,contador=0,stop=0,n=0;
	srand(time(NULL));

	double mean_power;

	int reader,salir,level;

	int number_robots=d_data->number_robots;
	int map_size=d_data->map_size;

	double* msj_tx=(double*) malloc(sizeof(double) * number_robots);

	//Inicializo variables
	for(i=0;i<number_robots;i++)
	{
		if(d_data->nueva_simulacion)
		{
			//Variables algoritmo
			d_data->robot_data[i].turn=0;
			d_data->robot_data[i].turning_count=0;
			d_data->robot_data[i].walking_count=0;
			d_data->robot_data[i].backbone=0;
			d_data->robot_data[i].stopped=0;
			d_data->robot_data[i].level=0;
		}

		if(rotos!=NULL)
		{
			d_data->robot_data[i].broken|=rotos[i];
		}
		else
		{
			d_data->robot_data[i].broken|=0;
		}

		//Variables para medir rendimiento
		d_data->p_data->distance_walked[i]=0;
		d_data->p_data->packets_sent[i]=0;
		d_data->p_data->own_packets_sent[i]=0;
		d_data->p_data->packets_rx[i]=0;
		d_data->p_data->own_packets_rx[i]=0;

	}

	// Si es una nueva simulacion, coloco el primero que pueda en la backbone
	if(d_data->nueva_simulacion)
	{	
		for(i=0;i<number_robots&&d_data->robot_data[i-1].backbone==0;i++)
		{
			if(d_data->robot_data[i].broken==0)
			{
				d_data->robot_data[i].backbone=1;
				d_data->robot_data[i].stopped=1;
			}
		}
	}	

	
	

	//Robots tell where they are, pose and odometrics and move a bit
	for(i=0;i<number_robots;i++) 
	{
		playerc_position2d_set_cmd_car(sim_position2d[i],0.01,0);

		char spider[10];
		sprintf(spider,"spider%d",i+1);

		double px,py,pa;	

		playerc_simulation_get_pose2d(*simulation, spider, &px,&py,&pa);
		//printf("Robot  %d empieza en: %f %f %f\n", i,px,py,pa);

		d_data->p_data->positions[i].px=px;
		d_data->p_data->positions[i].py=py;
		d_data->p_data->positions[i].pa=pa;
	}

	/* --------------------------- COMIENZO DEL DESPLIEGUE ------------------------*/

	time_t comienzo=time(NULL);
	time_t final,comienzo_pausa,final_pausa;
	double pause_time=0;

	//system("reset");
	printf("Para pausar la simulación y mostrar el menu de simulación pulse enter\n");

	//Start Algorithm
	while(stop!=1)
	{
		//Wait for new data from server
		playerc_client_read(*sim_robots);
		
		for(i=0;i<number_robots;i++)
		{
			//0)Si esta roto no hace nada.
			if(d_data->robot_data[i].broken==1)
			{
				//printf("Robot %d está roto.\n",i);
				playerc_position2d_set_cmd_vel(sim_position2d[i],0,0,0,0);
			}

			//1)Miro si estoy en la backbone. Si lo estoy llamo a backboneBehaviour.
			else if(d_data->robot_data[i].backbone==1)
			{
				//printf("Robot %d pertecene a la backbone\n",i);
				playerc_position2d_set_cmd_vel(sim_position2d[i],0,0,0,0);

			}	

			//2)Debo unirme a la backbone? Repaso condiciones, y si debo se une a backbone y termina.
			else if(shouldJoin(sim_position2d, d_data->robot_data, i, number_robots))
			{
				//printf("Robot %d se ha unido a la backbone\n",i);
				d_data->robot_data[i].backbone=1;
				playerc_position2d_set_cmd_vel(sim_position2d[i],0,0,0,0);
				d_data->robot_data[i].turn=0;
				d_data->robot_data[i].turning=0;
				d_data->robot_data[i].turning_count=0;
				d_data->robot_data[i].walking_count=0;
				d_data->robot_data[i].stopped=1;
			}
				
			//3)Puedo moverme? Repaso condiciones, si no puedo permanezco quieto y termino.
			else if(shouldStay(sim_position2d, d_data->robot_data, i, number_robots))
			{
				//printf("Robot %d se queda quieto\n",i);
				playerc_position2d_set_cmd_vel(sim_position2d[i],0,0,0,0);
				d_data->robot_data[i].turn=0;
				d_data->robot_data[i].turning=0;
				d_data->robot_data[i].turning_count=0;
				d_data->robot_data[i].walking_count=0;
				d_data->robot_data[i].stopped=1;
			}

			//4)Me muevo libremente, hay obstaculos? si no hay PUEDO intentar separarme de la backbone (no es necesario).
			else
			{
				//printf("Robot %d se mueve libremente\n",i);
				//printf("Su sim position es: %f,%f,%f\n",sim_position2d[i]->px,sim_position2d[i]->py,sim_position2d[i]->pa);
				wander(sim_position2d, sim_sonar, i, d_data->robot_data);
				d_data->robot_data[i].stopped=0;
			}

			//Update positions and calculate distance walked
			player_pose2d_t new_pose;
			new_pose.px= sim_position2d[i]->px;
			new_pose.py= sim_position2d[i]->py;
			new_pose.pa= sim_position2d[i]->pa;

			d_data->p_data->distance_walked[i]+=sqrt(pow(d_data->p_data->positions[i].px-new_pose.px,2)+pow(d_data->p_data->positions[i].py-new_pose.py,2));

			d_data->p_data->positions[i]=new_pose;

			//Send broadcast with position (receiver=-1, estrategia=0)
			clearArrays(msj_tx,number_robots);
			sendAdhoc(d_data, i, i, -1, msj_tx, 0);

			//Print backbone over robots
			if(d_data->robot_data[i].broken==0)
			{
				dibuja(d_data->robot_data, sim_position2d, sim_graphics2d, i, number_robots,d_data->tipo_red,d_data->algoritmo);
			}

		}

		//Calculo la potencia media:
		if(!contador)
		{
			if(n<1000)
			{
			
				system("clear");
				//mean_power=meanPower(d_data,d_data->tipo_red,1);
				//printf("Potencia media de %d = %lf\n",1,mean_power);
				d_data->p_data->net_mean_power[n]=netMeanPower(d_data,0);
				printf("Potencia media en la red = %lf\n",d_data->p_data->net_mean_power[n]);
			}
			n++;
		}

		contador=(contador+1)%NET_MEAN_POWER_ITERATIONS;
		
		//Miro si ha terminado el algoritmo

		stop=1;
		for(i=0;i<number_robots;i++)
		{
			//If there is a robot moving and it's not broken, go on
			if(d_data->robot_data[i].stopped==0&&d_data->robot_data[i].broken!=1)
			{
				stop=0;
			}
		}

		sleep(0.1);


		if(kbhit())
		{
			comienzo_pausa=time(NULL);

			reader=-1;
			salir=0;

			for(i=0;i<number_robots;i++)
			{
				playerc_position2d_set_cmd_vel(sim_position2d[i],0,0,0,0);
				//playerc_position2d_set_cmd_car(sim_position2d[my_robot],0,0);
			}

			while(salir==0)
			{
				system("clear");
				printf ("------------------  MENU DESPLIEGUE  ------------------\n");
				printf("Acciones posibles durante el despliegue:\n");
				printf("0) Reanudar simulación.\n");
				printf("1) Añadir/Sacar de la backbone a un spider.\n");
				printf("2) Romper/Arreglar un spider.\n");
				printf("3) Terminar despliegue.\n");

				fflush(stdin);
				scanf("%d",&reader);
			
				while(reader<0||reader>3)
				{
					printf("Introduzca una de esas opciones, por favor...\n");
					fflush(stdin);
					scanf("%d",&reader);
				}				
				switch(reader)
				{
					case 0:
						salir=1;
						break;
					
					case 1:
						printf("Introduce el número del spider a modificar\n"); 
						fflush(stdin);
						scanf("%d",&reader);
						changeBackbone(d_data->robot_data,reader,number_robots);
						salir=1;
						break;
					
					case 2:
						printf("Introduce el número del spider a modificar\n");
						fflush(stdin); 
						scanf("%d",&reader);
						changeBroken(d_data->robot_data,reader,number_robots);
						salir=1;
						break;
					
					case 3:
						final_pausa=time(NULL);
						pause_time+=difftime(final_pausa,comienzo_pausa);	
						final=time(NULL);
						d_data->p_data->depl_time=difftime(final,comienzo)-pause_time;
						//cierraproxys era aqui	
						free(msj_tx);
						d_data->p_data->num_iterations=n*NET_MEAN_POWER_ITERATIONS;
						return d_data;
				}
			}

			printf("Para pausar la simulación y mostrar el menu de simulación pulse enter\n");

			final_pausa=time(NULL);
			pause_time+=difftime(final_pausa,comienzo_pausa);
		}
		
	}

	final=time(NULL);
	d_data->p_data->depl_time=difftime(final,comienzo)-pause_time;

	printf("El algoritmo de despliegue ha terminado.\n",i);

	sleep(2);
	
	d_data->p_data->num_iterations=n*NET_MEAN_POWER_ITERATIONS;
	free(msj_tx);
	
	return d_data;
}
