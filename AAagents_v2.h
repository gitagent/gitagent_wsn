/*
 * AAagents_v2.h
 *
 *  Created on: Sep 12, 2019
 *      Author: debian
 */

#ifndef AAAGENTS_V2_H_
#define AAAGENTS_V2_H_

#include <stdio.h>
#include <unistd.h> //For the sleep command
#include <time.h> //For the seed to srand for time
#include <libplayerc/playerc.h>
#include <math.h>

#include "config.h"
#include "auxFunctions.h"
#include "proxys.h"
#include "rendimiento.h"
#include "datatypes.h"
#include "coverage.h"
#include "routing.h"
#include "dibuja.h"

deployment_data_t* simAAagents2(deployment_data_t* d_data, tasks_data* t_data, assigned_robots* ar_data,help_protocol_data* hpd_data,int* rotos,int obstaculos,playerc_client_t **sim_robots, playerc_position2d_t** sim_position2d, playerc_simulation_t** simulation, playerc_graphics2d_t** sim_graphics2d,playerc_sonar_t** sim_sonar, int AA_alg_delegation_strategy, int packets_per_agent[]); //JMCG. added sonar


#endif /* AAAGENTS_V2_H_ */
