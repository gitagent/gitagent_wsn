/*---------------------------------------------------------------------------
 *  Proyecto Fin de Carrera: Enjambre de microbots para despliegue autónomo de 
 *                           una red inalámbrica mallada.
 * 
 *  Francisco José Aguilera Leal
 *  Universidad de Málaga
 * --------------------------------------------------------------------------- 
 * Archivo de cabecera de auxFunctions.c
 * ---------------------------------------------------------------------------
*/

#ifndef AUXFUNCTIONS_H
#define AUXFUNCTIONS_H

#include <string.h>
#include <math.h>
#include <sys/select.h> // For kbhit()
#include <termios.h> // For khbit()
#include <libplayerc/playerc.h>

#include "datatypes.h"

char* cmd_system(const char* command);
int kbhit(void);
void clearArrays(double* arrays,int size);
double calculateDistance(playerc_position2d_t** sim_position2d, int my_robot, int other_robot);
double calculateDistance2(performance_data_t* p_data, int my_robot, int other_robot);
void makePause();
double maximo(double* arrays);


#endif /*AUXFUNCTIONS_H*/
