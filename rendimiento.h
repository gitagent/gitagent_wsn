/*---------------------------------------------------------------------------
 *  Proyecto Fin de Carrera: Enjambre de microbots para despliegue autónomo de 
 *                           una red inalámbrica mallada.
 * 
 *  Francisco José Aguilera Leal
 *  Universidad de Málaga
 * --------------------------------------------------------------------------- 
 * Archivo de cabecera de rendimiento.c
 * ---------------------------------------------------------------------------
*/

#ifndef REND_H
#define REND_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h> //For the sleep command
#include <time.h> //For the seed to srand for time
#include <math.h>
#include <libplayerc/playerc.h>

#include "datatypes.h"
#include "auxFunctions.h"
#include "coverage.h"
#include "routing.h"
#include "dibuja.h"

#define NUM_COMMANDS 5

#define NO_STRATEGY 	0
#define ONLY_IF_CLOSER	1

int hayDatosDespliegue(deployment_data_t* d_data);
void borraDatosDespliegue(deployment_data_t* d_data,int* rotos);
void inicializaDatosDespliegue(deployment_data_t* d_data,int number_robots,int map_size);
void rendimiento(deployment_data_t* d_data, playerc_client_t **sim_robots, playerc_position2d_t** sim_position2d, playerc_graphics2d_t** sim_graphics2d);
double meanPower(deployment_data_t* d_data,int my_robot);
double netMeanPower(deployment_data_t* d_data,int verbose);
void save_performance(FILE *  fichero,deployment_data_t *d_data);

#endif /*REND_H*/
